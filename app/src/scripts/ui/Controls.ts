import {Application} from 'dijon/application'
import { Game } from 'dijon/core';
import { Sprite, Group, Text, LabelledButton } from 'dijon/display';
import { Constants } from '../utils/Statics';

export class LCButton extends Phaser.Button {
    constructor(x: number, y: number, callback: any, context: any, assetKey: string, baseFrameName: string, forceOut: boolean = false) {
        super(Application.getInstance().game,
            x,
            y,
            assetKey,
            callback,
            context,
            baseFrameName + '_over',
            baseFrameName,
            baseFrameName + '_down', 
            baseFrameName);
        
        this.forceOut = forceOut;
        this.input.useHandCursor = true;
    }

    public onInputDownHandler(sprite: any, pointer: any): void {
        super.onInputDownHandler(sprite, pointer);
     //   this.dgame.audio.playAudio('uiClick', 0.5);
    }

    public onInputOverHandler(sprite: any, pointer: any): void {
        super.onInputOverHandler(sprite, pointer);
    //   this.dgame.audio.playAudio('uiHover', 0.5);
    }

    public updateBaseFrame(base: string): void {
        this.setFrames(base + '_over', base, base + '_down', base);
    }  
    
    public get dgame(): Game {
        return Application.getInstance().game;
    }
}

export class LCLabelledButton extends LabelledButton {
    public onInputDownHandler(sprite: any, pointer: any): void {
        super.onInputDownHandler(sprite, pointer);
     //   this.dgame.audio.playAudio('uiClick', 0.5);
    }
    
    public onInputOverHandler(sprite: any, pointer: any): void {
        super.onInputOverHandler(sprite, pointer);
    //    this.dgame.audio.playAudio('uiHover', 0.5);
    }

    public get dgame(): Game {
        return Application.getInstance().game;
    }
}

export class Slider extends Group {

    protected control: Sprite;
    protected bar: Phaser.TileSprite;
    protected scrollType: string;
    
    public label: Text;
    public icon: Phaser.Image;
    public iconFrames: { active: string, inactive: string };
    public onDragEnded: Phaser.Signal;

    constructor(x: number, y: number, key: string, bgFrame: string, controlFrame: string, iconFrames: { active: string, inactive: string } = null) {
        super(x, y);
        this.iconFrames = iconFrames;
        this.onDragEnded = new Phaser.Signal();
        this.bar = this.addInternal.tileSprite(0, 0, 10, 10, key, bgFrame);
        
        this.control = this.addInternal.dSprite(0, 0, key, controlFrame);
        this.control.anchor.setTo(0.5, 0.5);
        this.control.inputEnabled = true;
        this.control.input.draggable = true;
        this.control.events.onInputUp.add(this.onInputUp, this);

        if (iconFrames !== null) {
            this.icon = this.addInternal.image(0, 0, key, this.iconFrames.active);
        }        
        else {
            this.icon = null;
        }
        this.label = this.addInternal.dText(0, 0, '100%', Constants.FONT_PRESS_START, 16, Constants.COLOR_SLIDER_LABEL, 'center');
    }

    public initializeHorizontalSlider(barWidth: number, barHeight: number, showIcon: boolean = true, showLabel: boolean = true): void {
        this.scrollType = 'horz';
        this.bar.width = barWidth;
        this.bar.height = barHeight;
        this.bar.anchor.set(0, 0.5);

        this.control.x = this.bar.right;
        this.control.y = this.bar.y - 12;
        this.control.input.allowVerticalDrag = false;
        
        if (this.icon !== null) {
            this.icon.x = this.bar.left - 8;
            this.icon.y = this.bar.y;
            this.icon.visible = showIcon;
            this.icon.anchor.setTo(1, 0.5);
        }
        this.label.x = this.bar.right + 16;
        this.label.y = this.bar.y + 2;
        this.label.visible = showLabel;
        this.label.anchor.setTo(0, 0.5);

        this.updateFunc = this.horizontalUpdate;
    }

    public initializeVerticalSlider(barWidth: number, barHeight: number, showIcon: boolean = true, showLabel: boolean = true): void {
        this.scrollType = 'vert';
        this.bar.width = barWidth;
        this.bar.height = barHeight;
        this.bar.anchor.set(0.5, 0);
        
        this.control.x = this.bar.x - 12;
        this.control.y = this.bar.top;
        this.control.input.allowHorizontalDrag = false;
        this.control.angle = -90;

        if (this.icon !== null) {
            this.icon.x = this.bar.x;
            this.icon.y = this.bar.top - 8;
            this.icon.visible = showIcon;
            this.icon.anchor.setTo(0.5, 1);
        }
        this.label.x = this.bar.x;
        this.label.y = this.bar.bottom + 16;
        this.label.visible = showLabel;
        this.label.anchor.setTo(0.5, 0);

        this.updateFunc = this.verticalUpdate;
    }

    public update(): void {
        if (this.control.input.isDragged === true) {
            this.updateFunc();
        }
    }

    protected updateFunc(): void {}
    
    public onInputUp(): void {
        this.updateFunc();
        this.onDragEnded.dispatch(this);
    }

    public setControlPosition(percent: number): void {
        if (this.scrollType === 'horz') {
            this.control.x = this.bar.x + (this.barWidth * percent);
            this.updateElements();
        }
        else if (this.scrollType === 'vert') {
            this.control.y = this.bar.y + (this.barHeight * percent);
            this.updateElements();
        }
    }

    protected horizontalUpdate(): void {
        if (this.control.x <= this.bar.left) {
            this.control.x = this.bar.left;
        }
        else if (this.control.x >= this.bar.right) {
            this.control.x = this.bar.right;
        }
        this.updateElements();
    }

    protected verticalUpdate(): void {
        if (this.control.y <= this.bar.top) {
            this.control.y = this.bar.top;
        }
        else if (this.control.y >= this.bar.bottom) {
            this.control.y = this.bar.bottom;
        }
        this.updateElements();
    }

    protected updateElements(): void {
        if (this.icon !== null) {
            if (this.sliderPercent <= 0) {
                this.icon.frameName = this.iconFrames.inactive;
            }
            else {
                this.icon.frameName = this.iconFrames.active;
            }
        }
        if (this.label.visible) {
            this.label.text = Math.round(this.sliderPercent * 100) + '%';
        }
    }    
    
    public get sliderPercent(): number {
        if (this.scrollType === 'horz') {
            return this.sliderPercentHorz;
        }    
        else {
            return this.sliderPercentVert;
        }
    }
 
    protected get sliderPercentHorz(): number {
        return Math.min(Math.max((this.control.x - this.bar.x) / this.barWidth, 0), 1);
    }

    protected get sliderPercentVert(): number {
        return Math.min(Math.max((this.control.y - this.bar.y) / this.barHeight, 0), 1);
    }

    public get barWidth(): number {
        return this.bar.width;
    }

    public get barHeight(): number {
        return this.bar.height;
    }
}