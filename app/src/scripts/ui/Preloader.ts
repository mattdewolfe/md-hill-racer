import {Group, Text} from 'dijon/display';
import {IPreloadHandler} from 'dijon/interfaces';
import PieProgressBar from './PieProgressBar';

export default class Preloader extends Group implements IPreloadHandler {
    private _wiper: Phaser.Image;
    private _progressBar1: PieProgressBar;

    public transitionInComplete: Phaser.Signal = new Phaser.Signal();
    public transitionOutComplete: Phaser.Signal = new Phaser.Signal();

    private _inTween: Phaser.Tween;
    private _outTween: Phaser.Tween;

    constructor(x: number, y: number, name: string) {
        super(x, y, name, true);
        this.init();
        this.buildInterface();
    }

    // Group overrides
    protected buildInterface() {
        let gfx = this.game.add.graphics();
        gfx.beginFill(0xaaaaaa, 1);
        gfx.drawRect(0, 0, this.game.width, this.game.height);
        gfx.endFill();

        this._wiper = this.addInternal.image(0, 0, gfx.generateTexture());

        this.game.world.remove(gfx, true);

        this.alpha = 0;
        this.visible = false;

        this._progressBar1 = new PieProgressBar(this.game.width * 0.5, this.game.height * 0.75, 50);
        this.addInternal.existing(this._progressBar1);

        this._inTween = this.game.add.tween(this).to({ alpha: 1 }, 300, Phaser.Easing.Quadratic.Out);
        this._outTween = this.game.add.tween(this).to({ alpha: 0 }, 200, Phaser.Easing.Quadratic.In);

        this._inTween.onComplete.add(this._in, this);
        this._outTween.onComplete.add(this._out, this);
    }

    // iPreloadHandler implementations
    public loadStart() {
        this._progressBar1.updateProgress(0);

    }

    public loadProgress(progress: number) {
        this._progressBar1.updateProgress(progress/100);
    }

    public loadComplete() {

    }

    public transitionIn() {
        this.visible = true;
        this._inTween.start();
    }

    public transitionOut() {
        this._outTween.start();
    }

    // private methods
    protected _in() {
        this.transitionInComplete.dispatch();
    }

    protected _out() {
        this.visible = false;
        this.transitionOutComplete.dispatch();
    }
}