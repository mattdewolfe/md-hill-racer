import {Application} from "dijon/application";
import {Group, NineSliceImage} from "dijon/display";
import Notifications from "../utils/Notifications";
import BaseMediator from "../mediator/BaseMediator";
import { Globals } from '../utils/Statics';

export interface IPopup {
    id: string;
    show(): Phaser.Tween;
    hide(): Phaser.Tween;
    visible: boolean;
}

export default class BasePopup extends Group implements IPopup {
    protected showTween: Phaser.Tween;
    protected hideTween: Phaser.Tween;
    protected startPosition: Phaser.Point;
    protected allowInput: boolean;

    // Anything that will need to be scaled, but otherwise has no cause for a 
    // stored reference, should be placed in here.
    protected scallables: any[];

    public bg: Phaser.Image;
    
    constructor(public id: string, x: number= 0, y: number= 0) {
        super(x, y, id);

        this.visible = false;
        this.alpha = 0;
        this.scallables = [];

        this.init();
        this.buildInterface();

        this.startPosition = this.position.clone();
        this.y -= 20;
        
        this.addTweens();
        this.bg.inputEnabled = true;
        this.bg.input.useHandCursor = false;
    }

    public init(): void {
        this.addMediator();
    }

    /* OVERRIDE THESE FUNCTIONS IN EXTENDED CLASSES */
    
    public buildInterface(): void { }
    public handleLanguageToggle(): void { }
    public handleResize(): void {
        for (let i = 0; i < this.scallables.length; i++) {
            this.scallables[i].scale.setTo(Globals.SCALE_RATIO, 1);
        }
    }
    public getCopyByID(id: string): string {
        // Overrite this to get copy specific to your popups needs.
        return id;
    }

    protected addMediator(): void {
        this._mediator = BasePopup.retrieveMediator(BaseMediator.MEDIATOR_NAME, this);
        if (this._mediator === null) {
            this._mediator = new BaseMediator(this, true, BaseMediator.MEDIATOR_NAME);
        }
        this.register();
    }

    /* END VIRTUALS */

    public destroy(): void {
        this.hideTween.onComplete.removeAll();
        this.showTween = null;
        this.hideTween = null;
        super.destroy();
    }

    public show(): Phaser.Tween {
        if (this.showTween.isRunning) {
            return;
        }
        this.clearActiveTweens();
        this.visible = true;
        this.showTween.start();
        return this.showTween;
    }

    public hide(): Phaser.Tween {
        if (this.hideTween.isRunning) {
            return;
        }
        this.allowInput = false;
        this.clearActiveTweens();
        this.hideTween.start();
        return this.hideTween;
    }

    protected close(): void {
        this._mediator.sendNotification(Notifications.HIDE_POPUP, this.id);
    }
    
    protected addTweens(): void {
        this.showTween = this.game.add.tween(this).to({ alpha: 1, y: this.startPosition.y }, 300, Phaser.Easing.Bounce.Out);
        this.showTween.onComplete.add(this.showComplete, this);
        this.hideTween = this.game.add.tween(this).to({ alpha: 0, y: this.startPosition.y + 80 }, 300, Phaser.Easing.Quadratic.Out);
        this.hideTween.onComplete.add(this.hideComplete, this);
    }
    
    protected showComplete(): void {
        this.allowInput = true;
    }

    protected hideComplete(): void {
        this.visible = false;
        this.y = this.startPosition.y - 20;
    }

    protected fadeInElement(element: any): void {
        element.visible = true;
        this.game.add.tween(element).to({ alpha: 1 }, 350, Phaser.Easing.Cubic.In, true);
    }

    protected fadeOutElement(element: any): void {
        this.game.add.tween(element).to({ alpha: 0 }, 350, Phaser.Easing.Cubic.In, true).onComplete.addOnce(() => {
            element.visible = false;
        });
    }   
    
    protected sendResumeNotification(): void{
        this._mediator.sendNotification(Notifications.RESUME_GAME);
    }

    protected clearActiveTweens(): void {
        if (this.showTween.isRunning) {
            this.showTween.stop();
        }
        
        if (this.hideTween.isRunning) {
            this.hideTween.stop();
        }
    }

    public static retrieveMediator(name: string, viewComp: any): any {
        return BaseMediator.retrieveMediator(name, viewComp);
    }  
    
    protected register(): void {
        this._mediator.sendNotification(Notifications.REGISTER_POPUP, this);
    }
}