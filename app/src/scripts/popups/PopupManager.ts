import {Group} from 'dijon/display';
import PopupManagerMediator from '../mediator/PopupManagerMediator';
import {IPopup} from "./BasePopup";
import BasePopup from "./BasePopup";
import { Textures, Placeholders } from 'dijon/utils';

export default class PopupManager extends Group {
    public modal: Phaser.Sprite;
    
    protected fromOptions: boolean = false;
    
    constructor() {
        super(0, 0, 'PopupManager', false);

        this.init();
        this.buildInterface();
    }

    public init(): void {
        this.classType = BasePopup;
        this.fixedToCamera = true;
        this._mediator = BasePopup.retrieveMediator(PopupManagerMediator.MEDIATOR_NAME, this);
        if (this._mediator === null) {
            this._mediator = new PopupManagerMediator(this);
        }
    }

    public buildInterface(): void {
        this.addModal();
    }

    public addModal(): void {
        if (this.modal) {
            return;
        }

        this.modal = this.add(this.game.add.sprite(0, 0, Textures.rect(this.game.width, this.game.height, 0x000000, 0.25)));
        this.modal.name = 'popupManagerModal';
        this.modal.inputEnabled = true;
        this.modal.events.onInputDown.add(this.mediator.modalPressed, this.mediator);
        
        this.modal.cacheAsBitmap = true;
        this.modal.visible = false;
    }

    public showModal(): void {
        this.sendToBack(this.modal);
        this.mediator.notifyPauseGame();
        this.modal.visible = true;
    }

    public hideModal(andResume: boolean = true): void {
        if (andResume === true) {
            this.mediator.notifyResumeGame();
        }    
        this.modal.visible = false;
    }

    public removeAll(destroy: boolean = true, silent: boolean = false): void {
        if (this.modal) {
            this.sendToBack(this.modal);
            this.hideModal();
        }
        while (this.children.length > 1) {
            this.remove(this.getChildAt(1), destroy, silent);
        }
        this.mediator.clearLookup();
    }

    private get mediator(): PopupManagerMediator {
        return <PopupManagerMediator>this._mediator;
    }
}