import BasePopup from './BasePopup';
import { Constants, Globals } from '../utils/Statics';
import {Slider, LCLabelledButton} from '../ui/Controls';
import OptionsPopupMediator from '../mediator/OptionsPopupMediator';

export default class OptionsPopup extends BasePopup {

    private sfxSlider: Slider;
    private musicSlider: Slider;
    private quitBtn: LCLabelledButton;

    constructor() {
        super(Constants.POPUP_OPTIONS, 0, 0);
    }

    /* BUILD SEQUENCE */
    
    public buildInterface(): void {
        this.bg = this.addInternal.image(this.game.centerX, this.game.centerY, 'ui', 'popup_background');
        this.bg.anchor.setTo(0.5);
       
        this.sfxSlider = new Slider(this.bg.x, this.bg.y - (this.bg.realHeight * 0.3), 'ui', 'grey_slider_horz', 'yellow_slider_down', { active: 'sfx_icon', inactive: 'sfx_icon_mute'});
        this.sfxSlider.initializeHorizontalSlider(220, 10);
        this.sfxSlider.x -= this.sfxSlider.barWidth * 0.5;
        this.addInternal.existing(this.sfxSlider);
        this.sfxSlider.onDragEnded.add(this.changeSFXVolume, this);
        this.scallables.push(this.sfxSlider);

        this.musicSlider = new Slider(this.bg.x, this.bg.y - (this.bg.realHeight * 0.1), 'ui', 'grey_slider_horz', 'yellow_slider_down', { active: 'music_icon', inactive: 'music_icon_mute'});
        this.musicSlider.initializeHorizontalSlider(220, 10);
        this.musicSlider.x -= this.musicSlider.barWidth * 0.5;
        this.addInternal.existing(this.musicSlider);
        this.musicSlider.onDragEnded.add(this.changeMusicVolume, this);
        this.scallables.push(this.musicSlider);

        this.sfxSlider.setControlPosition(this.game.audio.spriteVolume);
        this.musicSlider.setControlPosition(this.game.audio.soundVolume);
        
        this.quitBtn = this.addButton(this.bg.x + (this.bg.realWidth * 0.225), this.bg.y + (this.bg.realHeight * 0.3), this.quitPressed, 'quit_game');
        this.addInternal.existing(this.quitBtn);
        this.scallables.push(this.quitBtn);
    }

    private addButton(x: number, y: number, callback: any, copyID: string): LCLabelledButton {
        let button = new LCLabelledButton(x, y, callback, this, 'menu', 'red_btn_normal', 'red_btn_down', 'red_btn_over', 'red_btn_normal');
        button.addLabel(this.getCopyByID(copyID), 14, Constants.FONT_PRESS_START);
        button.centerPivot();
        return button;
    }   
    
    private layoutForGameplay(): void {
        this.sfxSlider.y = this.y + this.bg.y - (this.bg.realHeight * 0.25);
        this.musicSlider.y = this.bg.y - (this.bg.realHeight * 0.1);
    }

    private layoutForMenu(): void {
        this.sfxSlider.y = this.y + this.bg.y - (this.bg.realHeight * 0.2);
        this.musicSlider.y = this.game.centerY;
    }

    /* INPUT & EVENTS */
    
    public changeMusicVolume(): void {
        this.game.audio.soundVolume = this.musicSlider.sliderPercent;
        this.game.audio.playAudio('message_alt', this.game.audio.soundVolume);
    }

    public changeSFXVolume(): void {
        this.game.audio.spriteVolume = this.sfxSlider.sliderPercent;
        this.game.audio.playAudio('message_alt');
    }

    public quitPressed(): void {
        this.mediator.quitToMenu();
    }

    public howToPressed(): void {
        this.mediator.requestPopup(Constants.POPUP_HELP);
    }

   /* OVERRIDES */
    
    public show(): Phaser.Tween {
        if (this.inGameState) { 
            this.layoutForGameplay();
        }
        else {
            this.layoutForMenu();
        }
        this.quitBtn.visible = this.inGameState;
        return super.show();
    }
    
    protected hideComplete(): void {
        super.hideComplete();
        if (this.game.state.current !== Constants.STATE_MENU) {
            this.mediator.notifyResumeGame();
        }
        this.mediator.updatePlayerSettings();
    }

    protected addMediator(): void {
        this._mediator = BasePopup.retrieveMediator(OptionsPopupMediator.MEDIATOR_NAME, this);
        if (this._mediator === null) {
            this._mediator = new OptionsPopupMediator(this, true, OptionsPopupMediator.MEDIATOR_NAME);
        }
        this.register();
    }

    public getCopyByID(id: string): string {
        return this.mediator.copyModel.getCopy('options', id);
    }
    
    /* GET/SETS */
    
    public get mediator(): OptionsPopupMediator {
        return <OptionsPopupMediator>this._mediator;
    }

    public get inGameState(): boolean {
        return (this.game.state.current !== Constants.STATE_MENU);
    }
    
}