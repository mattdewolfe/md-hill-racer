import BaseState from "./BaseState";
import GameplayMediator from "../mediator/GameplayMediator";
import Vehicle from '../actors/Vehicle';
import Rock from '../actors/Rock';
import Actor from '../actors/Actor';

export default class Gameplay extends BaseState {

    protected _worldMat: Phaser.Physics.P2.Material;
    protected _truck: Vehicle;

    // Phaser.State overrides
    public init(): void {
        this._mediator = GameplayMediator.retrieveMediator(GameplayMediator.MEDIATOR_NAME, this);
        if (this._mediator === null) {
            this._mediator = new GameplayMediator(this);
        }

        this.game.world.setBounds(0, 0, this.game.width * 3, this.game.height);
        this.game.physics.startSystem(Phaser.Physics.P2JS);
        this.game.physics.p2.gravity.y = 300;
        this._worldMat = this.game.physics.p2.createMaterial("worldMaterial");
    }

    // dijon.core.State overrides
    public listBuildSequence() {
        return [
            this._addBG, 
            this._addTerrain,
            this._addVehicle,
            this._setCollisions
        ]
    }
    
    protected _addBG(): void {

    }

    protected _addTerrain(): void {
        for (let i = 0; i < 6; i++) {
            //initialize the hill and place it in the center of the bottom boundary
            let hill = new Actor(150 + (i * 350) + (Math.random() * 50), this.game.height - 30, 'gameplay', 'hill', 'hill', true);
             /*
            * give it the same material as world boundaries so that it has
            * high friction with the wheels
            */
            hill.body.setMaterial(this._worldMat);
            this.add.existing(hill);
        }
    }

    protected _addVehicle(): void {
        this._truck = new Vehicle(100, this.game.height - 250, 'gameplay', 'truck', 'truck');
        this.add.existing(this._truck);
        this.game.camera.follow(this._truck);
    }

    protected _setCollisions(): void {
        let contactMaterial: Phaser.Physics.P2.ContactMaterial = this.game.physics.p2.createContactMaterial(this._truck.wheelMaterial, this._worldMat);
    	contactMaterial.friction = 1000;
        // Bounce
	    contactMaterial.restitution = 0.4;

        // Pass through material?
        // contactMaterial.stiffness = 10;
        // Conveyor Belt
        // contactMaterial.surfaceVelocity = 10;
    }

    /* GET/SET*/

    protected get mediator(): GameplayMediator {
        return <GameplayMediator>this._mediator;
    }

    public get preloadID(): string {
        return 'game';
    }
}