import {State} from "dijon/core";
import BaseMediator from "../mediator/BaseMediator";

export default class BaseState extends State {
    public afterBuild(): void {
        this.resume();
    }
}