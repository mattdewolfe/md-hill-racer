import BaseState from "./BaseState";
import MenuMediator from '../mediator/MenuMediator';
import { Constants } from '../utils/Statics';
import {LabelledButton, Text} from 'dijon/display';

// This class currently uses a lot of draw calls, because of all the text objects it creates.
// Any of these that do not change after their creation can be cached into a single draw call.
// Do you know how to do that?
export default class Menu extends BaseState {
    // Phaser.State overrides
    public init() {
        this._mediator = MenuMediator.retrieveMediator(MenuMediator.MEDIATOR_NAME, this);
        if (this._mediator === null) {
            this._mediator = new MenuMediator(this);
        }
    }
		
    // dijon.core.State overrides
    public listBuildSequence() {
        return [
            this._addBG, 
            this._addPopups,
            this._addButtons
        ]
    }

    public afterBuild() {
        super.afterBuild();
        this.resume();
    }

    private _addBG(): void {
        let title = this.add.dText(this.game.centerX, this.game.centerY - 200, this.getCopyByID('title'), Constants.FONT_PRESS_START, 30, '#ffffff');
        title.centerPivot();
    }   

    private _addButtons(): void {
        let startBtn = new LabelledButton(this.game.centerX, this.game.centerY, this.startGame, this, 'menu', 'red_btn_normal', 'red_btn_down', 'red_btn_over', 'red_btn_normal');
        startBtn.addLabel(this.getCopyByID('start_game'), 14, Constants.FONT_PRESS_START);
        startBtn.centerPivot();
        this.add.existing(startBtn);
    }
    
    private _addPopups(): void {
        this.mediator.addPopups();
    }

    /* EVENTS */

    public startGame(): void {
        this.game.transition.to(Constants.STATE_GAME);
    }

    /* GET/SET */

    public getCopyByID(id: string): string {
        return this.mediator.copyModel.getCopy('menu', id);
    }

    public get mediator(): MenuMediator {
        return <MenuMediator>this._mediator;
    }

    public get preloadID(): string {
        return 'menu';
    }
}
  