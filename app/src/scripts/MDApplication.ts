import {Application} from "dijon/application";
import {Game} from "dijon/core";
import {Device} from "dijon/utils";
import {CopyModel} from "dijon/mvc";

import ApplicationMediator from "./mediator/ApplicationMediator";
import {Constants, Globals} from "./utils/Statics";
import Notifications from "./utils/Notifications";
import Boot from "./state/Boot";
import Gameplay from "./state/Gameplay";
import Menu from "./state/Menu";
import GameModel from "./model/GameModel";
import Preloader from './ui/Preloader';

export enum EInitOrientation {
    CORRECT,
    INCORRECT,
    IRRELEVANT
}

export enum EDeviceOrientation {
    LANDSCAPE, 
    PORTRAIT,
    DESKTOP
}

export enum EBootState {
    PRELOADING,
    READY_FOR_BOOT,
    BOOT_COMPLETE
}

export default class MDApplication extends Application {
    public static ROTATE_IMAGE: string = "/assets/img/playlandscape.png";
    private static DESIRED_ORIENTATION: EDeviceOrientation = EDeviceOrientation.LANDSCAPE;

    public gameId: string = null;
    public preloader: Preloader;

    private _lastHeight: number = 0;
    private _initOrientation: EInitOrientation;
    private _bootState: EBootState = EBootState.PRELOADING;
    private _orientation: EDeviceOrientation;

    constructor() {
        super();
    }

    // overrides
    public createGame() {
        this.game = new Game({
            width: this._getGameWidth(),
            height: this._getGameHeight(),
            parent: 'game-container',
            //renderer: Phaser.CANVAS,
            renderer: Device.cocoon ? Phaser.CANVAS : this._getRendererByDevice(),
            transparent: false,
            // use this if you want to switch between @2x and @1x graphics
            //resolution: this._getResolution(),
            resolution: 1,
            plugins: Device.mobile ? [] : ['Debug']

        });

        this._mediator = new ApplicationMediator(this);
        this._addStates();
    }
    // public methods
    public startGame(): void {
        this.game.state.start(Constants.STATE_BOOT);
    }

    // called from the boot state as we can't initialize plugins until the game is booted
    public registerModels(): void {
        const gameModel = new GameModel('game_data');
        const copyModel = new CopyModel('copy');
    }

    // private methods
    // adds states
    private _addStates() {
        this.game.state.add(Constants.STATE_BOOT, Boot);
        this.game.state.add(Constants.STATE_GAME, Gameplay);
        this.game.state.add(Constants.STATE_MENU, Menu);
    }

    public bootComplete(): void {
        this.preloader = new Preloader(0, 0, 'preloader');
        this.game.addToUI.existing(this.preloader);
        this.game.transition.addAll(this.preloader);
        this.game.time.events.add(100, this._moveToMenu, this);
    }

    protected _moveToMenu(): void {
        this.game.transition.to(Constants.STATE_MENU);
    }    

    /* INITIAL ORIENTATION AND SCALE ADJUSTMENT FUNCTIONS */

    public adjustScaleSettings(): void {
        if (this.game.device.desktop) {
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            this.game.scale.setMinMax(300, 200, 1050, 700);
            this.game.scale.pageAlignHorizontally = true;
            this._initOrientation = EInitOrientation.CORRECT;
            this._orientation = EDeviceOrientation.DESKTOP;
        }
        else {
            this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
            if (this.game.device.android) {
                if (screen.width > screen.height) {
                    this._orientation = EDeviceOrientation.LANDSCAPE;
                }
                else {
                    this._orientation = EDeviceOrientation.PORTRAIT;
                }
                this.game.scale.setResizeCallback(this._checkOrientationAndroid, this);
            }
            else if (this.game.device.iOS) {
                if (window.orientation === 0 || window.orientation === 180) {
                    this._orientation = EDeviceOrientation.PORTRAIT;
                }
                else {
                    this._orientation = EDeviceOrientation.LANDSCAPE;
                }
                this.game.scale.setResizeCallback(this._checkOrientationiOS, this);
            }
            this.game.scale.pageAlignVertically = true;
            this._initRotationImage();
            this._disableScrollEvents();
            this._assignScreenSizeByDevice();
        }
    }

    // Add the 'please rotate device' image to the page (only called for mobile setup)    
    private _initRotationImage(): void {
        let element = document.getElementById("turn");
        let imageStr = "url('" + (window.hasOwnProperty("baseURL") ? window["baseURL"] + MDApplication.ROTATE_IMAGE : "." + MDApplication.ROTATE_IMAGE) + "') no-repeat center center";
        element.style.background = imageStr;
    }
        
    private _disableScrollEvents(): void {
        let container = document.getElementById('game-container');
        container.addEventListener('touchmove', function(e) {
            e.preventDefault();
        }, false);
    }

    private _assignScreenSizeByDevice(): void {
        if (this._orientation === MDApplication.DESIRED_ORIENTATION) {
            this._initOrientation = EInitOrientation.CORRECT;
            this.game.scale.setMinMax(window.innerWidth, window.innerHeight, window.innerWidth, window.innerHeight);
            Globals.SCALE_RATIO = this._originalAspectRatio / (window.innerWidth / window.innerHeight);
            this._hideRotateImage();
        }
        else {
            this._initOrientation = EInitOrientation.INCORRECT;
            this._showRotateImage();
        }
    }

    public adjustRendererSettings(): void {
        this.game.stage.disableVisibilityChange = true;
        this.game.forceSingleUpdate = true;
        this.game.camera.roundPx = false;
        this.game.renderer.renderSession.roundPixels = true;
        this.game.antialias = true;
        this.game.renderer.clearBeforeRender = this.game.renderType === Phaser.CANVAS;
    }

    // If mobile app was started in a locked out orientation, this will be called after the first 'rotate' to proper orientation    
    private _initialResize(): void {
        this._resizeApp();
        if (this._bootState === EBootState.READY_FOR_BOOT) {
            this.bootComplete();
        }
    }

    /* ORIENTATION HANDLING */

    private _handleEnterPortrait(): void {
        if (this._orientation === EDeviceOrientation.LANDSCAPE) {
            if (MDApplication.DESIRED_ORIENTATION === EDeviceOrientation.PORTRAIT) {
                this._enterCorrectOrientation();
            }    
            else {
                this._enterIncorrectOrientation();
            }
        }
        this._orientation = EDeviceOrientation.PORTRAIT;
    }

    private _handleEnterLandscape(): void {
        if (this._orientation === EDeviceOrientation.PORTRAIT) {
            if (MDApplication.DESIRED_ORIENTATION === EDeviceOrientation.LANDSCAPE) {
                this._enterCorrectOrientation();
            }    
            else {
                this._enterIncorrectOrientation();
            }
        } 
        this._orientation = EDeviceOrientation.LANDSCAPE;
    }

    private _enterCorrectOrientation(): void {
        this.mediator.sendNotification(Notifications.RESUME_GAME);
        if (!this.game.device.desktop) {
            if (this._initOrientation === EInitOrientation.INCORRECT) {
                this._initOrientation = EInitOrientation.CORRECT;
                this.game.time.events.add(100, this._initialResize, this);
            } else {
                this._hideRotateImage();
                this._resizeApp();
            }
        }
    }   
    
    private _enterIncorrectOrientation(): void {
        this.mediator.sendNotification(Notifications.PAUSE_GAME);
        if (!this.game.device.desktop) {
            this._showRotateImage();
        }
    }

    private _showRotateImage(): void {
        document.getElementById("turn").style.display = 'block';
    }

    private _hideRotateImage(): void {
        document.getElementById("turn").style.display = 'none';
    }

    private _resizeApp(): void {
        if (this._lastHeight !== Math.floor(window.innerHeight)) {
            this._lastHeight = Math.floor(window.innerHeight);
        //    this.game.scale.setMinMax(window.innerWidth, window.innerHeight, window.innerWidth, window.innerHeight);
        //    this.game.scale.refresh();
            this._hideRotateImage();
        //    Globals.SCALE_RATIO = this._originalAspectRatio / (window.innerHeight /window.innerWidth);
            this.mediator.notifyAppResized();
        }
    }
    
    /* ORIENTATION CHECKS */
    /* This block of code is custom code for handling device orientation and locking
     * out specific orientations as desired. This currently exists because in phaser 2.6.2
     * iFrames can cause phaser to stop receiving the pages resize callbacks.
     */

    /**
     * Checks device orientation against a desired orientation. Always true on desktop.
     * @param desired: string, orientation you want to be in - expects 'portrait' or 'landscape'
     * @return boolean: returns true if device orientation matches the desired orientation
     */
    protected _inOrientation(desired: string): boolean  {
        if (this.game.device.desktop) {
            return true;
        }
        else if (this.game.device.iOS) {
            // We are in portrait.
            if (window.orientation === 0 || window.orientation === 180) {
                return desired === 'portrait';
            }
            // We are in landscape.
            else {
                return desired === 'landscape';
            }
        }
        else {
            // We are in portrait.
            if (screen.width < screen.height) {
                return desired === 'portrait';
            }
            // We are in landscape.
            else {
                return desired === 'landscape';
            }
        }
    }
    
    private _checkOrientationAndroid(): void {
        // We are in portrait.
        if (screen.width < screen.height) {
            this._handleEnterPortrait();
        }
        // We are in landscape.
        else {
            this._handleEnterLandscape();
        }
    }   

    private _checkOrientationiOS(): void {
        // We are in portrait.
        if (window.orientation === 0 || window.orientation === 180) {
            this._handleEnterPortrait();
        }
        // We are in landscape.
        else {
            this._handleEnterLandscape();
        }
    } 

    /* GET/SETS */
    
    private get _originalAspectRatio(): number {
        return (this._getGameWidth() / this._getGameHeight());
    }

    private _getGameWidth(): number {
        return Device.mobile ? 1000 : 1050;
    }

    private _getGameHeight(): number {
        return Device.mobile ? 588 : 700;
    }

    private _getResolution(): number {
        if (Application.queryVar('resolution') && !isNaN(Application.queryVar('resolution'))) {
            return Application.queryVar('resolution');
        }
        if (Device.cocoon) {
            return (window.devicePixelRatio > 1 ? 2 : 1);
        } else {
            return Device.mobile ? 1 : (window.devicePixelRatio > 1 ? 2 : 1);
        }
    }

    private _getRendererByDevice(): number {
        return Device.mobile && window.devicePixelRatio < 2 ? Phaser.CANVAS : Phaser.AUTO;
    }
    
    // getter / setter
    public get mediator(): ApplicationMediator {
        return <ApplicationMediator>this._mediator;
    }

    public get gameModel(): GameModel {
        return <GameModel>this.retrieveModel(GameModel.MODEL_NAME);
    }

    public get copyModel(): CopyModel {
        return <CopyModel>this.retrieveModel(CopyModel.MODEL_NAME);
    }
}