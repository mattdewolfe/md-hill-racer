import Actor from './Actor';
import {Group} from 'dijon/display';

export default class Vehicle extends Actor {

    protected _maxSpeed: number = 300;
    protected _accel: number = 5;

    protected _wheels: Group;
    protected _wheelMaterial: Phaser.Physics.P2.Material;
    protected _cursors: Phaser.CursorKeys;
    protected _canBounce: boolean = true;
    protected _rotateSpeed: number = 0;

    constructor(x: number, y: number, key: string, frame: string, bodyKey: string) {
        super(x, y, key, frame, bodyKey, false);

        this._wheels = this.game.add.dGroup();
        this._wheelMaterial = this.game.physics.p2.createMaterial('wheelMaterial');
	    this._cursors = this.game.input.keyboard.createCursorKeys();

	    this._initWheel(55);
	    this._initWheel(-52);
    }

    public update(): void {
        /*
        * rotate truck wheels right and left based on keyboard arrows
        * by iterating through the wheel group
        */
        if (this._cursors.left.isDown) {
            this._accelBackward();
        }
        else if (this._cursors.right.isDown) {
            this._accelForward();
        }
        else {
            this._decelerate();
        }
        this._updateWheels();
    }

    protected _decelerate(): void {
        this._rotateSpeed = Math.floor(this._rotateSpeed * 0.95);
    }

    protected _accelForward(): void {
        this._rotateSpeed -= this._accel;
        if (this._rotateSpeed < -this._maxSpeed) {
            this._rotateSpeed = -this._maxSpeed;
        }
    }

    protected _accelBackward(): void {
        this._rotateSpeed += this._accel;
        if (this._rotateSpeed > this._maxSpeed * 0.5) {
            this._rotateSpeed = this._maxSpeed * 0.5;
        }
    }

    protected _updateWheels(): void {
        this._wheels.children.forEach( (wheel: any) => {
            wheel.body.rotateLeft(this._rotateSpeed);
        });
    }

    protected _initWheel(offsetX: number, driveWheel: boolean = true): void {
        let offsetY = 24;
        //position wheel relative to the truck
        var wheel = this.game.add.sprite(this.position.x + offsetX, this.position.y + offsetY, 'gameplay', 'wheel');

        this.game.physics.p2.enable(wheel);
        wheel.body.clearShapes();
        wheel.body.addCircle(15.5);
        
        /*
        * Constrain the wheel to the truck so that it can rotate freely on its pivot
        * createRevoluteConstraint(bodyA, pivotA, bodyB, pivotB, maxForce)
        * change maxForce to see how it affects chassis bounciness
        */
        var maxForce = 50;
        var rev = this.game.physics.p2.createRevoluteConstraint(this.body, [offsetX, offsetY],  wheel.body, [0,0], maxForce);

        if (driveWheel === true) {
            this._wheels.add(wheel);
        }

        //call onWheelContact when the wheel begins contact with something
        wheel.body.onBeginContact.add(this.onWheelContact, this);

        /*
        * set the material to be the wheel material so that it can have
        * high friction with the ground
        */
        wheel.body.setMaterial(this._wheelMaterial);
    }

    public onWheelContact(phaserBody: any, p2Body: Phaser.Physics.P2.Body): void {
        /*
	* allow another truck bounce if the wheel has touched the bottom boundary
	* (which has no phaser body and has id 4 in P2) or the hill
	*/
	if ((phaserBody === null && p2Body.id == 4)
        || (phaserBody && phaserBody.sprite.key == "hill")) {
            this._canBounce = true;
        }
    }

    public get wheelMaterial(): Phaser.Physics.P2.Material {
        return this._wheelMaterial;
    }
}