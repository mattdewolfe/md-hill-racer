import {Sprite} from 'dijon/display';

export default class Actor extends Sprite {
    constructor(x: number, y: number, key: string, frame: string, bodyKey: string, kinematic: boolean = false) {
        super(x, y, key, frame);

        this.game.physics.p2.enable(this);   
        this.p2Body.clearShapes(); 
        this.p2Body.loadPolygon("physics", bodyKey);
        this.p2Body.kinematic = kinematic;
    }

    public get p2Body(): Phaser.Physics.P2.Body {
        return <Phaser.Physics.P2.Body>this.body;
    }
}