import {Logger} from "dijon/utils";
import {INotification} from "dijon/interfaces";
import { AnalyticsEventModel } from "dijon/core";
import BaseMediator from './BaseMediator';
import {Constants} from '../utils/Statics';
import Notifications from '../utils/Notifications';
import MDApplication from '../MDApplication';

export default class ApplicationMediator extends BaseMediator {
    public static MEDIATOR_NAME: string = 'applicationMediator';

    // dijon.mvc.Mediator overrides
    public listNotificationInterests(): string[] {
        return [
            Notifications.BOOT_INIT,
            Notifications.BOOT_COMPLETE,
            Notifications.TRACK_EVENT
        ]
    }

    public handleNotification(notification: INotification) {
        switch (notification.getName()) {
            case Notifications.BOOT_INIT:
                Logger.log(this, 'Notifications.BOOT_INIT');
                this.viewComponent.adjustScaleSettings();
                this.viewComponent.adjustRendererSettings();
                this.viewComponent.addPlugins();
                break;
                
            case Notifications.BOOT_COMPLETE:
                Logger.log(this, 'Notifications.BOOT_COMPLETE');
                this.game.asset.setData(this.game.cache.getJSON('assets'));
                this.viewComponent.registerModels();
                this.viewComponent.bootComplete();
                break;
            
            case Notifications.TRACK_EVENT:
                let model: AnalyticsEventModel = <AnalyticsEventModel>notification.getBody();
                if (model !== null) {
                    this.viewComponent.trackEvent(model);
                }
                break;    
        }
    }

    public notifyAppResized(): void {
        this.sendNotification(Notifications.APP_RESIZED);
    }
    
    // getter / setter
    public get viewComponent(): MDApplication {
        return <MDApplication>this._viewComponent;
    }

    public get name():string {
        return ApplicationMediator.MEDIATOR_NAME;
    }
}