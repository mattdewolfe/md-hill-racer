import {INotification} from 'dijon/interfaces';
import {Constants} from "../utils/Statics";
import BaseMediator from './BaseMediator';
import Notifications from '../utils/Notifications';
import { IMainData } from '../utils/Interfaces';
import Gameplay from '../state/Gameplay';

export default class GameplayMediator extends BaseMediator {
    public static MEDIATOR_NAME: string = 'gameMediator';
		
    public listNotificationInterests(): string[] {
        return [
            // Add Notifications to listen for.
         ];
    }

    public handleNotification(notification: INotification) {
        const noteName = notification.getName();
        const noteBody = notification.getBody();
        switch (noteName) {
            // Handle notification cases.
        }
    }

    // public methods

    public get mainData(): IMainData {
        return this.gameModel.mainData;
    }
		
    /* GET/SET */

    public get name() {
        return GameplayMediator.MEDIATOR_NAME;
    }

    public get gameplay(): Gameplay {
        return <Gameplay>this._viewComponent;
    }
}