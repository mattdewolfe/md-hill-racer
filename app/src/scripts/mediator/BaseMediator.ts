import {Mediator, CopyModel} from "dijon/mvc";
import {Application} from "dijon/application";
import Notifications from '../utils/Notifications';
import GameModel from "../model/GameModel";
import MDApplication from '../MDApplication';

export default class BaseMediator extends Mediator {
    /**
     * Attempt to retrieve an existing mediator, based on the mediators name.
     * @parem(name, string): The name of the mediator you are trying to retrieve.
     * @parem(viewComp, any): The view component you wish to bind to the mediator you are retrieving.
     * Returns the mediator in question, if found, with the new viewComponent bound to it. Null otherwise.
     */
    public static retrieveMediator(name: string, viewComp: any): BaseMediator {
        let baseMed: BaseMediator = <BaseMediator>Application.getInstance().retrieveMediator(name);
        if (baseMed !== null) {
            baseMed._viewComponent = viewComp;
        }
        return baseMed;
    }

    // Nearly every mediator will need to listen for and handle these two events.
    // The rare cases that do not simply do not call the super in their listNotificationInterests func
    public listNotificationInterests(): string[] {
        return [ 
            Notifications.APP_RESIZED
        ];
    }

    public getCopy(groupId: string, textId: string): string {
        return this.copyModel.getCopy(groupId, textId);
    }

    public getCopyArray(groupId: string, textId: string): string[] {
        return this.copyModel.getCopyGroup(groupId)[textId];
    }
		
    public requestPopup(popupId: string): void {
        this.sendNotification(Notifications.SHOW_POPUP, popupId);
    }

    public closeAllPopups(): void {
        this.sendNotification(Notifications.HIDE_ALL_POPUPS);
    }   
    
    public notifyResumeGame(): void {
        this.sendNotification(Notifications.RESUME_GAME);
    }  

    public notifyPauseGame(): void {
        this.sendNotification(Notifications.PAUSE_GAME);
    }
    
    // getter / setter
    // offer access to the GameModel and CopyModel from any mediator extending BaseMediator
    public get gameModel(): GameModel {
        return this.lcApp.gameModel;
    }

    public get copyModel(): CopyModel {
        return this.lcApp.copyModel;
    }
    
    public get lcApp(): MDApplication {
        return <MDApplication>Application.getInstance();
    }

    public get name(): string { 
        return "baseMediator_" + this.game.rnd.uuid();
    }
}