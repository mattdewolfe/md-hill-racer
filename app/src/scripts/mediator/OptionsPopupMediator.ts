import {INotification} from 'dijon/interfaces';
import {Constants, Globals} from "../utils/Statics";
import BaseMediator from './BaseMediator';
import Notifications from '../utils/Notifications';
import OptionsPopup from '../popups/OptionsPopup';

export default class OptionsPopupMediator extends BaseMediator {
    public static MEDIATOR_NAME: string = 'optionsPopupMediator';

    public handleNotification(notification: INotification) {
        const noteName = notification.getName();
        const noteBody = notification.getBody();
        switch (noteName) {
            case Notifications.APP_RESIZED:
                this.options.handleResize();
                break;    
        }
    }

    /* EVENTS */

    public updatePlayerSettings(): void {
        this.gameModel.savePlayerSettings();
    }  
    
    public quitToMenu(): void {
        this.closeAllPopups();
        this.game.transition.to(Constants.STATE_MENU);
    }    

    /* GET/SETS */

    public get name() {
        return OptionsPopupMediator.MEDIATOR_NAME;
    }

    public get options(): OptionsPopup {
        return <OptionsPopup>this._viewComponent;
    }
}