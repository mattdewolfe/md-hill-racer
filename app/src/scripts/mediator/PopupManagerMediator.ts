import {Mediator, Notification} from "dijon/mvc";
import {INotification} from "dijon/interfaces";
import BaseMediator from './BaseMediator';
import Notifications from '../utils/Notifications';
import {Constants} from '../utils/Statics';
import PopupManager from '../popups/PopupManager';
import { IPopup } from '../popups/BasePopup';

// popups
import OptionsPopup from '../popups/OptionsPopup';

export default class PopupManagerMediator extends BaseMediator {
    public static MEDIATOR_NAME: string = 'popupManagerMediator';

    private lookup: { [popupID: string]: IPopup } = {};
    private allowModalPress: boolean = false;
    private visibleList: string[];

    constructor(viewComp: any) {
        super(viewComp, true, PopupManagerMediator.MEDIATOR_NAME);
        this.visibleList = [];
    }    
    
    public listNotificationInterests(): string[] {
        return [
            Notifications.ADD_ALL_POPUPS,
            Notifications.REGISTER_POPUP,
            Notifications.SHOW_POPUP,
            Notifications.HIDE_POPUP,
            Notifications.HIDE_ALL_POPUPS
        ];
    }

    public handleNotification(notification: INotification) {
        const noteName = notification.getName();
        const noteBody = notification.getBody();       

        switch (noteName) {
            case Notifications.ADD_ALL_POPUPS:
                this.addAllPopups();
                break;
                
            case Notifications.REGISTER_POPUP:
                this.registerPopup(noteBody);
                break;

            case Notifications.SHOW_POPUP:
                let toShow: IPopup = this.getPopup(noteBody); 
                if (toShow !== null) {
                    this.manager.showModal();
                    this.manager.bringToTop(toShow);
                    toShow.show().onComplete.addOnce(() => { this.allowModalPress = true });
                    this.visibleList.push(noteBody);
                }
                break;

            case Notifications.HIDE_POPUP:
                let toHide: IPopup = this.getPopup(noteBody); 
                if (toHide !== null) {
                    toHide.hide();
                    this.updateVisibleList(noteBody);
                }
                break;

            case Notifications.HIDE_ALL_POPUPS:
                while (this.visibleList.length > 0) {
                    this.lookup[this.visibleList.shift()].hide();
                }
                this.manager.hideModal(false);
                break;    
        }
    }

    public clearLookup(): void {
        this.lookup = {};
    }

    public closeTop(): void {
        this.sendNotification(Notifications.HIDE_POPUP, this.visibleList[this.visibleList.length - 1]);
    }

    public modalPressed(): void {
        if (this.popupSelectionRequired === true || this.allowModalPress === false) {
            return;
        }
        this.closeTop();
    }   
    
    public hasPopupWithId(popupID: string): boolean {
        return this.getPopup(popupID) ? true : false;
    }

    // private methods
    private addAllPopups(): void {
        let a = new OptionsPopup();
        
        this.addAllPopups = () => { };
    }
    
    private getPopup(popupID: string): IPopup {
        return this.lookup[popupID] || null;
    }

    private registerPopup(popup: IPopup): void {
        this.lookup[popup.id] = popup;
        this.manager.addModal();
        this.manager.add(popup);
    }

    private updateVisibleList(toRemove: string): void {
        let index = this.visibleList.lastIndexOf(toRemove);
        if (index != -1) {
            let hold = this.visibleList[this.visibleList.length - 1];
            this.visibleList[index] = this.visibleList[this.visibleList.length - 1];
            this.visibleList[index] = hold;
            this.visibleList.pop();
        }
        if (this.visibleList.length === 0 || index === -1) {
            this.manager.hideModal();
            this.allowModalPress = false;
        }
    }   

    /* GET/SETS */
    
    private get manager(): PopupManager {
        return <PopupManager>this.viewComponent;
    }

    private get popupSelectionRequired(): boolean {
        return false;
    }
    
    public get name(): string {
        return PopupManagerMediator.MEDIATOR_NAME;
    }
}