import {INotification} from 'dijon/interfaces';
import {Constants} from "../utils/Statics";
import BaseMediator from './BaseMediator';
import Notifications from '../utils/Notifications';
import { IMainData } from '../utils/Interfaces';

export default class MenuMediator extends BaseMediator {
    public static MEDIATOR_NAME: string = 'menuMediator';
		
    public listNotificationInterests(): string[] {
        return [
            // Add Notifications to listen for.
         ];
    }

    public handleNotification(notification: INotification) {
        const noteName = notification.getName();
        const noteBody = notification.getBody();
        switch (noteName) {
            // Handle notification cases.
        }
    }

    // public methods
    public addPopups(): void{
        this.sendNotification(Notifications.ADD_ALL_POPUPS);
    }
		
    // getter / setter
    public get name() {
        return MenuMediator.MEDIATOR_NAME;
    }
}