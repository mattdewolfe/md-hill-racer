import {Model} from 'dijon/mvc';
import { IPlayerData, IMainData } from '../utils/Interfaces';
import {Constants} from '../utils/Statics';

export default class GameModel extends Model {
    public static MODEL_NAME: string = "gameModel";

    // This stores players audio settings as well as achievements.
    protected playerData: IPlayerData = null;

    /* PLAYER DATA */

    public resetSaveData(): void {
        this.playerData = this.createPlayerData();
        this.updateAppSettings();
        this.savePlayerData();
    }   
    
    public updateAppSettings(): void {
        this.game.audio.spriteVolume = this.playerData.sfxVolume;
        this.game.audio.soundVolume = this.playerData.musicVolume;
    }

    public loadPlayerData(): void {
        //console.warn("Player Data currently not loaded, to avoid issues with data key changes");
        this.playerData = this.game.storage.getFromLocalStorage(Constants.SAVE_DATA_KEY, true);
        
        if (this.playerData == null) {
            this.playerData = this.createPlayerData();
        }
    
        this.updateAppSettings();
    }

    protected savePlayerData(): void {
        this.game.storage.saveToLocalStorage(Constants.SAVE_DATA_KEY, this.playerData);
    }

    public savePlayerSettings(): void {
        this.playerData.sfxVolume = this.game.audio.spriteVolume;
        this.playerData.musicVolume = this.game.audio.soundVolume;
        this.savePlayerData();
    }   
    
    protected createPlayerData(): IPlayerData {
        let data = <IPlayerData>new Object();
        
        data.sfxVolume = 1;
        data.musicVolume = 1;
        data.cash = 0;
        data.upgradeTiers = [0, 0, 0];

        return data;
    }

    /* GET/SETS */

    public get name(): string {
        return GameModel.MODEL_NAME;
    }

    public get mainData(): IMainData {
        return <IMainData>this._data['main'];
    }    
}