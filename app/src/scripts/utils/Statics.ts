export class Constants {
    // States
    static STATE_BOOT: string = 'boot';
    static STATE_MENU: string = 'menu';
    static STATE_GAME: string = 'game';
    
    // Keys
    static SAVE_DATA_KEY: string = 'saveData';

    // Fonts
    static FONT_PRESS_START: string = "'Press Start 2P', cursive";

    // Popups
    static POPUP_OPTIONS: string = 'optionsPopup';
    static POPUP_HELP: string = 'helpPopup';

    // Colours
    static COLOR_MENU_COPY: string = '#00ff00';
    static COLOR_SLIDER_LABEL: string = '#ff970f';
}

// Use this to house any global static variables you may want.
// IE, a global SFX mute or volume property.
export class Globals {
    static SCALE_RATIO: number = 1;
}