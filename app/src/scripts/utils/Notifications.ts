export default class Notifications {
    static BOOT_INIT: string = 'bootInit';
    static BOOT_COMPLETE: string = 'bootComplete';
    static TRACK_EVENT: string = 'trackEvent';
    static APP_RESIZED: string = 'appResized';
    
    static PAUSE_GAME: string = 'pauseGame';
    static RESUME_GAME: string = 'resumeGame';
    static START_GAME: string = 'startGame';

    static REGISTER_POPUP: string = 'registerPopup;'
    static HIDE_POPUP: string = 'hidePopup';
    static SHOW_POPUP: string = 'showPopup';
    static ADD_ALL_POPUPS: string = 'addAllPopups';
    static HIDE_ALL_POPUPS: string = 'hideAllPopups';
    
}