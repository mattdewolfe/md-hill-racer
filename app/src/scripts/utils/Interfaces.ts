export interface IPlayerData {
    sfxVolume: number;
    musicVolume: number;
    cash: number;
    upgradeTiers: number[];
}

export interface IMainData {

}