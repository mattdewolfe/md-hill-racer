var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
System.register("utils/Notifications", [], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Notifications;
    return {
        setters:[],
        execute: function() {
            Notifications = (function () {
                function Notifications() {
                }
                Notifications.BOOT_INIT = 'bootInit';
                Notifications.BOOT_COMPLETE = 'bootComplete';
                Notifications.TRACK_EVENT = 'trackEvent';
                Notifications.APP_RESIZED = 'appResized';
                Notifications.PAUSE_GAME = 'pauseGame';
                Notifications.RESUME_GAME = 'resumeGame';
                Notifications.START_GAME = 'startGame';
                Notifications.REGISTER_POPUP = 'registerPopup;';
                Notifications.HIDE_POPUP = 'hidePopup';
                Notifications.SHOW_POPUP = 'showPopup';
                Notifications.ADD_ALL_POPUPS = 'addAllPopups';
                Notifications.HIDE_ALL_POPUPS = 'hideAllPopups';
                return Notifications;
            }());
            exports_1("default", Notifications);
        }
    }
});
System.register("utils/Interfaces", [], function(exports_2, context_2) {
    "use strict";
    var __moduleName = context_2 && context_2.id;
    return {
        setters:[],
        execute: function() {
        }
    }
});
System.register("utils/Statics", [], function(exports_3, context_3) {
    "use strict";
    var __moduleName = context_3 && context_3.id;
    var Constants, Globals;
    return {
        setters:[],
        execute: function() {
            Constants = (function () {
                function Constants() {
                }
                Constants.STATE_BOOT = 'boot';
                Constants.STATE_MENU = 'menu';
                Constants.STATE_GAME = 'game';
                Constants.SAVE_DATA_KEY = 'saveData';
                Constants.FONT_PRESS_START = "'Press Start 2P', cursive";
                Constants.POPUP_OPTIONS = 'optionsPopup';
                Constants.POPUP_HELP = 'helpPopup';
                Constants.COLOR_MENU_COPY = '#00ff00';
                Constants.COLOR_SLIDER_LABEL = '#ff970f';
                return Constants;
            }());
            exports_3("Constants", Constants);
            Globals = (function () {
                function Globals() {
                }
                Globals.SCALE_RATIO = 1;
                return Globals;
            }());
            exports_3("Globals", Globals);
        }
    }
});
System.register("model/GameModel", ['dijon/mvc', "utils/Statics"], function(exports_4, context_4) {
    "use strict";
    var __moduleName = context_4 && context_4.id;
    var mvc_1, Statics_1;
    var GameModel;
    return {
        setters:[
            function (mvc_1_1) {
                mvc_1 = mvc_1_1;
            },
            function (Statics_1_1) {
                Statics_1 = Statics_1_1;
            }],
        execute: function() {
            GameModel = (function (_super) {
                __extends(GameModel, _super);
                function GameModel() {
                    _super.apply(this, arguments);
                    this.playerData = null;
                }
                GameModel.prototype.resetSaveData = function () {
                    this.playerData = this.createPlayerData();
                    this.updateAppSettings();
                    this.savePlayerData();
                };
                GameModel.prototype.updateAppSettings = function () {
                    this.game.audio.spriteVolume = this.playerData.sfxVolume;
                    this.game.audio.soundVolume = this.playerData.musicVolume;
                };
                GameModel.prototype.loadPlayerData = function () {
                    this.playerData = this.game.storage.getFromLocalStorage(Statics_1.Constants.SAVE_DATA_KEY, true);
                    if (this.playerData == null) {
                        this.playerData = this.createPlayerData();
                    }
                    this.updateAppSettings();
                };
                GameModel.prototype.savePlayerData = function () {
                    this.game.storage.saveToLocalStorage(Statics_1.Constants.SAVE_DATA_KEY, this.playerData);
                };
                GameModel.prototype.savePlayerSettings = function () {
                    this.playerData.sfxVolume = this.game.audio.spriteVolume;
                    this.playerData.musicVolume = this.game.audio.soundVolume;
                    this.savePlayerData();
                };
                GameModel.prototype.createPlayerData = function () {
                    var data = new Object();
                    data.sfxVolume = 1;
                    data.musicVolume = 1;
                    data.cash = 0;
                    data.upgradeTiers = [0, 0, 0];
                    return data;
                };
                Object.defineProperty(GameModel.prototype, "name", {
                    get: function () {
                        return GameModel.MODEL_NAME;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(GameModel.prototype, "mainData", {
                    get: function () {
                        return this._data['main'];
                    },
                    enumerable: true,
                    configurable: true
                });
                GameModel.MODEL_NAME = "gameModel";
                return GameModel;
            }(mvc_1.Model));
            exports_4("default", GameModel);
        }
    }
});
System.register("mediator/BaseMediator", ["dijon/mvc", "dijon/application", "utils/Notifications"], function(exports_5, context_5) {
    "use strict";
    var __moduleName = context_5 && context_5.id;
    var mvc_2, application_1, Notifications_1;
    var BaseMediator;
    return {
        setters:[
            function (mvc_2_1) {
                mvc_2 = mvc_2_1;
            },
            function (application_1_1) {
                application_1 = application_1_1;
            },
            function (Notifications_1_1) {
                Notifications_1 = Notifications_1_1;
            }],
        execute: function() {
            BaseMediator = (function (_super) {
                __extends(BaseMediator, _super);
                function BaseMediator() {
                    _super.apply(this, arguments);
                }
                BaseMediator.retrieveMediator = function (name, viewComp) {
                    var baseMed = application_1.Application.getInstance().retrieveMediator(name);
                    if (baseMed !== null) {
                        baseMed._viewComponent = viewComp;
                    }
                    return baseMed;
                };
                BaseMediator.prototype.listNotificationInterests = function () {
                    return [
                        Notifications_1.default.APP_RESIZED
                    ];
                };
                BaseMediator.prototype.getCopy = function (groupId, textId) {
                    return this.copyModel.getCopy(groupId, textId);
                };
                BaseMediator.prototype.getCopyArray = function (groupId, textId) {
                    return this.copyModel.getCopyGroup(groupId)[textId];
                };
                BaseMediator.prototype.requestPopup = function (popupId) {
                    this.sendNotification(Notifications_1.default.SHOW_POPUP, popupId);
                };
                BaseMediator.prototype.closeAllPopups = function () {
                    this.sendNotification(Notifications_1.default.HIDE_ALL_POPUPS);
                };
                BaseMediator.prototype.notifyResumeGame = function () {
                    this.sendNotification(Notifications_1.default.RESUME_GAME);
                };
                BaseMediator.prototype.notifyPauseGame = function () {
                    this.sendNotification(Notifications_1.default.PAUSE_GAME);
                };
                Object.defineProperty(BaseMediator.prototype, "gameModel", {
                    get: function () {
                        return this.lcApp.gameModel;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BaseMediator.prototype, "copyModel", {
                    get: function () {
                        return this.lcApp.copyModel;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BaseMediator.prototype, "lcApp", {
                    get: function () {
                        return application_1.Application.getInstance();
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(BaseMediator.prototype, "name", {
                    get: function () {
                        return "baseMediator_" + this.game.rnd.uuid();
                    },
                    enumerable: true,
                    configurable: true
                });
                return BaseMediator;
            }(mvc_2.Mediator));
            exports_5("default", BaseMediator);
        }
    }
});
System.register("mediator/ApplicationMediator", ["dijon/utils", "mediator/BaseMediator", "utils/Notifications"], function(exports_6, context_6) {
    "use strict";
    var __moduleName = context_6 && context_6.id;
    var utils_1, BaseMediator_1, Notifications_2;
    var ApplicationMediator;
    return {
        setters:[
            function (utils_1_1) {
                utils_1 = utils_1_1;
            },
            function (BaseMediator_1_1) {
                BaseMediator_1 = BaseMediator_1_1;
            },
            function (Notifications_2_1) {
                Notifications_2 = Notifications_2_1;
            }],
        execute: function() {
            ApplicationMediator = (function (_super) {
                __extends(ApplicationMediator, _super);
                function ApplicationMediator() {
                    _super.apply(this, arguments);
                }
                ApplicationMediator.prototype.listNotificationInterests = function () {
                    return [
                        Notifications_2.default.BOOT_INIT,
                        Notifications_2.default.BOOT_COMPLETE,
                        Notifications_2.default.TRACK_EVENT
                    ];
                };
                ApplicationMediator.prototype.handleNotification = function (notification) {
                    switch (notification.getName()) {
                        case Notifications_2.default.BOOT_INIT:
                            utils_1.Logger.log(this, 'Notifications.BOOT_INIT');
                            this.viewComponent.adjustScaleSettings();
                            this.viewComponent.adjustRendererSettings();
                            this.viewComponent.addPlugins();
                            break;
                        case Notifications_2.default.BOOT_COMPLETE:
                            utils_1.Logger.log(this, 'Notifications.BOOT_COMPLETE');
                            this.game.asset.setData(this.game.cache.getJSON('assets'));
                            this.viewComponent.registerModels();
                            this.viewComponent.bootComplete();
                            break;
                        case Notifications_2.default.TRACK_EVENT:
                            var model = notification.getBody();
                            if (model !== null) {
                                this.viewComponent.trackEvent(model);
                            }
                            break;
                    }
                };
                ApplicationMediator.prototype.notifyAppResized = function () {
                    this.sendNotification(Notifications_2.default.APP_RESIZED);
                };
                Object.defineProperty(ApplicationMediator.prototype, "viewComponent", {
                    get: function () {
                        return this._viewComponent;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(ApplicationMediator.prototype, "name", {
                    get: function () {
                        return ApplicationMediator.MEDIATOR_NAME;
                    },
                    enumerable: true,
                    configurable: true
                });
                ApplicationMediator.MEDIATOR_NAME = 'applicationMediator';
                return ApplicationMediator;
            }(BaseMediator_1.default));
            exports_6("default", ApplicationMediator);
        }
    }
});
System.register("state/BaseState", ["dijon/core"], function(exports_7, context_7) {
    "use strict";
    var __moduleName = context_7 && context_7.id;
    var core_1;
    var BaseState;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            }],
        execute: function() {
            BaseState = (function (_super) {
                __extends(BaseState, _super);
                function BaseState() {
                    _super.apply(this, arguments);
                }
                BaseState.prototype.afterBuild = function () {
                    this.resume();
                };
                return BaseState;
            }(core_1.State));
            exports_7("default", BaseState);
        }
    }
});
System.register("mediator/BootMediator", ["mediator/BaseMediator", "utils/Notifications"], function(exports_8, context_8) {
    "use strict";
    var __moduleName = context_8 && context_8.id;
    var BaseMediator_2, Notifications_3;
    var BootMediator;
    return {
        setters:[
            function (BaseMediator_2_1) {
                BaseMediator_2 = BaseMediator_2_1;
            },
            function (Notifications_3_1) {
                Notifications_3 = Notifications_3_1;
            }],
        execute: function() {
            BootMediator = (function (_super) {
                __extends(BootMediator, _super);
                function BootMediator() {
                    _super.apply(this, arguments);
                }
                BootMediator.prototype.onRegister = function () {
                    this.sendNotification(Notifications_3.default.BOOT_INIT);
                };
                BootMediator.prototype.bootComplete = function () {
                    this.sendNotification(Notifications_3.default.BOOT_COMPLETE);
                };
                Object.defineProperty(BootMediator.prototype, "name", {
                    get: function () {
                        return BootMediator.MEDIATOR_NAME;
                    },
                    enumerable: true,
                    configurable: true
                });
                BootMediator.MEDIATOR_NAME = 'bootMediator';
                return BootMediator;
            }(BaseMediator_2.default));
            exports_8("default", BootMediator);
        }
    }
});
System.register("state/Boot", ["state/BaseState", "mediator/BootMediator"], function(exports_9, context_9) {
    "use strict";
    var __moduleName = context_9 && context_9.id;
    var BaseState_1, BootMediator_1;
    var Boot;
    return {
        setters:[
            function (BaseState_1_1) {
                BaseState_1 = BaseState_1_1;
            },
            function (BootMediator_1_1) {
                BootMediator_1 = BootMediator_1_1;
            }],
        execute: function() {
            Boot = (function (_super) {
                __extends(Boot, _super);
                function Boot() {
                    _super.apply(this, arguments);
                }
                Boot.prototype.init = function () {
                    this._mediator = new BootMediator_1.default(this);
                };
                Boot.prototype.preload = function () {
                    if (window['version'] !== undefined) {
                        this.game.asset.cacheBustVersion = '@@version';
                    }
                    this.game.asset.loadJSON('game_data');
                    this.game.asset.loadJSON('assets');
                    this.game.asset.loadJSON('copy');
                    this.game.asset.loadPhysics('physics');
                };
                Boot.prototype.buildInterface = function () {
                    this.mediator.bootComplete();
                };
                Object.defineProperty(Boot.prototype, "mediator", {
                    get: function () {
                        return this._mediator;
                    },
                    enumerable: true,
                    configurable: true
                });
                return Boot;
            }(BaseState_1.default));
            exports_9("default", Boot);
        }
    }
});
System.register("mediator/GameplayMediator", ["mediator/BaseMediator"], function(exports_10, context_10) {
    "use strict";
    var __moduleName = context_10 && context_10.id;
    var BaseMediator_3;
    var GameplayMediator;
    return {
        setters:[
            function (BaseMediator_3_1) {
                BaseMediator_3 = BaseMediator_3_1;
            }],
        execute: function() {
            GameplayMediator = (function (_super) {
                __extends(GameplayMediator, _super);
                function GameplayMediator() {
                    _super.apply(this, arguments);
                }
                GameplayMediator.prototype.listNotificationInterests = function () {
                    return [];
                };
                GameplayMediator.prototype.handleNotification = function (notification) {
                    var noteName = notification.getName();
                    var noteBody = notification.getBody();
                    switch (noteName) {
                    }
                };
                Object.defineProperty(GameplayMediator.prototype, "mainData", {
                    get: function () {
                        return this.gameModel.mainData;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(GameplayMediator.prototype, "name", {
                    get: function () {
                        return GameplayMediator.MEDIATOR_NAME;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(GameplayMediator.prototype, "gameplay", {
                    get: function () {
                        return this._viewComponent;
                    },
                    enumerable: true,
                    configurable: true
                });
                GameplayMediator.MEDIATOR_NAME = 'gameMediator';
                return GameplayMediator;
            }(BaseMediator_3.default));
            exports_10("default", GameplayMediator);
        }
    }
});
System.register("actors/Actor", ['dijon/display'], function(exports_11, context_11) {
    "use strict";
    var __moduleName = context_11 && context_11.id;
    var display_1;
    var Actor;
    return {
        setters:[
            function (display_1_1) {
                display_1 = display_1_1;
            }],
        execute: function() {
            Actor = (function (_super) {
                __extends(Actor, _super);
                function Actor(x, y, key, frame, bodyKey, kinematic) {
                    if (kinematic === void 0) { kinematic = false; }
                    _super.call(this, x, y, key, frame);
                    this.game.physics.p2.enable(this);
                    this.p2Body.clearShapes();
                    this.p2Body.loadPolygon("physics", bodyKey);
                    this.p2Body.kinematic = kinematic;
                }
                Object.defineProperty(Actor.prototype, "p2Body", {
                    get: function () {
                        return this.body;
                    },
                    enumerable: true,
                    configurable: true
                });
                return Actor;
            }(display_1.Sprite));
            exports_11("default", Actor);
        }
    }
});
System.register("actors/Vehicle", ["actors/Actor"], function(exports_12, context_12) {
    "use strict";
    var __moduleName = context_12 && context_12.id;
    var Actor_1;
    var Vehicle;
    return {
        setters:[
            function (Actor_1_1) {
                Actor_1 = Actor_1_1;
            }],
        execute: function() {
            Vehicle = (function (_super) {
                __extends(Vehicle, _super);
                function Vehicle(x, y, key, frame, bodyKey) {
                    _super.call(this, x, y, key, frame, bodyKey, false);
                    this._maxSpeed = 300;
                    this._accel = 5;
                    this._canBounce = true;
                    this._rotateSpeed = 0;
                    this._wheels = this.game.add.dGroup();
                    this._wheelMaterial = this.game.physics.p2.createMaterial('wheelMaterial');
                    this._cursors = this.game.input.keyboard.createCursorKeys();
                    this._initWheel(55);
                    this._initWheel(-52);
                }
                Vehicle.prototype.update = function () {
                    if (this._cursors.left.isDown) {
                        this._accelBackward();
                    }
                    else if (this._cursors.right.isDown) {
                        this._accelForward();
                    }
                    else {
                        this._decelerate();
                    }
                    this._updateWheels();
                };
                Vehicle.prototype._decelerate = function () {
                    this._rotateSpeed = Math.floor(this._rotateSpeed * 0.95);
                };
                Vehicle.prototype._accelForward = function () {
                    this._rotateSpeed -= this._accel;
                    if (this._rotateSpeed < -this._maxSpeed) {
                        this._rotateSpeed = -this._maxSpeed;
                    }
                };
                Vehicle.prototype._accelBackward = function () {
                    this._rotateSpeed += this._accel;
                    if (this._rotateSpeed > this._maxSpeed * 0.5) {
                        this._rotateSpeed = this._maxSpeed * 0.5;
                    }
                };
                Vehicle.prototype._updateWheels = function () {
                    var _this = this;
                    this._wheels.children.forEach(function (wheel) {
                        wheel.body.rotateLeft(_this._rotateSpeed);
                    });
                };
                Vehicle.prototype._initWheel = function (offsetX, driveWheel) {
                    if (driveWheel === void 0) { driveWheel = true; }
                    var offsetY = 24;
                    var wheel = this.game.add.sprite(this.position.x + offsetX, this.position.y + offsetY, 'gameplay', 'wheel');
                    this.game.physics.p2.enable(wheel);
                    wheel.body.clearShapes();
                    wheel.body.addCircle(15.5);
                    var maxForce = 50;
                    var rev = this.game.physics.p2.createRevoluteConstraint(this.body, [offsetX, offsetY], wheel.body, [0, 0], maxForce);
                    if (driveWheel === true) {
                        this._wheels.add(wheel);
                    }
                    wheel.body.onBeginContact.add(this.onWheelContact, this);
                    wheel.body.setMaterial(this._wheelMaterial);
                };
                Vehicle.prototype.onWheelContact = function (phaserBody, p2Body) {
                    if ((phaserBody === null && p2Body.id == 4)
                        || (phaserBody && phaserBody.sprite.key == "hill")) {
                        this._canBounce = true;
                    }
                };
                Object.defineProperty(Vehicle.prototype, "wheelMaterial", {
                    get: function () {
                        return this._wheelMaterial;
                    },
                    enumerable: true,
                    configurable: true
                });
                return Vehicle;
            }(Actor_1.default));
            exports_12("default", Vehicle);
        }
    }
});
System.register("actors/Rock", ["actors/Actor"], function(exports_13, context_13) {
    "use strict";
    var __moduleName = context_13 && context_13.id;
    var Actor_2;
    var Rock;
    return {
        setters:[
            function (Actor_2_1) {
                Actor_2 = Actor_2_1;
            }],
        execute: function() {
            Rock = (function (_super) {
                __extends(Rock, _super);
                function Rock() {
                    _super.apply(this, arguments);
                }
                return Rock;
            }(Actor_2.default));
            exports_13("default", Rock);
        }
    }
});
System.register("state/Gameplay", ["state/BaseState", "mediator/GameplayMediator", "actors/Vehicle", "actors/Actor"], function(exports_14, context_14) {
    "use strict";
    var __moduleName = context_14 && context_14.id;
    var BaseState_2, GameplayMediator_1, Vehicle_1, Actor_3;
    var Gameplay;
    return {
        setters:[
            function (BaseState_2_1) {
                BaseState_2 = BaseState_2_1;
            },
            function (GameplayMediator_1_1) {
                GameplayMediator_1 = GameplayMediator_1_1;
            },
            function (Vehicle_1_1) {
                Vehicle_1 = Vehicle_1_1;
            },
            function (Actor_3_1) {
                Actor_3 = Actor_3_1;
            }],
        execute: function() {
            Gameplay = (function (_super) {
                __extends(Gameplay, _super);
                function Gameplay() {
                    _super.apply(this, arguments);
                }
                Gameplay.prototype.init = function () {
                    this._mediator = GameplayMediator_1.default.retrieveMediator(GameplayMediator_1.default.MEDIATOR_NAME, this);
                    if (this._mediator === null) {
                        this._mediator = new GameplayMediator_1.default(this);
                    }
                    this.game.world.setBounds(0, 0, this.game.width * 3, this.game.height);
                    this.game.physics.startSystem(Phaser.Physics.P2JS);
                    this.game.physics.p2.gravity.y = 300;
                    this._worldMat = this.game.physics.p2.createMaterial("worldMaterial");
                };
                Gameplay.prototype.listBuildSequence = function () {
                    return [
                        this._addBG,
                        this._addTerrain,
                        this._addVehicle,
                        this._setCollisions
                    ];
                };
                Gameplay.prototype._addBG = function () {
                };
                Gameplay.prototype._addTerrain = function () {
                    for (var i = 0; i < 6; i++) {
                        var hill = new Actor_3.default(150 + (i * 350) + (Math.random() * 50), this.game.height - 30, 'gameplay', 'hill', 'hill', true);
                        hill.body.setMaterial(this._worldMat);
                        this.add.existing(hill);
                    }
                };
                Gameplay.prototype._addVehicle = function () {
                    this._truck = new Vehicle_1.default(100, this.game.height - 250, 'gameplay', 'truck', 'truck');
                    this.add.existing(this._truck);
                    this.game.camera.follow(this._truck);
                };
                Gameplay.prototype._setCollisions = function () {
                    var contactMaterial = this.game.physics.p2.createContactMaterial(this._truck.wheelMaterial, this._worldMat);
                    contactMaterial.friction = 1000;
                    contactMaterial.restitution = 0.4;
                };
                Object.defineProperty(Gameplay.prototype, "mediator", {
                    get: function () {
                        return this._mediator;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Gameplay.prototype, "preloadID", {
                    get: function () {
                        return 'game';
                    },
                    enumerable: true,
                    configurable: true
                });
                return Gameplay;
            }(BaseState_2.default));
            exports_14("default", Gameplay);
        }
    }
});
System.register("mediator/MenuMediator", ["mediator/BaseMediator", "utils/Notifications"], function(exports_15, context_15) {
    "use strict";
    var __moduleName = context_15 && context_15.id;
    var BaseMediator_4, Notifications_4;
    var MenuMediator;
    return {
        setters:[
            function (BaseMediator_4_1) {
                BaseMediator_4 = BaseMediator_4_1;
            },
            function (Notifications_4_1) {
                Notifications_4 = Notifications_4_1;
            }],
        execute: function() {
            MenuMediator = (function (_super) {
                __extends(MenuMediator, _super);
                function MenuMediator() {
                    _super.apply(this, arguments);
                }
                MenuMediator.prototype.listNotificationInterests = function () {
                    return [];
                };
                MenuMediator.prototype.handleNotification = function (notification) {
                    var noteName = notification.getName();
                    var noteBody = notification.getBody();
                    switch (noteName) {
                    }
                };
                MenuMediator.prototype.addPopups = function () {
                    this.sendNotification(Notifications_4.default.ADD_ALL_POPUPS);
                };
                Object.defineProperty(MenuMediator.prototype, "name", {
                    get: function () {
                        return MenuMediator.MEDIATOR_NAME;
                    },
                    enumerable: true,
                    configurable: true
                });
                MenuMediator.MEDIATOR_NAME = 'menuMediator';
                return MenuMediator;
            }(BaseMediator_4.default));
            exports_15("default", MenuMediator);
        }
    }
});
System.register("state/Menu", ["state/BaseState", "mediator/MenuMediator", "utils/Statics", 'dijon/display'], function(exports_16, context_16) {
    "use strict";
    var __moduleName = context_16 && context_16.id;
    var BaseState_3, MenuMediator_1, Statics_2, display_2;
    var Menu;
    return {
        setters:[
            function (BaseState_3_1) {
                BaseState_3 = BaseState_3_1;
            },
            function (MenuMediator_1_1) {
                MenuMediator_1 = MenuMediator_1_1;
            },
            function (Statics_2_1) {
                Statics_2 = Statics_2_1;
            },
            function (display_2_1) {
                display_2 = display_2_1;
            }],
        execute: function() {
            Menu = (function (_super) {
                __extends(Menu, _super);
                function Menu() {
                    _super.apply(this, arguments);
                }
                Menu.prototype.init = function () {
                    this._mediator = MenuMediator_1.default.retrieveMediator(MenuMediator_1.default.MEDIATOR_NAME, this);
                    if (this._mediator === null) {
                        this._mediator = new MenuMediator_1.default(this);
                    }
                };
                Menu.prototype.listBuildSequence = function () {
                    return [
                        this._addBG,
                        this._addPopups,
                        this._addButtons
                    ];
                };
                Menu.prototype.afterBuild = function () {
                    _super.prototype.afterBuild.call(this);
                    this.resume();
                };
                Menu.prototype._addBG = function () {
                    var title = this.add.dText(this.game.centerX, this.game.centerY - 200, this.getCopyByID('title'), Statics_2.Constants.FONT_PRESS_START, 30, '#ffffff');
                    title.centerPivot();
                };
                Menu.prototype._addButtons = function () {
                    var startBtn = new display_2.LabelledButton(this.game.centerX, this.game.centerY, this.startGame, this, 'menu', 'red_btn_normal', 'red_btn_down', 'red_btn_over', 'red_btn_normal');
                    startBtn.addLabel(this.getCopyByID('start_game'), 14, Statics_2.Constants.FONT_PRESS_START);
                    startBtn.centerPivot();
                    this.add.existing(startBtn);
                };
                Menu.prototype._addPopups = function () {
                    this.mediator.addPopups();
                };
                Menu.prototype.startGame = function () {
                    this.game.transition.to(Statics_2.Constants.STATE_GAME);
                };
                Menu.prototype.getCopyByID = function (id) {
                    return this.mediator.copyModel.getCopy('menu', id);
                };
                Object.defineProperty(Menu.prototype, "mediator", {
                    get: function () {
                        return this._mediator;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Menu.prototype, "preloadID", {
                    get: function () {
                        return 'menu';
                    },
                    enumerable: true,
                    configurable: true
                });
                return Menu;
            }(BaseState_3.default));
            exports_16("default", Menu);
        }
    }
});
System.register("ui/PieProgressBar", ['dijon/application'], function(exports_17, context_17) {
    "use strict";
    var __moduleName = context_17 && context_17.id;
    var application_2;
    var PieProgressBar;
    return {
        setters:[
            function (application_2_1) {
                application_2 = application_2_1;
            }],
        execute: function() {
            PieProgressBar = (function (_super) {
                __extends(PieProgressBar, _super);
                function PieProgressBar(x, y, radius, weight, color, angle) {
                    if (radius === void 0) { radius = 2; }
                    if (weight === void 0) { weight = 0; }
                    if (color === void 0) { color = '#ffffff'; }
                    if (angle === void 0) { angle = -90; }
                    _super.call(this, application_2.Application.getInstance().game, x, y);
                    this.anchor.set(0.5);
                    this._weight = Phaser.Math.clamp(weight, 0, 1);
                    this._radius = radius;
                    this.angle = angle;
                    this._color = color;
                    if (this._weight > 0) {
                        this._bmp = this.game.add.bitmapData((this._radius * 2) + (this._weight * (this._radius * 0.6)), (this._radius * 2) + (this._weight * (this._radius * 0.6)));
                    }
                    else {
                        this._bmp = this.game.add.bitmapData(radius * 2, radius * 2);
                    }
                    this.loadTexture(this._bmp);
                    this.updateProgress(0);
                }
                PieProgressBar.prototype.updateProgress = function (percent) {
                    var progress = percent;
                    this._bmp.clear();
                    if (this._weight > 0) {
                        progress = Phaser.Math.clamp(percent, 0, 0.9999);
                        this._bmp.ctx.fillStyle = this._color;
                        this._bmp.ctx.strokeStyle = this._color;
                        this._bmp.ctx.lineWidth = this._weight * this._radius;
                        this._bmp.ctx.beginPath();
                        this._bmp.ctx.arc(this._bmp.width * 0.5, this._bmp.height * 0.5, this._radius - 15, 0, (Math.PI * 2) * progress, false);
                        this._bmp.ctx.stroke();
                    }
                    else {
                        progress = 1 - Phaser.Math.clamp(percent, 0.0001, 1);
                        this._bmp.ctx.fillStyle = this._color;
                        this._bmp.ctx.beginPath();
                        this._bmp.ctx.arc(this._radius, this._radius, this._radius, 0, (Math.PI * 2) * progress, true);
                        this._bmp.ctx.lineTo(this._radius, this._radius);
                        this._bmp.ctx.closePath();
                        this._bmp.ctx.fill();
                    }
                    this._bmp.dirty = true;
                };
                Object.defineProperty(PieProgressBar.prototype, "radius", {
                    get: function () {
                        return this._radius;
                    },
                    set: function (value) {
                        this._radius = (value > 0 ? value : 0);
                        this._bmp.resize(this._radius * 2, this._radius * 2);
                    },
                    enumerable: true,
                    configurable: true
                });
                return PieProgressBar;
            }(Phaser.Sprite));
            exports_17("default", PieProgressBar);
        }
    }
});
System.register("ui/Preloader", ['dijon/display', "ui/PieProgressBar"], function(exports_18, context_18) {
    "use strict";
    var __moduleName = context_18 && context_18.id;
    var display_3, PieProgressBar_1;
    var Preloader;
    return {
        setters:[
            function (display_3_1) {
                display_3 = display_3_1;
            },
            function (PieProgressBar_1_1) {
                PieProgressBar_1 = PieProgressBar_1_1;
            }],
        execute: function() {
            Preloader = (function (_super) {
                __extends(Preloader, _super);
                function Preloader(x, y, name) {
                    _super.call(this, x, y, name, true);
                    this.transitionInComplete = new Phaser.Signal();
                    this.transitionOutComplete = new Phaser.Signal();
                    this.init();
                    this.buildInterface();
                }
                Preloader.prototype.buildInterface = function () {
                    var gfx = this.game.add.graphics();
                    gfx.beginFill(0xaaaaaa, 1);
                    gfx.drawRect(0, 0, this.game.width, this.game.height);
                    gfx.endFill();
                    this._wiper = this.addInternal.image(0, 0, gfx.generateTexture());
                    this.game.world.remove(gfx, true);
                    this.alpha = 0;
                    this.visible = false;
                    this._progressBar1 = new PieProgressBar_1.default(this.game.width * 0.5, this.game.height * 0.75, 50);
                    this.addInternal.existing(this._progressBar1);
                    this._inTween = this.game.add.tween(this).to({ alpha: 1 }, 300, Phaser.Easing.Quadratic.Out);
                    this._outTween = this.game.add.tween(this).to({ alpha: 0 }, 200, Phaser.Easing.Quadratic.In);
                    this._inTween.onComplete.add(this._in, this);
                    this._outTween.onComplete.add(this._out, this);
                };
                Preloader.prototype.loadStart = function () {
                    this._progressBar1.updateProgress(0);
                };
                Preloader.prototype.loadProgress = function (progress) {
                    this._progressBar1.updateProgress(progress / 100);
                };
                Preloader.prototype.loadComplete = function () {
                };
                Preloader.prototype.transitionIn = function () {
                    this.visible = true;
                    this._inTween.start();
                };
                Preloader.prototype.transitionOut = function () {
                    this._outTween.start();
                };
                Preloader.prototype._in = function () {
                    this.transitionInComplete.dispatch();
                };
                Preloader.prototype._out = function () {
                    this.visible = false;
                    this.transitionOutComplete.dispatch();
                };
                return Preloader;
            }(display_3.Group));
            exports_18("default", Preloader);
        }
    }
});
System.register("MDApplication", ["dijon/application", "dijon/core", "dijon/utils", "dijon/mvc", "mediator/ApplicationMediator", "utils/Statics", "utils/Notifications", "state/Boot", "state/Gameplay", "state/Menu", "model/GameModel", "ui/Preloader"], function(exports_19, context_19) {
    "use strict";
    var __moduleName = context_19 && context_19.id;
    var application_3, core_2, utils_2, mvc_3, ApplicationMediator_1, Statics_3, Notifications_5, Boot_1, Gameplay_1, Menu_1, GameModel_1, Preloader_1;
    var EInitOrientation, EDeviceOrientation, EBootState, MDApplication;
    return {
        setters:[
            function (application_3_1) {
                application_3 = application_3_1;
            },
            function (core_2_1) {
                core_2 = core_2_1;
            },
            function (utils_2_1) {
                utils_2 = utils_2_1;
            },
            function (mvc_3_1) {
                mvc_3 = mvc_3_1;
            },
            function (ApplicationMediator_1_1) {
                ApplicationMediator_1 = ApplicationMediator_1_1;
            },
            function (Statics_3_1) {
                Statics_3 = Statics_3_1;
            },
            function (Notifications_5_1) {
                Notifications_5 = Notifications_5_1;
            },
            function (Boot_1_1) {
                Boot_1 = Boot_1_1;
            },
            function (Gameplay_1_1) {
                Gameplay_1 = Gameplay_1_1;
            },
            function (Menu_1_1) {
                Menu_1 = Menu_1_1;
            },
            function (GameModel_1_1) {
                GameModel_1 = GameModel_1_1;
            },
            function (Preloader_1_1) {
                Preloader_1 = Preloader_1_1;
            }],
        execute: function() {
            (function (EInitOrientation) {
                EInitOrientation[EInitOrientation["CORRECT"] = 0] = "CORRECT";
                EInitOrientation[EInitOrientation["INCORRECT"] = 1] = "INCORRECT";
                EInitOrientation[EInitOrientation["IRRELEVANT"] = 2] = "IRRELEVANT";
            })(EInitOrientation || (EInitOrientation = {}));
            exports_19("EInitOrientation", EInitOrientation);
            (function (EDeviceOrientation) {
                EDeviceOrientation[EDeviceOrientation["LANDSCAPE"] = 0] = "LANDSCAPE";
                EDeviceOrientation[EDeviceOrientation["PORTRAIT"] = 1] = "PORTRAIT";
                EDeviceOrientation[EDeviceOrientation["DESKTOP"] = 2] = "DESKTOP";
            })(EDeviceOrientation || (EDeviceOrientation = {}));
            exports_19("EDeviceOrientation", EDeviceOrientation);
            (function (EBootState) {
                EBootState[EBootState["PRELOADING"] = 0] = "PRELOADING";
                EBootState[EBootState["READY_FOR_BOOT"] = 1] = "READY_FOR_BOOT";
                EBootState[EBootState["BOOT_COMPLETE"] = 2] = "BOOT_COMPLETE";
            })(EBootState || (EBootState = {}));
            exports_19("EBootState", EBootState);
            MDApplication = (function (_super) {
                __extends(MDApplication, _super);
                function MDApplication() {
                    _super.call(this);
                    this.gameId = null;
                    this._lastHeight = 0;
                    this._bootState = EBootState.PRELOADING;
                }
                MDApplication.prototype.createGame = function () {
                    this.game = new core_2.Game({
                        width: this._getGameWidth(),
                        height: this._getGameHeight(),
                        parent: 'game-container',
                        renderer: utils_2.Device.cocoon ? Phaser.CANVAS : this._getRendererByDevice(),
                        transparent: false,
                        resolution: 1,
                        plugins: utils_2.Device.mobile ? [] : ['Debug']
                    });
                    this._mediator = new ApplicationMediator_1.default(this);
                    this._addStates();
                };
                MDApplication.prototype.startGame = function () {
                    this.game.state.start(Statics_3.Constants.STATE_BOOT);
                };
                MDApplication.prototype.registerModels = function () {
                    var gameModel = new GameModel_1.default('game_data');
                    var copyModel = new mvc_3.CopyModel('copy');
                };
                MDApplication.prototype._addStates = function () {
                    this.game.state.add(Statics_3.Constants.STATE_BOOT, Boot_1.default);
                    this.game.state.add(Statics_3.Constants.STATE_GAME, Gameplay_1.default);
                    this.game.state.add(Statics_3.Constants.STATE_MENU, Menu_1.default);
                };
                MDApplication.prototype.bootComplete = function () {
                    this.preloader = new Preloader_1.default(0, 0, 'preloader');
                    this.game.addToUI.existing(this.preloader);
                    this.game.transition.addAll(this.preloader);
                    this.game.time.events.add(100, this._moveToMenu, this);
                };
                MDApplication.prototype._moveToMenu = function () {
                    this.game.transition.to(Statics_3.Constants.STATE_MENU);
                };
                MDApplication.prototype.adjustScaleSettings = function () {
                    if (this.game.device.desktop) {
                        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                        this.game.scale.setMinMax(300, 200, 1050, 700);
                        this.game.scale.pageAlignHorizontally = true;
                        this._initOrientation = EInitOrientation.CORRECT;
                        this._orientation = EDeviceOrientation.DESKTOP;
                    }
                    else {
                        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
                        if (this.game.device.android) {
                            if (screen.width > screen.height) {
                                this._orientation = EDeviceOrientation.LANDSCAPE;
                            }
                            else {
                                this._orientation = EDeviceOrientation.PORTRAIT;
                            }
                            this.game.scale.setResizeCallback(this._checkOrientationAndroid, this);
                        }
                        else if (this.game.device.iOS) {
                            if (window.orientation === 0 || window.orientation === 180) {
                                this._orientation = EDeviceOrientation.PORTRAIT;
                            }
                            else {
                                this._orientation = EDeviceOrientation.LANDSCAPE;
                            }
                            this.game.scale.setResizeCallback(this._checkOrientationiOS, this);
                        }
                        this.game.scale.pageAlignVertically = true;
                        this._initRotationImage();
                        this._disableScrollEvents();
                        this._assignScreenSizeByDevice();
                    }
                };
                MDApplication.prototype._initRotationImage = function () {
                    var element = document.getElementById("turn");
                    var imageStr = "url('" + (window.hasOwnProperty("baseURL") ? window["baseURL"] + MDApplication.ROTATE_IMAGE : "." + MDApplication.ROTATE_IMAGE) + "') no-repeat center center";
                    element.style.background = imageStr;
                };
                MDApplication.prototype._disableScrollEvents = function () {
                    var container = document.getElementById('game-container');
                    container.addEventListener('touchmove', function (e) {
                        e.preventDefault();
                    }, false);
                };
                MDApplication.prototype._assignScreenSizeByDevice = function () {
                    if (this._orientation === MDApplication.DESIRED_ORIENTATION) {
                        this._initOrientation = EInitOrientation.CORRECT;
                        this.game.scale.setMinMax(window.innerWidth, window.innerHeight, window.innerWidth, window.innerHeight);
                        Statics_3.Globals.SCALE_RATIO = this._originalAspectRatio / (window.innerWidth / window.innerHeight);
                        this._hideRotateImage();
                    }
                    else {
                        this._initOrientation = EInitOrientation.INCORRECT;
                        this._showRotateImage();
                    }
                };
                MDApplication.prototype.adjustRendererSettings = function () {
                    this.game.stage.disableVisibilityChange = true;
                    this.game.forceSingleUpdate = true;
                    this.game.camera.roundPx = false;
                    this.game.renderer.renderSession.roundPixels = true;
                    this.game.antialias = true;
                    this.game.renderer.clearBeforeRender = this.game.renderType === Phaser.CANVAS;
                };
                MDApplication.prototype._initialResize = function () {
                    this._resizeApp();
                    if (this._bootState === EBootState.READY_FOR_BOOT) {
                        this.bootComplete();
                    }
                };
                MDApplication.prototype._handleEnterPortrait = function () {
                    if (this._orientation === EDeviceOrientation.LANDSCAPE) {
                        if (MDApplication.DESIRED_ORIENTATION === EDeviceOrientation.PORTRAIT) {
                            this._enterCorrectOrientation();
                        }
                        else {
                            this._enterIncorrectOrientation();
                        }
                    }
                    this._orientation = EDeviceOrientation.PORTRAIT;
                };
                MDApplication.prototype._handleEnterLandscape = function () {
                    if (this._orientation === EDeviceOrientation.PORTRAIT) {
                        if (MDApplication.DESIRED_ORIENTATION === EDeviceOrientation.LANDSCAPE) {
                            this._enterCorrectOrientation();
                        }
                        else {
                            this._enterIncorrectOrientation();
                        }
                    }
                    this._orientation = EDeviceOrientation.LANDSCAPE;
                };
                MDApplication.prototype._enterCorrectOrientation = function () {
                    this.mediator.sendNotification(Notifications_5.default.RESUME_GAME);
                    if (!this.game.device.desktop) {
                        if (this._initOrientation === EInitOrientation.INCORRECT) {
                            this._initOrientation = EInitOrientation.CORRECT;
                            this.game.time.events.add(100, this._initialResize, this);
                        }
                        else {
                            this._hideRotateImage();
                            this._resizeApp();
                        }
                    }
                };
                MDApplication.prototype._enterIncorrectOrientation = function () {
                    this.mediator.sendNotification(Notifications_5.default.PAUSE_GAME);
                    if (!this.game.device.desktop) {
                        this._showRotateImage();
                    }
                };
                MDApplication.prototype._showRotateImage = function () {
                    document.getElementById("turn").style.display = 'block';
                };
                MDApplication.prototype._hideRotateImage = function () {
                    document.getElementById("turn").style.display = 'none';
                };
                MDApplication.prototype._resizeApp = function () {
                    if (this._lastHeight !== Math.floor(window.innerHeight)) {
                        this._lastHeight = Math.floor(window.innerHeight);
                        this._hideRotateImage();
                        this.mediator.notifyAppResized();
                    }
                };
                MDApplication.prototype._inOrientation = function (desired) {
                    if (this.game.device.desktop) {
                        return true;
                    }
                    else if (this.game.device.iOS) {
                        if (window.orientation === 0 || window.orientation === 180) {
                            return desired === 'portrait';
                        }
                        else {
                            return desired === 'landscape';
                        }
                    }
                    else {
                        if (screen.width < screen.height) {
                            return desired === 'portrait';
                        }
                        else {
                            return desired === 'landscape';
                        }
                    }
                };
                MDApplication.prototype._checkOrientationAndroid = function () {
                    if (screen.width < screen.height) {
                        this._handleEnterPortrait();
                    }
                    else {
                        this._handleEnterLandscape();
                    }
                };
                MDApplication.prototype._checkOrientationiOS = function () {
                    if (window.orientation === 0 || window.orientation === 180) {
                        this._handleEnterPortrait();
                    }
                    else {
                        this._handleEnterLandscape();
                    }
                };
                Object.defineProperty(MDApplication.prototype, "_originalAspectRatio", {
                    get: function () {
                        return (this._getGameWidth() / this._getGameHeight());
                    },
                    enumerable: true,
                    configurable: true
                });
                MDApplication.prototype._getGameWidth = function () {
                    return utils_2.Device.mobile ? 1000 : 1050;
                };
                MDApplication.prototype._getGameHeight = function () {
                    return utils_2.Device.mobile ? 588 : 700;
                };
                MDApplication.prototype._getResolution = function () {
                    if (application_3.Application.queryVar('resolution') && !isNaN(application_3.Application.queryVar('resolution'))) {
                        return application_3.Application.queryVar('resolution');
                    }
                    if (utils_2.Device.cocoon) {
                        return (window.devicePixelRatio > 1 ? 2 : 1);
                    }
                    else {
                        return utils_2.Device.mobile ? 1 : (window.devicePixelRatio > 1 ? 2 : 1);
                    }
                };
                MDApplication.prototype._getRendererByDevice = function () {
                    return utils_2.Device.mobile && window.devicePixelRatio < 2 ? Phaser.CANVAS : Phaser.AUTO;
                };
                Object.defineProperty(MDApplication.prototype, "mediator", {
                    get: function () {
                        return this._mediator;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(MDApplication.prototype, "gameModel", {
                    get: function () {
                        return this.retrieveModel(GameModel_1.default.MODEL_NAME);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(MDApplication.prototype, "copyModel", {
                    get: function () {
                        return this.retrieveModel(mvc_3.CopyModel.MODEL_NAME);
                    },
                    enumerable: true,
                    configurable: true
                });
                MDApplication.ROTATE_IMAGE = "/assets/img/playlandscape.png";
                MDApplication.DESIRED_ORIENTATION = EDeviceOrientation.LANDSCAPE;
                return MDApplication;
            }(application_3.Application));
            exports_19("default", MDApplication);
        }
    }
});
System.register("bootstrap", ["MDApplication"], function(exports_20, context_20) {
    "use strict";
    var __moduleName = context_20 && context_20.id;
    var MDApplication_1;
    var app;
    return {
        setters:[
            function (MDApplication_1_1) {
                MDApplication_1 = MDApplication_1_1;
            }],
        execute: function() {
            exports_20("app", app = new MDApplication_1.default());
        }
    }
});
System.register("popups/BasePopup", ["dijon/display", "utils/Notifications", "mediator/BaseMediator", "utils/Statics"], function(exports_21, context_21) {
    "use strict";
    var __moduleName = context_21 && context_21.id;
    var display_4, Notifications_6, BaseMediator_5, Statics_4;
    var BasePopup;
    return {
        setters:[
            function (display_4_1) {
                display_4 = display_4_1;
            },
            function (Notifications_6_1) {
                Notifications_6 = Notifications_6_1;
            },
            function (BaseMediator_5_1) {
                BaseMediator_5 = BaseMediator_5_1;
            },
            function (Statics_4_1) {
                Statics_4 = Statics_4_1;
            }],
        execute: function() {
            BasePopup = (function (_super) {
                __extends(BasePopup, _super);
                function BasePopup(id, x, y) {
                    if (x === void 0) { x = 0; }
                    if (y === void 0) { y = 0; }
                    _super.call(this, x, y, id);
                    this.id = id;
                    this.visible = false;
                    this.alpha = 0;
                    this.scallables = [];
                    this.init();
                    this.buildInterface();
                    this.startPosition = this.position.clone();
                    this.y -= 20;
                    this.addTweens();
                    this.bg.inputEnabled = true;
                    this.bg.input.useHandCursor = false;
                }
                BasePopup.prototype.init = function () {
                    this.addMediator();
                };
                BasePopup.prototype.buildInterface = function () { };
                BasePopup.prototype.handleLanguageToggle = function () { };
                BasePopup.prototype.handleResize = function () {
                    for (var i = 0; i < this.scallables.length; i++) {
                        this.scallables[i].scale.setTo(Statics_4.Globals.SCALE_RATIO, 1);
                    }
                };
                BasePopup.prototype.getCopyByID = function (id) {
                    return id;
                };
                BasePopup.prototype.addMediator = function () {
                    this._mediator = BasePopup.retrieveMediator(BaseMediator_5.default.MEDIATOR_NAME, this);
                    if (this._mediator === null) {
                        this._mediator = new BaseMediator_5.default(this, true, BaseMediator_5.default.MEDIATOR_NAME);
                    }
                    this.register();
                };
                BasePopup.prototype.destroy = function () {
                    this.hideTween.onComplete.removeAll();
                    this.showTween = null;
                    this.hideTween = null;
                    _super.prototype.destroy.call(this);
                };
                BasePopup.prototype.show = function () {
                    if (this.showTween.isRunning) {
                        return;
                    }
                    this.clearActiveTweens();
                    this.visible = true;
                    this.showTween.start();
                    return this.showTween;
                };
                BasePopup.prototype.hide = function () {
                    if (this.hideTween.isRunning) {
                        return;
                    }
                    this.allowInput = false;
                    this.clearActiveTweens();
                    this.hideTween.start();
                    return this.hideTween;
                };
                BasePopup.prototype.close = function () {
                    this._mediator.sendNotification(Notifications_6.default.HIDE_POPUP, this.id);
                };
                BasePopup.prototype.addTweens = function () {
                    this.showTween = this.game.add.tween(this).to({ alpha: 1, y: this.startPosition.y }, 300, Phaser.Easing.Bounce.Out);
                    this.showTween.onComplete.add(this.showComplete, this);
                    this.hideTween = this.game.add.tween(this).to({ alpha: 0, y: this.startPosition.y + 80 }, 300, Phaser.Easing.Quadratic.Out);
                    this.hideTween.onComplete.add(this.hideComplete, this);
                };
                BasePopup.prototype.showComplete = function () {
                    this.allowInput = true;
                };
                BasePopup.prototype.hideComplete = function () {
                    this.visible = false;
                    this.y = this.startPosition.y - 20;
                };
                BasePopup.prototype.fadeInElement = function (element) {
                    element.visible = true;
                    this.game.add.tween(element).to({ alpha: 1 }, 350, Phaser.Easing.Cubic.In, true);
                };
                BasePopup.prototype.fadeOutElement = function (element) {
                    this.game.add.tween(element).to({ alpha: 0 }, 350, Phaser.Easing.Cubic.In, true).onComplete.addOnce(function () {
                        element.visible = false;
                    });
                };
                BasePopup.prototype.sendResumeNotification = function () {
                    this._mediator.sendNotification(Notifications_6.default.RESUME_GAME);
                };
                BasePopup.prototype.clearActiveTweens = function () {
                    if (this.showTween.isRunning) {
                        this.showTween.stop();
                    }
                    if (this.hideTween.isRunning) {
                        this.hideTween.stop();
                    }
                };
                BasePopup.retrieveMediator = function (name, viewComp) {
                    return BaseMediator_5.default.retrieveMediator(name, viewComp);
                };
                BasePopup.prototype.register = function () {
                    this._mediator.sendNotification(Notifications_6.default.REGISTER_POPUP, this);
                };
                return BasePopup;
            }(display_4.Group));
            exports_21("default", BasePopup);
        }
    }
});
System.register("ui/Controls", ['dijon/application', 'dijon/display', "utils/Statics"], function(exports_22, context_22) {
    "use strict";
    var __moduleName = context_22 && context_22.id;
    var application_4, display_5, Statics_5;
    var LCButton, LCLabelledButton, Slider;
    return {
        setters:[
            function (application_4_1) {
                application_4 = application_4_1;
            },
            function (display_5_1) {
                display_5 = display_5_1;
            },
            function (Statics_5_1) {
                Statics_5 = Statics_5_1;
            }],
        execute: function() {
            LCButton = (function (_super) {
                __extends(LCButton, _super);
                function LCButton(x, y, callback, context, assetKey, baseFrameName, forceOut) {
                    if (forceOut === void 0) { forceOut = false; }
                    _super.call(this, application_4.Application.getInstance().game, x, y, assetKey, callback, context, baseFrameName + '_over', baseFrameName, baseFrameName + '_down', baseFrameName);
                    this.forceOut = forceOut;
                    this.input.useHandCursor = true;
                }
                LCButton.prototype.onInputDownHandler = function (sprite, pointer) {
                    _super.prototype.onInputDownHandler.call(this, sprite, pointer);
                };
                LCButton.prototype.onInputOverHandler = function (sprite, pointer) {
                    _super.prototype.onInputOverHandler.call(this, sprite, pointer);
                };
                LCButton.prototype.updateBaseFrame = function (base) {
                    this.setFrames(base + '_over', base, base + '_down', base);
                };
                Object.defineProperty(LCButton.prototype, "dgame", {
                    get: function () {
                        return application_4.Application.getInstance().game;
                    },
                    enumerable: true,
                    configurable: true
                });
                return LCButton;
            }(Phaser.Button));
            exports_22("LCButton", LCButton);
            LCLabelledButton = (function (_super) {
                __extends(LCLabelledButton, _super);
                function LCLabelledButton() {
                    _super.apply(this, arguments);
                }
                LCLabelledButton.prototype.onInputDownHandler = function (sprite, pointer) {
                    _super.prototype.onInputDownHandler.call(this, sprite, pointer);
                };
                LCLabelledButton.prototype.onInputOverHandler = function (sprite, pointer) {
                    _super.prototype.onInputOverHandler.call(this, sprite, pointer);
                };
                Object.defineProperty(LCLabelledButton.prototype, "dgame", {
                    get: function () {
                        return application_4.Application.getInstance().game;
                    },
                    enumerable: true,
                    configurable: true
                });
                return LCLabelledButton;
            }(display_5.LabelledButton));
            exports_22("LCLabelledButton", LCLabelledButton);
            Slider = (function (_super) {
                __extends(Slider, _super);
                function Slider(x, y, key, bgFrame, controlFrame, iconFrames) {
                    if (iconFrames === void 0) { iconFrames = null; }
                    _super.call(this, x, y);
                    this.iconFrames = iconFrames;
                    this.onDragEnded = new Phaser.Signal();
                    this.bar = this.addInternal.tileSprite(0, 0, 10, 10, key, bgFrame);
                    this.control = this.addInternal.dSprite(0, 0, key, controlFrame);
                    this.control.anchor.setTo(0.5, 0.5);
                    this.control.inputEnabled = true;
                    this.control.input.draggable = true;
                    this.control.events.onInputUp.add(this.onInputUp, this);
                    if (iconFrames !== null) {
                        this.icon = this.addInternal.image(0, 0, key, this.iconFrames.active);
                    }
                    else {
                        this.icon = null;
                    }
                    this.label = this.addInternal.dText(0, 0, '100%', Statics_5.Constants.FONT_PRESS_START, 16, Statics_5.Constants.COLOR_SLIDER_LABEL, 'center');
                }
                Slider.prototype.initializeHorizontalSlider = function (barWidth, barHeight, showIcon, showLabel) {
                    if (showIcon === void 0) { showIcon = true; }
                    if (showLabel === void 0) { showLabel = true; }
                    this.scrollType = 'horz';
                    this.bar.width = barWidth;
                    this.bar.height = barHeight;
                    this.bar.anchor.set(0, 0.5);
                    this.control.x = this.bar.right;
                    this.control.y = this.bar.y - 12;
                    this.control.input.allowVerticalDrag = false;
                    if (this.icon !== null) {
                        this.icon.x = this.bar.left - 8;
                        this.icon.y = this.bar.y;
                        this.icon.visible = showIcon;
                        this.icon.anchor.setTo(1, 0.5);
                    }
                    this.label.x = this.bar.right + 16;
                    this.label.y = this.bar.y + 2;
                    this.label.visible = showLabel;
                    this.label.anchor.setTo(0, 0.5);
                    this.updateFunc = this.horizontalUpdate;
                };
                Slider.prototype.initializeVerticalSlider = function (barWidth, barHeight, showIcon, showLabel) {
                    if (showIcon === void 0) { showIcon = true; }
                    if (showLabel === void 0) { showLabel = true; }
                    this.scrollType = 'vert';
                    this.bar.width = barWidth;
                    this.bar.height = barHeight;
                    this.bar.anchor.set(0.5, 0);
                    this.control.x = this.bar.x - 12;
                    this.control.y = this.bar.top;
                    this.control.input.allowHorizontalDrag = false;
                    this.control.angle = -90;
                    if (this.icon !== null) {
                        this.icon.x = this.bar.x;
                        this.icon.y = this.bar.top - 8;
                        this.icon.visible = showIcon;
                        this.icon.anchor.setTo(0.5, 1);
                    }
                    this.label.x = this.bar.x;
                    this.label.y = this.bar.bottom + 16;
                    this.label.visible = showLabel;
                    this.label.anchor.setTo(0.5, 0);
                    this.updateFunc = this.verticalUpdate;
                };
                Slider.prototype.update = function () {
                    if (this.control.input.isDragged === true) {
                        this.updateFunc();
                    }
                };
                Slider.prototype.updateFunc = function () { };
                Slider.prototype.onInputUp = function () {
                    this.updateFunc();
                    this.onDragEnded.dispatch(this);
                };
                Slider.prototype.setControlPosition = function (percent) {
                    if (this.scrollType === 'horz') {
                        this.control.x = this.bar.x + (this.barWidth * percent);
                        this.updateElements();
                    }
                    else if (this.scrollType === 'vert') {
                        this.control.y = this.bar.y + (this.barHeight * percent);
                        this.updateElements();
                    }
                };
                Slider.prototype.horizontalUpdate = function () {
                    if (this.control.x <= this.bar.left) {
                        this.control.x = this.bar.left;
                    }
                    else if (this.control.x >= this.bar.right) {
                        this.control.x = this.bar.right;
                    }
                    this.updateElements();
                };
                Slider.prototype.verticalUpdate = function () {
                    if (this.control.y <= this.bar.top) {
                        this.control.y = this.bar.top;
                    }
                    else if (this.control.y >= this.bar.bottom) {
                        this.control.y = this.bar.bottom;
                    }
                    this.updateElements();
                };
                Slider.prototype.updateElements = function () {
                    if (this.icon !== null) {
                        if (this.sliderPercent <= 0) {
                            this.icon.frameName = this.iconFrames.inactive;
                        }
                        else {
                            this.icon.frameName = this.iconFrames.active;
                        }
                    }
                    if (this.label.visible) {
                        this.label.text = Math.round(this.sliderPercent * 100) + '%';
                    }
                };
                Object.defineProperty(Slider.prototype, "sliderPercent", {
                    get: function () {
                        if (this.scrollType === 'horz') {
                            return this.sliderPercentHorz;
                        }
                        else {
                            return this.sliderPercentVert;
                        }
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Slider.prototype, "sliderPercentHorz", {
                    get: function () {
                        return Math.min(Math.max((this.control.x - this.bar.x) / this.barWidth, 0), 1);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Slider.prototype, "sliderPercentVert", {
                    get: function () {
                        return Math.min(Math.max((this.control.y - this.bar.y) / this.barHeight, 0), 1);
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Slider.prototype, "barWidth", {
                    get: function () {
                        return this.bar.width;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(Slider.prototype, "barHeight", {
                    get: function () {
                        return this.bar.height;
                    },
                    enumerable: true,
                    configurable: true
                });
                return Slider;
            }(display_5.Group));
            exports_22("Slider", Slider);
        }
    }
});
System.register("popups/OptionsPopup", ["popups/BasePopup", "utils/Statics", "ui/Controls", "mediator/OptionsPopupMediator"], function(exports_23, context_23) {
    "use strict";
    var __moduleName = context_23 && context_23.id;
    var BasePopup_1, Statics_6, Controls_1, OptionsPopupMediator_1;
    var OptionsPopup;
    return {
        setters:[
            function (BasePopup_1_1) {
                BasePopup_1 = BasePopup_1_1;
            },
            function (Statics_6_1) {
                Statics_6 = Statics_6_1;
            },
            function (Controls_1_1) {
                Controls_1 = Controls_1_1;
            },
            function (OptionsPopupMediator_1_1) {
                OptionsPopupMediator_1 = OptionsPopupMediator_1_1;
            }],
        execute: function() {
            OptionsPopup = (function (_super) {
                __extends(OptionsPopup, _super);
                function OptionsPopup() {
                    _super.call(this, Statics_6.Constants.POPUP_OPTIONS, 0, 0);
                }
                OptionsPopup.prototype.buildInterface = function () {
                    this.bg = this.addInternal.image(this.game.centerX, this.game.centerY, 'ui', 'popup_background');
                    this.bg.anchor.setTo(0.5);
                    this.sfxSlider = new Controls_1.Slider(this.bg.x, this.bg.y - (this.bg.realHeight * 0.3), 'ui', 'grey_slider_horz', 'yellow_slider_down', { active: 'sfx_icon', inactive: 'sfx_icon_mute' });
                    this.sfxSlider.initializeHorizontalSlider(220, 10);
                    this.sfxSlider.x -= this.sfxSlider.barWidth * 0.5;
                    this.addInternal.existing(this.sfxSlider);
                    this.sfxSlider.onDragEnded.add(this.changeSFXVolume, this);
                    this.scallables.push(this.sfxSlider);
                    this.musicSlider = new Controls_1.Slider(this.bg.x, this.bg.y - (this.bg.realHeight * 0.1), 'ui', 'grey_slider_horz', 'yellow_slider_down', { active: 'music_icon', inactive: 'music_icon_mute' });
                    this.musicSlider.initializeHorizontalSlider(220, 10);
                    this.musicSlider.x -= this.musicSlider.barWidth * 0.5;
                    this.addInternal.existing(this.musicSlider);
                    this.musicSlider.onDragEnded.add(this.changeMusicVolume, this);
                    this.scallables.push(this.musicSlider);
                    this.sfxSlider.setControlPosition(this.game.audio.spriteVolume);
                    this.musicSlider.setControlPosition(this.game.audio.soundVolume);
                    this.quitBtn = this.addButton(this.bg.x + (this.bg.realWidth * 0.225), this.bg.y + (this.bg.realHeight * 0.3), this.quitPressed, 'quit_game');
                    this.addInternal.existing(this.quitBtn);
                    this.scallables.push(this.quitBtn);
                };
                OptionsPopup.prototype.addButton = function (x, y, callback, copyID) {
                    var button = new Controls_1.LCLabelledButton(x, y, callback, this, 'menu', 'red_btn_normal', 'red_btn_down', 'red_btn_over', 'red_btn_normal');
                    button.addLabel(this.getCopyByID(copyID), 14, Statics_6.Constants.FONT_PRESS_START);
                    button.centerPivot();
                    return button;
                };
                OptionsPopup.prototype.layoutForGameplay = function () {
                    this.sfxSlider.y = this.y + this.bg.y - (this.bg.realHeight * 0.25);
                    this.musicSlider.y = this.bg.y - (this.bg.realHeight * 0.1);
                };
                OptionsPopup.prototype.layoutForMenu = function () {
                    this.sfxSlider.y = this.y + this.bg.y - (this.bg.realHeight * 0.2);
                    this.musicSlider.y = this.game.centerY;
                };
                OptionsPopup.prototype.changeMusicVolume = function () {
                    this.game.audio.soundVolume = this.musicSlider.sliderPercent;
                    this.game.audio.playAudio('message_alt', this.game.audio.soundVolume);
                };
                OptionsPopup.prototype.changeSFXVolume = function () {
                    this.game.audio.spriteVolume = this.sfxSlider.sliderPercent;
                    this.game.audio.playAudio('message_alt');
                };
                OptionsPopup.prototype.quitPressed = function () {
                    this.mediator.quitToMenu();
                };
                OptionsPopup.prototype.howToPressed = function () {
                    this.mediator.requestPopup(Statics_6.Constants.POPUP_HELP);
                };
                OptionsPopup.prototype.show = function () {
                    if (this.inGameState) {
                        this.layoutForGameplay();
                    }
                    else {
                        this.layoutForMenu();
                    }
                    this.quitBtn.visible = this.inGameState;
                    return _super.prototype.show.call(this);
                };
                OptionsPopup.prototype.hideComplete = function () {
                    _super.prototype.hideComplete.call(this);
                    if (this.game.state.current !== Statics_6.Constants.STATE_MENU) {
                        this.mediator.notifyResumeGame();
                    }
                    this.mediator.updatePlayerSettings();
                };
                OptionsPopup.prototype.addMediator = function () {
                    this._mediator = BasePopup_1.default.retrieveMediator(OptionsPopupMediator_1.default.MEDIATOR_NAME, this);
                    if (this._mediator === null) {
                        this._mediator = new OptionsPopupMediator_1.default(this, true, OptionsPopupMediator_1.default.MEDIATOR_NAME);
                    }
                    this.register();
                };
                OptionsPopup.prototype.getCopyByID = function (id) {
                    return this.mediator.copyModel.getCopy('options', id);
                };
                Object.defineProperty(OptionsPopup.prototype, "mediator", {
                    get: function () {
                        return this._mediator;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(OptionsPopup.prototype, "inGameState", {
                    get: function () {
                        return (this.game.state.current !== Statics_6.Constants.STATE_MENU);
                    },
                    enumerable: true,
                    configurable: true
                });
                return OptionsPopup;
            }(BasePopup_1.default));
            exports_23("default", OptionsPopup);
        }
    }
});
System.register("mediator/OptionsPopupMediator", ["utils/Statics", "mediator/BaseMediator", "utils/Notifications"], function(exports_24, context_24) {
    "use strict";
    var __moduleName = context_24 && context_24.id;
    var Statics_7, BaseMediator_6, Notifications_7;
    var OptionsPopupMediator;
    return {
        setters:[
            function (Statics_7_1) {
                Statics_7 = Statics_7_1;
            },
            function (BaseMediator_6_1) {
                BaseMediator_6 = BaseMediator_6_1;
            },
            function (Notifications_7_1) {
                Notifications_7 = Notifications_7_1;
            }],
        execute: function() {
            OptionsPopupMediator = (function (_super) {
                __extends(OptionsPopupMediator, _super);
                function OptionsPopupMediator() {
                    _super.apply(this, arguments);
                }
                OptionsPopupMediator.prototype.handleNotification = function (notification) {
                    var noteName = notification.getName();
                    var noteBody = notification.getBody();
                    switch (noteName) {
                        case Notifications_7.default.APP_RESIZED:
                            this.options.handleResize();
                            break;
                    }
                };
                OptionsPopupMediator.prototype.updatePlayerSettings = function () {
                    this.gameModel.savePlayerSettings();
                };
                OptionsPopupMediator.prototype.quitToMenu = function () {
                    this.closeAllPopups();
                    this.game.transition.to(Statics_7.Constants.STATE_MENU);
                };
                Object.defineProperty(OptionsPopupMediator.prototype, "name", {
                    get: function () {
                        return OptionsPopupMediator.MEDIATOR_NAME;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(OptionsPopupMediator.prototype, "options", {
                    get: function () {
                        return this._viewComponent;
                    },
                    enumerable: true,
                    configurable: true
                });
                OptionsPopupMediator.MEDIATOR_NAME = 'optionsPopupMediator';
                return OptionsPopupMediator;
            }(BaseMediator_6.default));
            exports_24("default", OptionsPopupMediator);
        }
    }
});
System.register("popups/PopupManager", ['dijon/display', "mediator/PopupManagerMediator", "popups/BasePopup", 'dijon/utils'], function(exports_25, context_25) {
    "use strict";
    var __moduleName = context_25 && context_25.id;
    var display_6, PopupManagerMediator_1, BasePopup_2, utils_3;
    var PopupManager;
    return {
        setters:[
            function (display_6_1) {
                display_6 = display_6_1;
            },
            function (PopupManagerMediator_1_1) {
                PopupManagerMediator_1 = PopupManagerMediator_1_1;
            },
            function (BasePopup_2_1) {
                BasePopup_2 = BasePopup_2_1;
            },
            function (utils_3_1) {
                utils_3 = utils_3_1;
            }],
        execute: function() {
            PopupManager = (function (_super) {
                __extends(PopupManager, _super);
                function PopupManager() {
                    _super.call(this, 0, 0, 'PopupManager', false);
                    this.fromOptions = false;
                    this.init();
                    this.buildInterface();
                }
                PopupManager.prototype.init = function () {
                    this.classType = BasePopup_2.default;
                    this.fixedToCamera = true;
                    this._mediator = BasePopup_2.default.retrieveMediator(PopupManagerMediator_1.default.MEDIATOR_NAME, this);
                    if (this._mediator === null) {
                        this._mediator = new PopupManagerMediator_1.default(this);
                    }
                };
                PopupManager.prototype.buildInterface = function () {
                    this.addModal();
                };
                PopupManager.prototype.addModal = function () {
                    if (this.modal) {
                        return;
                    }
                    this.modal = this.add(this.game.add.sprite(0, 0, utils_3.Textures.rect(this.game.width, this.game.height, 0x000000, 0.25)));
                    this.modal.name = 'popupManagerModal';
                    this.modal.inputEnabled = true;
                    this.modal.events.onInputDown.add(this.mediator.modalPressed, this.mediator);
                    this.modal.cacheAsBitmap = true;
                    this.modal.visible = false;
                };
                PopupManager.prototype.showModal = function () {
                    this.sendToBack(this.modal);
                    this.mediator.notifyPauseGame();
                    this.modal.visible = true;
                };
                PopupManager.prototype.hideModal = function (andResume) {
                    if (andResume === void 0) { andResume = true; }
                    if (andResume === true) {
                        this.mediator.notifyResumeGame();
                    }
                    this.modal.visible = false;
                };
                PopupManager.prototype.removeAll = function (destroy, silent) {
                    if (destroy === void 0) { destroy = true; }
                    if (silent === void 0) { silent = false; }
                    if (this.modal) {
                        this.sendToBack(this.modal);
                        this.hideModal();
                    }
                    while (this.children.length > 1) {
                        this.remove(this.getChildAt(1), destroy, silent);
                    }
                    this.mediator.clearLookup();
                };
                Object.defineProperty(PopupManager.prototype, "mediator", {
                    get: function () {
                        return this._mediator;
                    },
                    enumerable: true,
                    configurable: true
                });
                return PopupManager;
            }(display_6.Group));
            exports_25("default", PopupManager);
        }
    }
});
System.register("mediator/PopupManagerMediator", ["mediator/BaseMediator", "utils/Notifications", "popups/OptionsPopup"], function(exports_26, context_26) {
    "use strict";
    var __moduleName = context_26 && context_26.id;
    var BaseMediator_7, Notifications_8, OptionsPopup_1;
    var PopupManagerMediator;
    return {
        setters:[
            function (BaseMediator_7_1) {
                BaseMediator_7 = BaseMediator_7_1;
            },
            function (Notifications_8_1) {
                Notifications_8 = Notifications_8_1;
            },
            function (OptionsPopup_1_1) {
                OptionsPopup_1 = OptionsPopup_1_1;
            }],
        execute: function() {
            PopupManagerMediator = (function (_super) {
                __extends(PopupManagerMediator, _super);
                function PopupManagerMediator(viewComp) {
                    _super.call(this, viewComp, true, PopupManagerMediator.MEDIATOR_NAME);
                    this.lookup = {};
                    this.allowModalPress = false;
                    this.visibleList = [];
                }
                PopupManagerMediator.prototype.listNotificationInterests = function () {
                    return [
                        Notifications_8.default.ADD_ALL_POPUPS,
                        Notifications_8.default.REGISTER_POPUP,
                        Notifications_8.default.SHOW_POPUP,
                        Notifications_8.default.HIDE_POPUP,
                        Notifications_8.default.HIDE_ALL_POPUPS
                    ];
                };
                PopupManagerMediator.prototype.handleNotification = function (notification) {
                    var _this = this;
                    var noteName = notification.getName();
                    var noteBody = notification.getBody();
                    switch (noteName) {
                        case Notifications_8.default.ADD_ALL_POPUPS:
                            this.addAllPopups();
                            break;
                        case Notifications_8.default.REGISTER_POPUP:
                            this.registerPopup(noteBody);
                            break;
                        case Notifications_8.default.SHOW_POPUP:
                            var toShow = this.getPopup(noteBody);
                            if (toShow !== null) {
                                this.manager.showModal();
                                this.manager.bringToTop(toShow);
                                toShow.show().onComplete.addOnce(function () { _this.allowModalPress = true; });
                                this.visibleList.push(noteBody);
                            }
                            break;
                        case Notifications_8.default.HIDE_POPUP:
                            var toHide = this.getPopup(noteBody);
                            if (toHide !== null) {
                                toHide.hide();
                                this.updateVisibleList(noteBody);
                            }
                            break;
                        case Notifications_8.default.HIDE_ALL_POPUPS:
                            while (this.visibleList.length > 0) {
                                this.lookup[this.visibleList.shift()].hide();
                            }
                            this.manager.hideModal(false);
                            break;
                    }
                };
                PopupManagerMediator.prototype.clearLookup = function () {
                    this.lookup = {};
                };
                PopupManagerMediator.prototype.closeTop = function () {
                    this.sendNotification(Notifications_8.default.HIDE_POPUP, this.visibleList[this.visibleList.length - 1]);
                };
                PopupManagerMediator.prototype.modalPressed = function () {
                    if (this.popupSelectionRequired === true || this.allowModalPress === false) {
                        return;
                    }
                    this.closeTop();
                };
                PopupManagerMediator.prototype.hasPopupWithId = function (popupID) {
                    return this.getPopup(popupID) ? true : false;
                };
                PopupManagerMediator.prototype.addAllPopups = function () {
                    var a = new OptionsPopup_1.default();
                    this.addAllPopups = function () { };
                };
                PopupManagerMediator.prototype.getPopup = function (popupID) {
                    return this.lookup[popupID] || null;
                };
                PopupManagerMediator.prototype.registerPopup = function (popup) {
                    this.lookup[popup.id] = popup;
                    this.manager.addModal();
                    this.manager.add(popup);
                };
                PopupManagerMediator.prototype.updateVisibleList = function (toRemove) {
                    var index = this.visibleList.lastIndexOf(toRemove);
                    if (index != -1) {
                        var hold = this.visibleList[this.visibleList.length - 1];
                        this.visibleList[index] = this.visibleList[this.visibleList.length - 1];
                        this.visibleList[index] = hold;
                        this.visibleList.pop();
                    }
                    if (this.visibleList.length === 0 || index === -1) {
                        this.manager.hideModal();
                        this.allowModalPress = false;
                    }
                };
                Object.defineProperty(PopupManagerMediator.prototype, "manager", {
                    get: function () {
                        return this.viewComponent;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PopupManagerMediator.prototype, "popupSelectionRequired", {
                    get: function () {
                        return false;
                    },
                    enumerable: true,
                    configurable: true
                });
                Object.defineProperty(PopupManagerMediator.prototype, "name", {
                    get: function () {
                        return PopupManagerMediator.MEDIATOR_NAME;
                    },
                    enumerable: true,
                    configurable: true
                });
                PopupManagerMediator.MEDIATOR_NAME = 'popupManagerMediator';
                return PopupManagerMediator;
            }(BaseMediator_7.default));
            exports_26("default", PopupManagerMediator);
        }
    }
});
System.register("mediator/PreloadMediator", ["utils/Statics", "mediator/BaseMediator"], function(exports_27, context_27) {
    "use strict";
    var __moduleName = context_27 && context_27.id;
    var Statics_8, BaseMediator_8;
    var PreloadMediator;
    return {
        setters:[
            function (Statics_8_1) {
                Statics_8 = Statics_8_1;
            },
            function (BaseMediator_8_1) {
                BaseMediator_8 = BaseMediator_8_1;
            }],
        execute: function() {
            PreloadMediator = (function (_super) {
                __extends(PreloadMediator, _super);
                function PreloadMediator() {
                    _super.apply(this, arguments);
                }
                PreloadMediator.prototype.next = function () {
                    this.game.transition.to(Statics_8.Constants.STATE_MENU);
                };
                Object.defineProperty(PreloadMediator.prototype, "name", {
                    get: function () {
                        return PreloadMediator.MEDIATOR_NAME;
                    },
                    enumerable: true,
                    configurable: true
                });
                PreloadMediator.MEDIATOR_NAME = 'preloadMediator';
                return PreloadMediator;
            }(BaseMediator_8.default));
            exports_27("default", PreloadMediator);
        }
    }
});

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInV0aWxzL05vdGlmaWNhdGlvbnMudHMiLCJ1dGlscy9TdGF0aWNzLnRzIiwibW9kZWwvR2FtZU1vZGVsLnRzIiwibWVkaWF0b3IvQmFzZU1lZGlhdG9yLnRzIiwibWVkaWF0b3IvQXBwbGljYXRpb25NZWRpYXRvci50cyIsInN0YXRlL0Jhc2VTdGF0ZS50cyIsIm1lZGlhdG9yL0Jvb3RNZWRpYXRvci50cyIsInN0YXRlL0Jvb3QudHMiLCJtZWRpYXRvci9HYW1lcGxheU1lZGlhdG9yLnRzIiwiYWN0b3JzL0FjdG9yLnRzIiwiYWN0b3JzL1ZlaGljbGUudHMiLCJhY3RvcnMvUm9jay50cyIsInN0YXRlL0dhbWVwbGF5LnRzIiwibWVkaWF0b3IvTWVudU1lZGlhdG9yLnRzIiwic3RhdGUvTWVudS50cyIsInVpL1BpZVByb2dyZXNzQmFyLnRzIiwidWkvUHJlbG9hZGVyLnRzIiwiTURBcHBsaWNhdGlvbi50cyIsImJvb3RzdHJhcC50cyIsInBvcHVwcy9CYXNlUG9wdXAudHMiLCJ1aS9Db250cm9scy50cyIsInBvcHVwcy9PcHRpb25zUG9wdXAudHMiLCJtZWRpYXRvci9PcHRpb25zUG9wdXBNZWRpYXRvci50cyIsInBvcHVwcy9Qb3B1cE1hbmFnZXIudHMiLCJtZWRpYXRvci9Qb3B1cE1hbmFnZXJNZWRpYXRvci50cyIsIm1lZGlhdG9yL1ByZWxvYWRNZWRpYXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7WUFBQTtnQkFBQTtnQkFnQkEsQ0FBQztnQkFmVSx1QkFBUyxHQUFXLFVBQVUsQ0FBQztnQkFDL0IsMkJBQWEsR0FBVyxjQUFjLENBQUM7Z0JBQ3ZDLHlCQUFXLEdBQVcsWUFBWSxDQUFDO2dCQUNuQyx5QkFBVyxHQUFXLFlBQVksQ0FBQztnQkFFbkMsd0JBQVUsR0FBVyxXQUFXLENBQUM7Z0JBQ2pDLHlCQUFXLEdBQVcsWUFBWSxDQUFDO2dCQUNuQyx3QkFBVSxHQUFXLFdBQVcsQ0FBQztnQkFFakMsNEJBQWMsR0FBVyxnQkFBZ0IsQ0FBQTtnQkFDekMsd0JBQVUsR0FBVyxXQUFXLENBQUM7Z0JBQ2pDLHdCQUFVLEdBQVcsV0FBVyxDQUFDO2dCQUNqQyw0QkFBYyxHQUFXLGNBQWMsQ0FBQztnQkFDeEMsNkJBQWUsR0FBVyxlQUFlLENBQUM7Z0JBRXJELG9CQUFDO1lBQUQsQ0FoQkEsQUFnQkMsSUFBQTtZQWhCRCxtQ0FnQkMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUNoQkQ7Z0JBQUE7Z0JBbUJBLENBQUM7Z0JBakJVLG9CQUFVLEdBQVcsTUFBTSxDQUFDO2dCQUM1QixvQkFBVSxHQUFXLE1BQU0sQ0FBQztnQkFDNUIsb0JBQVUsR0FBVyxNQUFNLENBQUM7Z0JBRzVCLHVCQUFhLEdBQVcsVUFBVSxDQUFDO2dCQUduQywwQkFBZ0IsR0FBVywyQkFBMkIsQ0FBQztnQkFHdkQsdUJBQWEsR0FBVyxjQUFjLENBQUM7Z0JBQ3ZDLG9CQUFVLEdBQVcsV0FBVyxDQUFDO2dCQUdqQyx5QkFBZSxHQUFXLFNBQVMsQ0FBQztnQkFDcEMsNEJBQWtCLEdBQVcsU0FBUyxDQUFDO2dCQUNsRCxnQkFBQztZQUFELENBbkJBLEFBbUJDLElBQUE7WUFuQkQsaUNBbUJDLENBQUE7WUFJRDtnQkFBQTtnQkFFQSxDQUFDO2dCQURVLG1CQUFXLEdBQVcsQ0FBQyxDQUFDO2dCQUNuQyxjQUFDO1lBQUQsQ0FGQSxBQUVDLElBQUE7WUFGRCw2QkFFQyxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7WUNyQkQ7Z0JBQXVDLDZCQUFLO2dCQUE1QztvQkFBdUMsOEJBQUs7b0JBSTlCLGVBQVUsR0FBZ0IsSUFBSSxDQUFDO2dCQXdEN0MsQ0FBQztnQkFwRFUsaUNBQWEsR0FBcEI7b0JBQ0ksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDMUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7b0JBQ3pCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDMUIsQ0FBQztnQkFFTSxxQ0FBaUIsR0FBeEI7b0JBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDO29CQUN6RCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUM7Z0JBQzlELENBQUM7Z0JBRU0sa0NBQWMsR0FBckI7b0JBRUksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxtQkFBUyxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFFdkYsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUMxQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO29CQUM5QyxDQUFDO29CQUVELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2dCQUM3QixDQUFDO2dCQUVTLGtDQUFjLEdBQXhCO29CQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLG1CQUFTLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDbkYsQ0FBQztnQkFFTSxzQ0FBa0IsR0FBekI7b0JBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDO29CQUN6RCxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7b0JBQzFELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDMUIsQ0FBQztnQkFFUyxvQ0FBZ0IsR0FBMUI7b0JBQ0ksSUFBSSxJQUFJLEdBQWdCLElBQUksTUFBTSxFQUFFLENBQUM7b0JBRXJDLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO29CQUNuQixJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQztvQkFDckIsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUM7b0JBQ2QsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBRTlCLE1BQU0sQ0FBQyxJQUFJLENBQUM7Z0JBQ2hCLENBQUM7Z0JBSUQsc0JBQVcsMkJBQUk7eUJBQWY7d0JBQ0ksTUFBTSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUM7b0JBQ2hDLENBQUM7OzttQkFBQTtnQkFFRCxzQkFBVywrQkFBUTt5QkFBbkI7d0JBQ0ksTUFBTSxDQUFZLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3pDLENBQUM7OzttQkFBQTtnQkExRGEsb0JBQVUsR0FBVyxXQUFXLENBQUM7Z0JBMkRuRCxnQkFBQztZQUFELENBNURBLEFBNERDLENBNURzQyxXQUFLLEdBNEQzQztZQTVERCwrQkE0REMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lDMUREO2dCQUEwQyxnQ0FBUTtnQkFBbEQ7b0JBQTBDLDhCQUFRO2dCQWdFbEQsQ0FBQztnQkF6RGlCLDZCQUFnQixHQUE5QixVQUErQixJQUFZLEVBQUUsUUFBYTtvQkFDdEQsSUFBSSxPQUFPLEdBQStCLHlCQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzNGLEVBQUUsQ0FBQyxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUNuQixPQUFPLENBQUMsY0FBYyxHQUFHLFFBQVEsQ0FBQztvQkFDdEMsQ0FBQztvQkFDRCxNQUFNLENBQUMsT0FBTyxDQUFDO2dCQUNuQixDQUFDO2dCQUlNLGdEQUF5QixHQUFoQztvQkFDSSxNQUFNLENBQUM7d0JBQ0gsdUJBQWEsQ0FBQyxXQUFXO3FCQUM1QixDQUFDO2dCQUNOLENBQUM7Z0JBRU0sOEJBQU8sR0FBZCxVQUFlLE9BQWUsRUFBRSxNQUFjO29CQUMxQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2dCQUNuRCxDQUFDO2dCQUVNLG1DQUFZLEdBQW5CLFVBQW9CLE9BQWUsRUFBRSxNQUFjO29CQUMvQyxNQUFNLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3hELENBQUM7Z0JBRU0sbUNBQVksR0FBbkIsVUFBb0IsT0FBZTtvQkFDL0IsSUFBSSxDQUFDLGdCQUFnQixDQUFDLHVCQUFhLENBQUMsVUFBVSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUM3RCxDQUFDO2dCQUVNLHFDQUFjLEdBQXJCO29CQUNJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBYSxDQUFDLGVBQWUsQ0FBQyxDQUFDO2dCQUN6RCxDQUFDO2dCQUVNLHVDQUFnQixHQUF2QjtvQkFDSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsdUJBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDckQsQ0FBQztnQkFFTSxzQ0FBZSxHQUF0QjtvQkFDSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsdUJBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDcEQsQ0FBQztnQkFJRCxzQkFBVyxtQ0FBUzt5QkFBcEI7d0JBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDO29CQUNoQyxDQUFDOzs7bUJBQUE7Z0JBRUQsc0JBQVcsbUNBQVM7eUJBQXBCO3dCQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztvQkFDaEMsQ0FBQzs7O21CQUFBO2dCQUVELHNCQUFXLCtCQUFLO3lCQUFoQjt3QkFDSSxNQUFNLENBQWdCLHlCQUFXLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3BELENBQUM7OzttQkFBQTtnQkFFRCxzQkFBVyw4QkFBSTt5QkFBZjt3QkFDSSxNQUFNLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxDQUFDO29CQUNsRCxDQUFDOzs7bUJBQUE7Z0JBQ0wsbUJBQUM7WUFBRCxDQWhFQSxBQWdFQyxDQWhFeUMsY0FBUSxHQWdFakQ7WUFoRUQsa0NBZ0VDLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQzlERDtnQkFBaUQsdUNBQVk7Z0JBQTdEO29CQUFpRCw4QkFBWTtnQkFpRDdELENBQUM7Z0JBN0NVLHVEQUF5QixHQUFoQztvQkFDSSxNQUFNLENBQUM7d0JBQ0gsdUJBQWEsQ0FBQyxTQUFTO3dCQUN2Qix1QkFBYSxDQUFDLGFBQWE7d0JBQzNCLHVCQUFhLENBQUMsV0FBVztxQkFDNUIsQ0FBQTtnQkFDTCxDQUFDO2dCQUVNLGdEQUFrQixHQUF6QixVQUEwQixZQUEyQjtvQkFDakQsTUFBTSxDQUFDLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQzt3QkFDN0IsS0FBSyx1QkFBYSxDQUFDLFNBQVM7NEJBQ3hCLGNBQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLHlCQUF5QixDQUFDLENBQUM7NEJBQzVDLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQW1CLEVBQUUsQ0FBQzs0QkFDekMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDOzRCQUM1QyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxDQUFDOzRCQUNoQyxLQUFLLENBQUM7d0JBRVYsS0FBSyx1QkFBYSxDQUFDLGFBQWE7NEJBQzVCLGNBQU0sQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFLDZCQUE2QixDQUFDLENBQUM7NEJBQ2hELElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzs0QkFDM0QsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLEVBQUUsQ0FBQzs0QkFDcEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQzs0QkFDbEMsS0FBSyxDQUFDO3dCQUVWLEtBQUssdUJBQWEsQ0FBQyxXQUFXOzRCQUMxQixJQUFJLEtBQUssR0FBNkMsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDOzRCQUM3RSxFQUFFLENBQUMsQ0FBQyxLQUFLLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQztnQ0FDakIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7NEJBQ3pDLENBQUM7NEJBQ0QsS0FBSyxDQUFDO29CQUNkLENBQUM7Z0JBQ0wsQ0FBQztnQkFFTSw4Q0FBZ0IsR0FBdkI7b0JBQ0ksSUFBSSxDQUFDLGdCQUFnQixDQUFDLHVCQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ3JELENBQUM7Z0JBR0Qsc0JBQVcsOENBQWE7eUJBQXhCO3dCQUNJLE1BQU0sQ0FBZ0IsSUFBSSxDQUFDLGNBQWMsQ0FBQztvQkFDOUMsQ0FBQzs7O21CQUFBO2dCQUVELHNCQUFXLHFDQUFJO3lCQUFmO3dCQUNJLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxhQUFhLENBQUM7b0JBQzdDLENBQUM7OzttQkFBQTtnQkEvQ2EsaUNBQWEsR0FBVyxxQkFBcUIsQ0FBQztnQkFnRGhFLDBCQUFDO1lBQUQsQ0FqREEsQUFpREMsQ0FqRGdELHNCQUFZLEdBaUQ1RDtZQWpERCx5Q0FpREMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7O1lDdEREO2dCQUF1Qyw2QkFBSztnQkFBNUM7b0JBQXVDLDhCQUFLO2dCQUk1QyxDQUFDO2dCQUhVLDhCQUFVLEdBQWpCO29CQUNJLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDbEIsQ0FBQztnQkFDTCxnQkFBQztZQUFELENBSkEsQUFJQyxDQUpzQyxZQUFLLEdBSTNDO1lBSkQsK0JBSUMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lDSkQ7Z0JBQTBDLGdDQUFZO2dCQUF0RDtvQkFBMEMsOEJBQVk7Z0JBa0J0RCxDQUFDO2dCQWRVLGlDQUFVLEdBQWpCO29CQUNJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNuRCxDQUFDO2dCQUlNLG1DQUFZLEdBQW5CO29CQUNJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUN2RCxDQUFDO2dCQUdELHNCQUFXLDhCQUFJO3lCQUFmO3dCQUNJLE1BQU0sQ0FBQyxZQUFZLENBQUMsYUFBYSxDQUFDO29CQUN0QyxDQUFDOzs7bUJBQUE7Z0JBaEJhLDBCQUFhLEdBQVcsY0FBYyxDQUFDO2dCQWlCekQsbUJBQUM7WUFBRCxDQWxCQSxBQWtCQyxDQWxCeUMsc0JBQVksR0FrQnJEO1lBbEJELGtDQWtCQyxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7WUNsQkQ7Z0JBQWtDLHdCQUFTO2dCQUEzQztvQkFBa0MsOEJBQVM7Z0JBMkIzQyxDQUFDO2dCQXpCVSxtQkFBSSxHQUFYO29CQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxzQkFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUM1QyxDQUFDO2dCQUVNLHNCQUFPLEdBQWQ7b0JBQ0ksRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUM7d0JBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixHQUFHLFdBQVcsQ0FBQztvQkFDbkQsQ0FBQztvQkFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDbkMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzNDLENBQUM7Z0JBR00sNkJBQWMsR0FBckI7b0JBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLEVBQUUsQ0FBQztnQkFDakMsQ0FBQztnQkFLRCxzQkFBYywwQkFBUTt5QkFBdEI7d0JBQ0ksTUFBTSxDQUFlLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQ3hDLENBQUM7OzttQkFBQTtnQkFDTCxXQUFDO1lBQUQsQ0EzQkEsQUEyQkMsQ0EzQmlDLG1CQUFTLEdBMkIxQztZQTNCRCwwQkEyQkMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7O1lDdkJEO2dCQUE4QyxvQ0FBWTtnQkFBMUQ7b0JBQThDLDhCQUFZO2dCQWdDMUQsQ0FBQztnQkE3QlUsb0RBQXlCLEdBQWhDO29CQUNJLE1BQU0sQ0FBQyxFQUVMLENBQUM7Z0JBQ1AsQ0FBQztnQkFFTSw2Q0FBa0IsR0FBekIsVUFBMEIsWUFBMkI7b0JBQ2pELElBQU0sUUFBUSxHQUFHLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDeEMsSUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUN4QyxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO29CQUVuQixDQUFDO2dCQUNMLENBQUM7Z0JBSUQsc0JBQVcsc0NBQVE7eUJBQW5CO3dCQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztvQkFDbkMsQ0FBQzs7O21CQUFBO2dCQUlELHNCQUFXLGtDQUFJO3lCQUFmO3dCQUNJLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLENBQUM7b0JBQzFDLENBQUM7OzttQkFBQTtnQkFFRCxzQkFBVyxzQ0FBUTt5QkFBbkI7d0JBQ0ksTUFBTSxDQUFXLElBQUksQ0FBQyxjQUFjLENBQUM7b0JBQ3pDLENBQUM7OzttQkFBQTtnQkE5QmEsOEJBQWEsR0FBVyxjQUFjLENBQUM7Z0JBK0J6RCx1QkFBQztZQUFELENBaENBLEFBZ0NDLENBaEM2QyxzQkFBWSxHQWdDekQ7WUFoQ0QsdUNBZ0NDLENBQUE7Ozs7Ozs7Ozs7Ozs7OztZQ3JDRDtnQkFBbUMseUJBQU07Z0JBQ3JDLGVBQVksQ0FBUyxFQUFFLENBQVMsRUFBRSxHQUFXLEVBQUUsS0FBYSxFQUFFLE9BQWUsRUFBRSxTQUEwQjtvQkFBMUIseUJBQTBCLEdBQTFCLGlCQUEwQjtvQkFDckcsa0JBQU0sQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsS0FBSyxDQUFDLENBQUM7b0JBRXhCLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ2xDLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQzFCLElBQUksQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztvQkFDNUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO2dCQUN0QyxDQUFDO2dCQUVELHNCQUFXLHlCQUFNO3lCQUFqQjt3QkFDSSxNQUFNLENBQXlCLElBQUksQ0FBQyxJQUFJLENBQUM7b0JBQzdDLENBQUM7OzttQkFBQTtnQkFDTCxZQUFDO1lBQUQsQ0FiQSxBQWFDLENBYmtDLGdCQUFNLEdBYXhDO1lBYkQsNEJBYUMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7O1lDWkQ7Z0JBQXFDLDJCQUFLO2dCQVd0QyxpQkFBWSxDQUFTLEVBQUUsQ0FBUyxFQUFFLEdBQVcsRUFBRSxLQUFhLEVBQUUsT0FBZTtvQkFDekUsa0JBQU0sQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztvQkFWbEMsY0FBUyxHQUFXLEdBQUcsQ0FBQztvQkFDeEIsV0FBTSxHQUFXLENBQUMsQ0FBQztvQkFLbkIsZUFBVSxHQUFZLElBQUksQ0FBQztvQkFDM0IsaUJBQVksR0FBVyxDQUFDLENBQUM7b0JBSy9CLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBQ3RDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQztvQkFDOUUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFFNUQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN0QixDQUFDO2dCQUVNLHdCQUFNLEdBQWI7b0JBS0ksRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzt3QkFDNUIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUMxQixDQUFDO29CQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO3dCQUNsQyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7b0JBQ3pCLENBQUM7b0JBQ0QsSUFBSSxDQUFDLENBQUM7d0JBQ0YsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUN2QixDQUFDO29CQUNELElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDekIsQ0FBQztnQkFFUyw2QkFBVyxHQUFyQjtvQkFDSSxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsQ0FBQztnQkFDN0QsQ0FBQztnQkFFUywrQkFBYSxHQUF2QjtvQkFDSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ2pDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzt3QkFDdEMsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQ3hDLENBQUM7Z0JBQ0wsQ0FBQztnQkFFUyxnQ0FBYyxHQUF4QjtvQkFDSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUM7b0JBQ2pDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUMzQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDO29CQUM3QyxDQUFDO2dCQUNMLENBQUM7Z0JBRVMsK0JBQWEsR0FBdkI7b0JBQUEsaUJBSUM7b0JBSEcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFFLFVBQUMsS0FBVTt3QkFDdEMsS0FBSyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO29CQUM3QyxDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2dCQUVTLDRCQUFVLEdBQXBCLFVBQXFCLE9BQWUsRUFBRSxVQUEwQjtvQkFBMUIsMEJBQTBCLEdBQTFCLGlCQUEwQjtvQkFDNUQsSUFBSSxPQUFPLEdBQUcsRUFBRSxDQUFDO29CQUVqQixJQUFJLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsT0FBTyxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLE9BQU8sRUFBRSxVQUFVLEVBQUUsT0FBTyxDQUFDLENBQUM7b0JBRTVHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7b0JBQ25DLEtBQUssQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7b0JBQ3pCLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQU8zQixJQUFJLFFBQVEsR0FBRyxFQUFFLENBQUM7b0JBQ2xCLElBQUksR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsT0FBTyxFQUFFLE9BQU8sQ0FBQyxFQUFHLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLEVBQUMsQ0FBQyxDQUFDLEVBQUUsUUFBUSxDQUFDLENBQUM7b0JBRXJILEVBQUUsQ0FBQyxDQUFDLFVBQVUsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUN0QixJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDNUIsQ0FBQztvQkFHRCxLQUFLLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFNekQsS0FBSyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2dCQUNoRCxDQUFDO2dCQUVNLGdDQUFjLEdBQXJCLFVBQXNCLFVBQWUsRUFBRSxNQUE4QjtvQkFLeEUsRUFBRSxDQUFDLENBQUMsQ0FBQyxVQUFVLEtBQUssSUFBSSxJQUFJLE1BQU0sQ0FBQyxFQUFFLElBQUksQ0FBQyxDQUFDOzJCQUNqQyxDQUFDLFVBQVUsSUFBSSxVQUFVLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO29CQUMzQixDQUFDO2dCQUNMLENBQUM7Z0JBRUQsc0JBQVcsa0NBQWE7eUJBQXhCO3dCQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO29CQUMvQixDQUFDOzs7bUJBQUE7Z0JBQ0wsY0FBQztZQUFELENBNUdBLEFBNEdDLENBNUdvQyxlQUFLLEdBNEd6QztZQTVHRCw4QkE0R0MsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7O1lDN0dEO2dCQUFrQyx3QkFBSztnQkFBdkM7b0JBQWtDLDhCQUFLO2dCQUV2QyxDQUFDO2dCQUFELFdBQUM7WUFBRCxDQUZBLEFBRUMsQ0FGaUMsZUFBSyxHQUV0QztZQUZELDJCQUVDLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQ0VEO2dCQUFzQyw0QkFBUztnQkFBL0M7b0JBQXNDLDhCQUFTO2dCQXdFL0MsQ0FBQztnQkFsRVUsdUJBQUksR0FBWDtvQkFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLDBCQUFnQixDQUFDLGdCQUFnQixDQUFDLDBCQUFnQixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFDekYsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUMxQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksMEJBQWdCLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQ2hELENBQUM7b0JBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3ZFLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNuRCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUM7b0JBQ3JDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsQ0FBQztnQkFDMUUsQ0FBQztnQkFHTSxvQ0FBaUIsR0FBeEI7b0JBQ0ksTUFBTSxDQUFDO3dCQUNILElBQUksQ0FBQyxNQUFNO3dCQUNYLElBQUksQ0FBQyxXQUFXO3dCQUNoQixJQUFJLENBQUMsV0FBVzt3QkFDaEIsSUFBSSxDQUFDLGNBQWM7cUJBQ3RCLENBQUE7Z0JBQ0wsQ0FBQztnQkFFUyx5QkFBTSxHQUFoQjtnQkFFQSxDQUFDO2dCQUVTLDhCQUFXLEdBQXJCO29CQUNJLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7d0JBRXpCLElBQUksSUFBSSxHQUFHLElBQUksZUFBSyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBS3RILElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDdEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzVCLENBQUM7Z0JBQ0wsQ0FBQztnQkFFUyw4QkFBVyxHQUFyQjtvQkFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksaUJBQU8sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsT0FBTyxDQUFDLENBQUM7b0JBQ3JGLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDL0IsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDekMsQ0FBQztnQkFFUyxpQ0FBYyxHQUF4QjtvQkFDSSxJQUFJLGVBQWUsR0FBc0MsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDbEosZUFBZSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7b0JBRWhDLGVBQWUsQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDO2dCQU1uQyxDQUFDO2dCQUlELHNCQUFjLDhCQUFRO3lCQUF0Qjt3QkFDSSxNQUFNLENBQW1CLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQzVDLENBQUM7OzttQkFBQTtnQkFFRCxzQkFBVywrQkFBUzt5QkFBcEI7d0JBQ0ksTUFBTSxDQUFDLE1BQU0sQ0FBQztvQkFDbEIsQ0FBQzs7O21CQUFBO2dCQUNMLGVBQUM7WUFBRCxDQXhFQSxBQXdFQyxDQXhFcUMsbUJBQVMsR0F3RTlDO1lBeEVELCtCQXdFQyxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7WUN4RUQ7Z0JBQTBDLGdDQUFZO2dCQUF0RDtvQkFBMEMsOEJBQVk7Z0JBMEJ0RCxDQUFDO2dCQXZCVSxnREFBeUIsR0FBaEM7b0JBQ0ksTUFBTSxDQUFDLEVBRUwsQ0FBQztnQkFDUCxDQUFDO2dCQUVNLHlDQUFrQixHQUF6QixVQUEwQixZQUEyQjtvQkFDakQsSUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUN4QyxJQUFNLFFBQVEsR0FBRyxZQUFZLENBQUMsT0FBTyxFQUFFLENBQUM7b0JBQ3hDLE1BQU0sQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7b0JBRW5CLENBQUM7Z0JBQ0wsQ0FBQztnQkFHTSxnQ0FBUyxHQUFoQjtvQkFDSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsdUJBQWEsQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDeEQsQ0FBQztnQkFHRCxzQkFBVyw4QkFBSTt5QkFBZjt3QkFDSSxNQUFNLENBQUMsWUFBWSxDQUFDLGFBQWEsQ0FBQztvQkFDdEMsQ0FBQzs7O21CQUFBO2dCQXhCYSwwQkFBYSxHQUFXLGNBQWMsQ0FBQztnQkF5QnpELG1CQUFDO1lBQUQsQ0ExQkEsQUEwQkMsQ0ExQnlDLHNCQUFZLEdBMEJyRDtZQTFCRCxtQ0EwQkMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lDeEJEO2dCQUFrQyx3QkFBUztnQkFBM0M7b0JBQWtDLDhCQUFTO2dCQTBEM0MsQ0FBQztnQkF4RFUsbUJBQUksR0FBWDtvQkFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLHNCQUFZLENBQUMsZ0JBQWdCLENBQUMsc0JBQVksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ2pGLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLHNCQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzVDLENBQUM7Z0JBQ0wsQ0FBQztnQkFHTSxnQ0FBaUIsR0FBeEI7b0JBQ0ksTUFBTSxDQUFDO3dCQUNILElBQUksQ0FBQyxNQUFNO3dCQUNYLElBQUksQ0FBQyxVQUFVO3dCQUNmLElBQUksQ0FBQyxXQUFXO3FCQUNuQixDQUFBO2dCQUNMLENBQUM7Z0JBRU0seUJBQVUsR0FBakI7b0JBQ0ksZ0JBQUssQ0FBQyxVQUFVLFdBQUUsQ0FBQztvQkFDbkIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO2dCQUNsQixDQUFDO2dCQUVPLHFCQUFNLEdBQWQ7b0JBQ0ksSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsbUJBQVMsQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLENBQUM7b0JBQzdJLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDeEIsQ0FBQztnQkFFTywwQkFBVyxHQUFuQjtvQkFDSSxJQUFJLFFBQVEsR0FBRyxJQUFJLHdCQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLGdCQUFnQixFQUFFLGNBQWMsRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztvQkFDMUssUUFBUSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxFQUFFLEVBQUUsRUFBRSxtQkFBUyxDQUFDLGdCQUFnQixDQUFDLENBQUM7b0JBQ2xGLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztvQkFDdkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUM7Z0JBQ2hDLENBQUM7Z0JBRU8seUJBQVUsR0FBbEI7b0JBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLEVBQUUsQ0FBQztnQkFDOUIsQ0FBQztnQkFJTSx3QkFBUyxHQUFoQjtvQkFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsbUJBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDbEQsQ0FBQztnQkFJTSwwQkFBVyxHQUFsQixVQUFtQixFQUFVO29CQUN6QixNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDdkQsQ0FBQztnQkFFRCxzQkFBVywwQkFBUTt5QkFBbkI7d0JBQ0ksTUFBTSxDQUFlLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQ3hDLENBQUM7OzttQkFBQTtnQkFFRCxzQkFBVywyQkFBUzt5QkFBcEI7d0JBQ0ksTUFBTSxDQUFDLE1BQU0sQ0FBQztvQkFDbEIsQ0FBQzs7O21CQUFBO2dCQUNMLFdBQUM7WUFBRCxDQTFEQSxBQTBEQyxDQTFEaUMsbUJBQVMsR0EwRDFDO1lBMURELDJCQTBEQyxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7WUNoRUQ7Z0JBQTRDLGtDQUFhO2dCQVVyRCx3QkFBWSxDQUFTLEVBQUUsQ0FBUyxFQUFFLE1BQWtCLEVBQUUsTUFBa0IsRUFBRSxLQUF5QixFQUFFLEtBQW1CO29CQUF0RixzQkFBa0IsR0FBbEIsVUFBa0I7b0JBQUUsc0JBQWtCLEdBQWxCLFVBQWtCO29CQUFFLHFCQUF5QixHQUF6QixpQkFBeUI7b0JBQUUscUJBQW1CLEdBQW5CLFNBQWlCLEVBQUU7b0JBQ3BILGtCQUFNLHlCQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDNUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDL0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7b0JBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO29CQUNuQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztvQkFFcEIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDakssQ0FBQztvQkFDRCxJQUFJLENBQUMsQ0FBQzt3QkFDRixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDakUsQ0FBQztvQkFDRCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDNUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0IsQ0FBQztnQkFFTSx1Q0FBYyxHQUFyQixVQUFzQixPQUFlO29CQUNqQyxJQUFJLFFBQVEsR0FBVyxPQUFPLENBQUM7b0JBQy9CLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ2xCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDbkIsUUFBUSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxDQUFDLEVBQUUsTUFBTSxDQUFDLENBQUM7d0JBQ2pELElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO3dCQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQzt3QkFDeEMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQzt3QkFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxFQUFFLENBQUM7d0JBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxFQUFFLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUSxFQUFFLEtBQUssQ0FBQyxDQUFDO3dCQUN4SCxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQkFDM0IsQ0FBQztvQkFDRCxJQUFJLENBQUMsQ0FBQzt3QkFDRixRQUFRLEdBQUcsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsQ0FBQyxDQUFDLENBQUM7d0JBQ3JELElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO3dCQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsQ0FBQzt3QkFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDO3dCQUMvRixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7d0JBQ2pELElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsRUFBRSxDQUFDO3dCQUMxQixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDekIsQ0FBQztvQkFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7Z0JBQzNCLENBQUM7Z0JBRUQsc0JBQVcsa0NBQU07eUJBS2pCO3dCQUNJLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO29CQUN4QixDQUFDO3lCQVBELFVBQWtCLEtBQWE7d0JBQzNCLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxLQUFLLEdBQUcsQ0FBQyxHQUFHLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDdkMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLEVBQUUsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQztvQkFDekQsQ0FBQzs7O21CQUFBO2dCQUtMLHFCQUFDO1lBQUQsQ0E3REEsQUE2REMsQ0E3RDJDLE1BQU0sQ0FBQyxNQUFNLEdBNkR4RDtZQTdERCxxQ0E2REMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lDM0REO2dCQUF1Qyw2QkFBSztnQkFVeEMsbUJBQVksQ0FBUyxFQUFFLENBQVMsRUFBRSxJQUFZO29CQUMxQyxrQkFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFQckIseUJBQW9CLEdBQWtCLElBQUksTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDO29CQUMxRCwwQkFBcUIsR0FBa0IsSUFBSSxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUM7b0JBTzlELElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDWixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQzFCLENBQUM7Z0JBR1Msa0NBQWMsR0FBeEI7b0JBQ0ksSUFBSSxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ25DLEdBQUcsQ0FBQyxTQUFTLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUMzQixHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDdEQsR0FBRyxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUVkLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsZUFBZSxFQUFFLENBQUMsQ0FBQztvQkFFbEUsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFFbEMsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7b0JBQ2YsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7b0JBRXJCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSx3QkFBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUM7b0JBQzVGLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztvQkFFOUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDN0YsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFFN0YsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQzdDLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUNuRCxDQUFDO2dCQUdNLDZCQUFTLEdBQWhCO29CQUNJLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQyxDQUFDO2dCQUV6QyxDQUFDO2dCQUVNLGdDQUFZLEdBQW5CLFVBQW9CLFFBQWdCO29CQUNoQyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUMsR0FBRyxDQUFDLENBQUM7Z0JBQ3BELENBQUM7Z0JBRU0sZ0NBQVksR0FBbkI7Z0JBRUEsQ0FBQztnQkFFTSxnQ0FBWSxHQUFuQjtvQkFDSSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztvQkFDcEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDMUIsQ0FBQztnQkFFTSxpQ0FBYSxHQUFwQjtvQkFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxDQUFDO2dCQUMzQixDQUFDO2dCQUdTLHVCQUFHLEdBQWI7b0JBQ0ksSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN6QyxDQUFDO2dCQUVTLHdCQUFJLEdBQWQ7b0JBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7b0JBQ3JCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDMUMsQ0FBQztnQkFDTCxnQkFBQztZQUFELENBeEVBLEFBd0VDLENBeEVzQyxlQUFLLEdBd0UzQztZQXhFRCxnQ0F3RUMsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lDOURELFdBQVksZ0JBQWdCO2dCQUN4Qiw2REFBTyxDQUFBO2dCQUNQLGlFQUFTLENBQUE7Z0JBQ1QsbUVBQVUsQ0FBQTtZQUNkLENBQUMsRUFKVyxnQkFBZ0IsS0FBaEIsZ0JBQWdCLFFBSTNCOzZEQUFBO1lBRUQsV0FBWSxrQkFBa0I7Z0JBQzFCLHFFQUFTLENBQUE7Z0JBQ1QsbUVBQVEsQ0FBQTtnQkFDUixpRUFBTyxDQUFBO1lBQ1gsQ0FBQyxFQUpXLGtCQUFrQixLQUFsQixrQkFBa0IsUUFJN0I7aUVBQUE7WUFFRCxXQUFZLFVBQVU7Z0JBQ2xCLHVEQUFVLENBQUE7Z0JBQ1YsK0RBQWMsQ0FBQTtnQkFDZCw2REFBYSxDQUFBO1lBQ2pCLENBQUMsRUFKVyxVQUFVLEtBQVYsVUFBVSxRQUlyQjtpREFBQTtZQUVEO2dCQUEyQyxpQ0FBVztnQkFZbEQ7b0JBQ0ksaUJBQU8sQ0FBQztvQkFUTCxXQUFNLEdBQVcsSUFBSSxDQUFDO29CQUdyQixnQkFBVyxHQUFXLENBQUMsQ0FBQztvQkFFeEIsZUFBVSxHQUFlLFVBQVUsQ0FBQyxVQUFVLENBQUM7Z0JBS3ZELENBQUM7Z0JBR00sa0NBQVUsR0FBakI7b0JBQ0ksSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLFdBQUksQ0FBQzt3QkFDakIsS0FBSyxFQUFFLElBQUksQ0FBQyxhQUFhLEVBQUU7d0JBQzNCLE1BQU0sRUFBRSxJQUFJLENBQUMsY0FBYyxFQUFFO3dCQUM3QixNQUFNLEVBQUUsZ0JBQWdCO3dCQUV4QixRQUFRLEVBQUUsY0FBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsRUFBRTt3QkFDckUsV0FBVyxFQUFFLEtBQUs7d0JBR2xCLFVBQVUsRUFBRSxDQUFDO3dCQUNiLE9BQU8sRUFBRSxjQUFNLENBQUMsTUFBTSxHQUFHLEVBQUUsR0FBRyxDQUFDLE9BQU8sQ0FBQztxQkFFMUMsQ0FBQyxDQUFDO29CQUVILElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSw2QkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDL0MsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO2dCQUN0QixDQUFDO2dCQUVNLGlDQUFTLEdBQWhCO29CQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxtQkFBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNoRCxDQUFDO2dCQUdNLHNDQUFjLEdBQXJCO29CQUNJLElBQU0sU0FBUyxHQUFHLElBQUksbUJBQVMsQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFDN0MsSUFBTSxTQUFTLEdBQUcsSUFBSSxlQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQzVDLENBQUM7Z0JBSU8sa0NBQVUsR0FBbEI7b0JBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLG1CQUFTLENBQUMsVUFBVSxFQUFFLGNBQUksQ0FBQyxDQUFDO29CQUNoRCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsbUJBQVMsQ0FBQyxVQUFVLEVBQUUsa0JBQVEsQ0FBQyxDQUFDO29CQUNwRCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsbUJBQVMsQ0FBQyxVQUFVLEVBQUUsY0FBSSxDQUFDLENBQUM7Z0JBQ3BELENBQUM7Z0JBRU0sb0NBQVksR0FBbkI7b0JBQ0ksSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLG1CQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxXQUFXLENBQUMsQ0FBQztvQkFDbEQsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDM0MsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztvQkFDNUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDM0QsQ0FBQztnQkFFUyxtQ0FBVyxHQUFyQjtvQkFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsbUJBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDbEQsQ0FBQztnQkFJTSwyQ0FBbUIsR0FBMUI7b0JBQ0ksRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDO3dCQUN6RCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7d0JBQy9DLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQzt3QkFDN0MsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQzt3QkFDakQsSUFBSSxDQUFDLFlBQVksR0FBRyxrQkFBa0IsQ0FBQyxPQUFPLENBQUM7b0JBQ25ELENBQUM7b0JBQ0QsSUFBSSxDQUFDLENBQUM7d0JBQ0YsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsUUFBUSxDQUFDO3dCQUN6RCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDOzRCQUMzQixFQUFFLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO2dDQUMvQixJQUFJLENBQUMsWUFBWSxHQUFHLGtCQUFrQixDQUFDLFNBQVMsQ0FBQzs0QkFDckQsQ0FBQzs0QkFDRCxJQUFJLENBQUMsQ0FBQztnQ0FDRixJQUFJLENBQUMsWUFBWSxHQUFHLGtCQUFrQixDQUFDLFFBQVEsQ0FBQzs0QkFDcEQsQ0FBQzs0QkFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBQzNFLENBQUM7d0JBQ0QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7NEJBQzVCLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxXQUFXLEtBQUssQ0FBQyxJQUFJLE1BQU0sQ0FBQyxXQUFXLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQztnQ0FDekQsSUFBSSxDQUFDLFlBQVksR0FBRyxrQkFBa0IsQ0FBQyxRQUFRLENBQUM7NEJBQ3BELENBQUM7NEJBQ0QsSUFBSSxDQUFDLENBQUM7Z0NBQ0YsSUFBSSxDQUFDLFlBQVksR0FBRyxrQkFBa0IsQ0FBQyxTQUFTLENBQUM7NEJBQ3JELENBQUM7NEJBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQyxDQUFDO3dCQUN2RSxDQUFDO3dCQUNELElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQzt3QkFDM0MsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7d0JBQzFCLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO3dCQUM1QixJQUFJLENBQUMseUJBQXlCLEVBQUUsQ0FBQztvQkFDckMsQ0FBQztnQkFDTCxDQUFDO2dCQUdPLDBDQUFrQixHQUExQjtvQkFDSSxJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUM5QyxJQUFJLFFBQVEsR0FBRyxPQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxTQUFTLENBQUMsR0FBRyxhQUFhLENBQUMsWUFBWSxHQUFHLEdBQUcsR0FBRyxhQUFhLENBQUMsWUFBWSxDQUFDLEdBQUcsNEJBQTRCLENBQUM7b0JBQy9LLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLFFBQVEsQ0FBQztnQkFDeEMsQ0FBQztnQkFFTyw0Q0FBb0IsR0FBNUI7b0JBQ0ksSUFBSSxTQUFTLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO29CQUMxRCxTQUFTLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLFVBQVMsQ0FBQzt3QkFDOUMsQ0FBQyxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUN2QixDQUFDLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQ2QsQ0FBQztnQkFFTyxpREFBeUIsR0FBakM7b0JBQ0ksRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFlBQVksS0FBSyxhQUFhLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDO3dCQUMxRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsZ0JBQWdCLENBQUMsT0FBTyxDQUFDO3dCQUNqRCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxNQUFNLENBQUMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxVQUFVLEVBQUUsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUN4RyxpQkFBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsQ0FBQyxNQUFNLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDM0YsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7b0JBQzVCLENBQUM7b0JBQ0QsSUFBSSxDQUFDLENBQUM7d0JBQ0YsSUFBSSxDQUFDLGdCQUFnQixHQUFHLGdCQUFnQixDQUFDLFNBQVMsQ0FBQzt3QkFDbkQsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7b0JBQzVCLENBQUM7Z0JBQ0wsQ0FBQztnQkFFTSw4Q0FBc0IsR0FBN0I7b0JBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDO29CQUMvQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQztvQkFDbkMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztvQkFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7b0JBQ3BELElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDM0IsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEtBQUssTUFBTSxDQUFDLE1BQU0sQ0FBQztnQkFDbEYsQ0FBQztnQkFHTyxzQ0FBYyxHQUF0QjtvQkFDSSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7b0JBQ2xCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEtBQUssVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7d0JBQ2hELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQztnQkFDTCxDQUFDO2dCQUlPLDRDQUFvQixHQUE1QjtvQkFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxLQUFLLGtCQUFrQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7d0JBQ3JELEVBQUUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsS0FBSyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDOzRCQUNwRSxJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQzt3QkFDcEMsQ0FBQzt3QkFDRCxJQUFJLENBQUMsQ0FBQzs0QkFDRixJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQzt3QkFDdEMsQ0FBQztvQkFDTCxDQUFDO29CQUNELElBQUksQ0FBQyxZQUFZLEdBQUcsa0JBQWtCLENBQUMsUUFBUSxDQUFDO2dCQUNwRCxDQUFDO2dCQUVPLDZDQUFxQixHQUE3QjtvQkFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsWUFBWSxLQUFLLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7d0JBQ3BELEVBQUUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxtQkFBbUIsS0FBSyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDOzRCQUNyRSxJQUFJLENBQUMsd0JBQXdCLEVBQUUsQ0FBQzt3QkFDcEMsQ0FBQzt3QkFDRCxJQUFJLENBQUMsQ0FBQzs0QkFDRixJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQzt3QkFDdEMsQ0FBQztvQkFDTCxDQUFDO29CQUNELElBQUksQ0FBQyxZQUFZLEdBQUcsa0JBQWtCLENBQUMsU0FBUyxDQUFDO2dCQUNyRCxDQUFDO2dCQUVPLGdEQUF3QixHQUFoQztvQkFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLHVCQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzFELEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzt3QkFDNUIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixLQUFLLGdCQUFnQixDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7NEJBQ3ZELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUM7NEJBQ2pELElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7d0JBQzlELENBQUM7d0JBQUMsSUFBSSxDQUFDLENBQUM7NEJBQ0osSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7NEJBQ3hCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQzt3QkFDdEIsQ0FBQztvQkFDTCxDQUFDO2dCQUNMLENBQUM7Z0JBRU8sa0RBQTBCLEdBQWxDO29CQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsZ0JBQWdCLENBQUMsdUJBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDekQsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO3dCQUM1QixJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztvQkFDNUIsQ0FBQztnQkFDTCxDQUFDO2dCQUVPLHdDQUFnQixHQUF4QjtvQkFDSSxRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO2dCQUM1RCxDQUFDO2dCQUVPLHdDQUFnQixHQUF4QjtvQkFDSSxRQUFRLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO2dCQUMzRCxDQUFDO2dCQUVPLGtDQUFVLEdBQWxCO29CQUNJLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUN0RCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO3dCQUdsRCxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzt3QkFFeEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO29CQUNyQyxDQUFDO2dCQUNMLENBQUM7Z0JBYVMsc0NBQWMsR0FBeEIsVUFBeUIsT0FBZTtvQkFDcEMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQzt3QkFDM0IsTUFBTSxDQUFDLElBQUksQ0FBQztvQkFDaEIsQ0FBQztvQkFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFFNUIsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsS0FBSyxDQUFDLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDOzRCQUN6RCxNQUFNLENBQUMsT0FBTyxLQUFLLFVBQVUsQ0FBQzt3QkFDbEMsQ0FBQzt3QkFFRCxJQUFJLENBQUMsQ0FBQzs0QkFDRixNQUFNLENBQUMsT0FBTyxLQUFLLFdBQVcsQ0FBQzt3QkFDbkMsQ0FBQztvQkFDTCxDQUFDO29CQUNELElBQUksQ0FBQyxDQUFDO3dCQUVGLEVBQUUsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7NEJBQy9CLE1BQU0sQ0FBQyxPQUFPLEtBQUssVUFBVSxDQUFDO3dCQUNsQyxDQUFDO3dCQUVELElBQUksQ0FBQyxDQUFDOzRCQUNGLE1BQU0sQ0FBQyxPQUFPLEtBQUssV0FBVyxDQUFDO3dCQUNuQyxDQUFDO29CQUNMLENBQUM7Z0JBQ0wsQ0FBQztnQkFFTyxnREFBd0IsR0FBaEM7b0JBRUksRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQzt3QkFDL0IsSUFBSSxDQUFDLG9CQUFvQixFQUFFLENBQUM7b0JBQ2hDLENBQUM7b0JBRUQsSUFBSSxDQUFDLENBQUM7d0JBQ0YsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7b0JBQ2pDLENBQUM7Z0JBQ0wsQ0FBQztnQkFFTyw0Q0FBb0IsR0FBNUI7b0JBRUksRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLFdBQVcsS0FBSyxDQUFDLElBQUksTUFBTSxDQUFDLFdBQVcsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUN6RCxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztvQkFDaEMsQ0FBQztvQkFFRCxJQUFJLENBQUMsQ0FBQzt3QkFDRixJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztvQkFDakMsQ0FBQztnQkFDTCxDQUFDO2dCQUlELHNCQUFZLCtDQUFvQjt5QkFBaEM7d0JBQ0ksTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO29CQUMxRCxDQUFDOzs7bUJBQUE7Z0JBRU8scUNBQWEsR0FBckI7b0JBQ0ksTUFBTSxDQUFDLGNBQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDdkMsQ0FBQztnQkFFTyxzQ0FBYyxHQUF0QjtvQkFDSSxNQUFNLENBQUMsY0FBTSxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsR0FBRyxDQUFDO2dCQUNyQyxDQUFDO2dCQUVPLHNDQUFjLEdBQXRCO29CQUNJLEVBQUUsQ0FBQyxDQUFDLHlCQUFXLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLHlCQUFXLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNuRixNQUFNLENBQUMseUJBQVcsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7b0JBQzlDLENBQUM7b0JBQ0QsRUFBRSxDQUFDLENBQUMsY0FBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7d0JBQ2hCLE1BQU0sQ0FBQyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUNqRCxDQUFDO29CQUFDLElBQUksQ0FBQyxDQUFDO3dCQUNKLE1BQU0sQ0FBQyxjQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO29CQUNyRSxDQUFDO2dCQUNMLENBQUM7Z0JBRU8sNENBQW9CLEdBQTVCO29CQUNJLE1BQU0sQ0FBQyxjQUFNLENBQUMsTUFBTSxJQUFJLE1BQU0sQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO2dCQUN0RixDQUFDO2dCQUdELHNCQUFXLG1DQUFRO3lCQUFuQjt3QkFDSSxNQUFNLENBQXNCLElBQUksQ0FBQyxTQUFTLENBQUM7b0JBQy9DLENBQUM7OzttQkFBQTtnQkFFRCxzQkFBVyxvQ0FBUzt5QkFBcEI7d0JBQ0ksTUFBTSxDQUFZLElBQUksQ0FBQyxhQUFhLENBQUMsbUJBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztvQkFDL0QsQ0FBQzs7O21CQUFBO2dCQUVELHNCQUFXLG9DQUFTO3lCQUFwQjt3QkFDSSxNQUFNLENBQVksSUFBSSxDQUFDLGFBQWEsQ0FBQyxlQUFTLENBQUMsVUFBVSxDQUFDLENBQUM7b0JBQy9ELENBQUM7OzttQkFBQTtnQkFyVGEsMEJBQVksR0FBVywrQkFBK0IsQ0FBQztnQkFDdEQsaUNBQW1CLEdBQXVCLGtCQUFrQixDQUFDLFNBQVMsQ0FBQztnQkFxVDFGLG9CQUFDO1lBQUQsQ0F2VEEsQUF1VEMsQ0F2VDBDLHlCQUFXLEdBdVRyRDtZQXZURCxvQ0F1VEMsQ0FBQTs7Ozs7Ozs7UUNuVlksR0FBRzs7Ozs7OztZQUFILGtCQUFBLEdBQUcsR0FBRyxJQUFJLHVCQUFhLEVBQUUsQ0FBQSxDQUFDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUNTdkM7Z0JBQXVDLDZCQUFLO2dCQVl4QyxtQkFBbUIsRUFBVSxFQUFFLENBQVksRUFBRSxDQUFZO29CQUExQixpQkFBWSxHQUFaLEtBQVk7b0JBQUUsaUJBQVksR0FBWixLQUFZO29CQUNyRCxrQkFBTSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO29CQURELE9BQUUsR0FBRixFQUFFLENBQVE7b0JBR3pCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO29CQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQztvQkFDZixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztvQkFFckIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUNaLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFFdEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxDQUFDO29CQUMzQyxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFFYixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7b0JBQ2pCLElBQUksQ0FBQyxFQUFFLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztvQkFDNUIsSUFBSSxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUMsYUFBYSxHQUFHLEtBQUssQ0FBQztnQkFDeEMsQ0FBQztnQkFFTSx3QkFBSSxHQUFYO29CQUNJLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDdkIsQ0FBQztnQkFJTSxrQ0FBYyxHQUFyQixjQUFnQyxDQUFDO2dCQUMxQix3Q0FBb0IsR0FBM0IsY0FBc0MsQ0FBQztnQkFDaEMsZ0NBQVksR0FBbkI7b0JBQ0ksR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRSxDQUFDO3dCQUM5QyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsaUJBQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQzNELENBQUM7Z0JBQ0wsQ0FBQztnQkFDTSwrQkFBVyxHQUFsQixVQUFtQixFQUFVO29CQUV6QixNQUFNLENBQUMsRUFBRSxDQUFDO2dCQUNkLENBQUM7Z0JBRVMsK0JBQVcsR0FBckI7b0JBQ0ksSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsc0JBQVksQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQzlFLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLHNCQUFZLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxzQkFBWSxDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUM5RSxDQUFDO29CQUNELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDcEIsQ0FBQztnQkFJTSwyQkFBTyxHQUFkO29CQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUN0QyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDdEIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3RCLGdCQUFLLENBQUMsT0FBTyxXQUFFLENBQUM7Z0JBQ3BCLENBQUM7Z0JBRU0sd0JBQUksR0FBWDtvQkFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7d0JBQzNCLE1BQU0sQ0FBQztvQkFDWCxDQUFDO29CQUNELElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO29CQUN6QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztvQkFDcEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDdkIsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7Z0JBQzFCLENBQUM7Z0JBRU0sd0JBQUksR0FBWDtvQkFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7d0JBQzNCLE1BQU0sQ0FBQztvQkFDWCxDQUFDO29CQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO29CQUN4QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztvQkFDekIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDdkIsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7Z0JBQzFCLENBQUM7Z0JBRVMseUJBQUssR0FBZjtvQkFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLHVCQUFhLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDdkUsQ0FBQztnQkFFUyw2QkFBUyxHQUFuQjtvQkFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDcEgsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ3ZELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDNUgsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQzNELENBQUM7Z0JBRVMsZ0NBQVksR0FBdEI7b0JBQ0ksSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQzNCLENBQUM7Z0JBRVMsZ0NBQVksR0FBdEI7b0JBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7b0JBQ3JCLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO2dCQUN2QyxDQUFDO2dCQUVTLGlDQUFhLEdBQXZCLFVBQXdCLE9BQVk7b0JBQ2hDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO29CQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLENBQUM7Z0JBQ3JGLENBQUM7Z0JBRVMsa0NBQWMsR0FBeEIsVUFBeUIsT0FBWTtvQkFDakMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUM7d0JBQ2hHLE9BQU8sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO29CQUM1QixDQUFDLENBQUMsQ0FBQztnQkFDUCxDQUFDO2dCQUVTLDBDQUFzQixHQUFoQztvQkFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLHVCQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQy9ELENBQUM7Z0JBRVMscUNBQWlCLEdBQTNCO29CQUNJLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQzt3QkFDM0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDMUIsQ0FBQztvQkFFRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUM7d0JBQzNCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFLENBQUM7b0JBQzFCLENBQUM7Z0JBQ0wsQ0FBQztnQkFFYSwwQkFBZ0IsR0FBOUIsVUFBK0IsSUFBWSxFQUFFLFFBQWE7b0JBQ3RELE1BQU0sQ0FBQyxzQkFBWSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztnQkFDekQsQ0FBQztnQkFFUyw0QkFBUSxHQUFsQjtvQkFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDLHVCQUFhLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUN4RSxDQUFDO2dCQUNMLGdCQUFDO1lBQUQsQ0F6SUEsQUF5SUMsQ0F6SXNDLGVBQUssR0F5STNDO1lBeklELGdDQXlJQyxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUNqSkQ7Z0JBQThCLDRCQUFhO2dCQUN2QyxrQkFBWSxDQUFTLEVBQUUsQ0FBUyxFQUFFLFFBQWEsRUFBRSxPQUFZLEVBQUUsUUFBZ0IsRUFBRSxhQUFxQixFQUFFLFFBQXlCO29CQUF6Qix3QkFBeUIsR0FBekIsZ0JBQXlCO29CQUM3SCxrQkFBTSx5QkFBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksRUFDaEMsQ0FBQyxFQUNELENBQUMsRUFDRCxRQUFRLEVBQ1IsUUFBUSxFQUNSLE9BQU8sRUFDUCxhQUFhLEdBQUcsT0FBTyxFQUN2QixhQUFhLEVBQ2IsYUFBYSxHQUFHLE9BQU8sRUFDdkIsYUFBYSxDQUFDLENBQUM7b0JBRW5CLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO29CQUN6QixJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7Z0JBQ3BDLENBQUM7Z0JBRU0scUNBQWtCLEdBQXpCLFVBQTBCLE1BQVcsRUFBRSxPQUFZO29CQUMvQyxnQkFBSyxDQUFDLGtCQUFrQixZQUFDLE1BQU0sRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFFOUMsQ0FBQztnQkFFTSxxQ0FBa0IsR0FBekIsVUFBMEIsTUFBVyxFQUFFLE9BQVk7b0JBQy9DLGdCQUFLLENBQUMsa0JBQWtCLFlBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUU5QyxDQUFDO2dCQUVNLGtDQUFlLEdBQXRCLFVBQXVCLElBQVk7b0JBQy9CLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxHQUFHLE9BQU8sRUFBRSxJQUFJLEVBQUUsSUFBSSxHQUFHLE9BQU8sRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDL0QsQ0FBQztnQkFFRCxzQkFBVywyQkFBSzt5QkFBaEI7d0JBQ0ksTUFBTSxDQUFDLHlCQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDO29CQUMxQyxDQUFDOzs7bUJBQUE7Z0JBQ0wsZUFBQztZQUFELENBbENBLEFBa0NDLENBbEM2QixNQUFNLENBQUMsTUFBTSxHQWtDMUM7WUFsQ0QsZ0NBa0NDLENBQUE7WUFFRDtnQkFBc0Msb0NBQWM7Z0JBQXBEO29CQUFzQyw4QkFBYztnQkFjcEQsQ0FBQztnQkFiVSw2Q0FBa0IsR0FBekIsVUFBMEIsTUFBVyxFQUFFLE9BQVk7b0JBQy9DLGdCQUFLLENBQUMsa0JBQWtCLFlBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUU5QyxDQUFDO2dCQUVNLDZDQUFrQixHQUF6QixVQUEwQixNQUFXLEVBQUUsT0FBWTtvQkFDL0MsZ0JBQUssQ0FBQyxrQkFBa0IsWUFBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7Z0JBRTlDLENBQUM7Z0JBRUQsc0JBQVcsbUNBQUs7eUJBQWhCO3dCQUNJLE1BQU0sQ0FBQyx5QkFBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQztvQkFDMUMsQ0FBQzs7O21CQUFBO2dCQUNMLHVCQUFDO1lBQUQsQ0FkQSxBQWNDLENBZHFDLHdCQUFjLEdBY25EO1lBZEQsZ0RBY0MsQ0FBQTtZQUVEO2dCQUE0QiwwQkFBSztnQkFXN0IsZ0JBQVksQ0FBUyxFQUFFLENBQVMsRUFBRSxHQUFXLEVBQUUsT0FBZSxFQUFFLFlBQW9CLEVBQUUsVUFBdUQ7b0JBQXZELDBCQUF1RCxHQUF2RCxpQkFBdUQ7b0JBQ3pJLGtCQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDWixJQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztvQkFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQztvQkFDdkMsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsR0FBRyxFQUFFLE9BQU8sQ0FBQyxDQUFDO29CQUVuRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsR0FBRyxFQUFFLFlBQVksQ0FBQyxDQUFDO29CQUNqRSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO29CQUNwQyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3BDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFFeEQsRUFBRSxDQUFDLENBQUMsVUFBVSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQ3RCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDMUUsQ0FBQztvQkFDRCxJQUFJLENBQUMsQ0FBQzt3QkFDRixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztvQkFDckIsQ0FBQztvQkFDRCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsTUFBTSxFQUFFLG1CQUFTLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxFQUFFLG1CQUFTLENBQUMsa0JBQWtCLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQzlILENBQUM7Z0JBRU0sMkNBQTBCLEdBQWpDLFVBQWtDLFFBQWdCLEVBQUUsU0FBaUIsRUFBRSxRQUF3QixFQUFFLFNBQXlCO29CQUFuRCx3QkFBd0IsR0FBeEIsZUFBd0I7b0JBQUUseUJBQXlCLEdBQXpCLGdCQUF5QjtvQkFDdEgsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLENBQUM7b0JBQ3pCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztvQkFDMUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEdBQUcsU0FBUyxDQUFDO29CQUM1QixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO29CQUU1QixJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztvQkFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUNqQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsR0FBRyxLQUFLLENBQUM7b0JBRTdDLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDckIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO3dCQUNoQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsUUFBUSxDQUFDO3dCQUM3QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO29CQUNuQyxDQUFDO29CQUNELElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztvQkFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUM5QixJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUM7b0JBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7b0JBRWhDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO2dCQUM1QyxDQUFDO2dCQUVNLHlDQUF3QixHQUEvQixVQUFnQyxRQUFnQixFQUFFLFNBQWlCLEVBQUUsUUFBd0IsRUFBRSxTQUF5QjtvQkFBbkQsd0JBQXdCLEdBQXhCLGVBQXdCO29CQUFFLHlCQUF5QixHQUF6QixnQkFBeUI7b0JBQ3BILElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDO29CQUN6QixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUM7b0JBQzFCLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLFNBQVMsQ0FBQztvQkFDNUIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFFNUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEdBQUcsRUFBRSxDQUFDO29CQUNqQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQztvQkFDOUIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsbUJBQW1CLEdBQUcsS0FBSyxDQUFDO29CQUMvQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxDQUFDLEVBQUUsQ0FBQztvQkFFekIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDO3dCQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQyxDQUFDO3dCQUMvQixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxRQUFRLENBQUM7d0JBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDLENBQUM7b0JBQ25DLENBQUM7b0JBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7b0JBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztvQkFDcEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFDO29CQUMvQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO29CQUVoQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7Z0JBQzFDLENBQUM7Z0JBRU0sdUJBQU0sR0FBYjtvQkFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDeEMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO29CQUN0QixDQUFDO2dCQUNMLENBQUM7Z0JBRVMsMkJBQVUsR0FBcEIsY0FBOEIsQ0FBQztnQkFFeEIsMEJBQVMsR0FBaEI7b0JBQ0ksSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO29CQUNsQixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEMsQ0FBQztnQkFFTSxtQ0FBa0IsR0FBekIsVUFBMEIsT0FBZTtvQkFDckMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDO3dCQUM3QixJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLENBQUM7d0JBQ3hELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDMUIsQ0FBQztvQkFDRCxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDO3dCQUNsQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDLENBQUM7d0JBQ3pELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDMUIsQ0FBQztnQkFDTCxDQUFDO2dCQUVTLGlDQUFnQixHQUExQjtvQkFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQ2xDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDO29CQUNuQyxDQUFDO29CQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQ3hDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO29CQUNwQyxDQUFDO29CQUNELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDMUIsQ0FBQztnQkFFUywrQkFBYyxHQUF4QjtvQkFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7d0JBQ2pDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDO29CQUNsQyxDQUFDO29CQUNELElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7d0JBQ3pDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDO29CQUNyQyxDQUFDO29CQUNELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDMUIsQ0FBQztnQkFFUywrQkFBYyxHQUF4QjtvQkFDSSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQ3JCLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQzs0QkFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUM7d0JBQ25ELENBQUM7d0JBQ0QsSUFBSSxDQUFDLENBQUM7NEJBQ0YsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUM7d0JBQ2pELENBQUM7b0JBQ0wsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7d0JBQ3JCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUM7b0JBQ2pFLENBQUM7Z0JBQ0wsQ0FBQztnQkFFRCxzQkFBVyxpQ0FBYTt5QkFBeEI7d0JBQ0ksRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsS0FBSyxNQUFNLENBQUMsQ0FBQyxDQUFDOzRCQUM3QixNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDO3dCQUNsQyxDQUFDO3dCQUNELElBQUksQ0FBQyxDQUFDOzRCQUNGLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7d0JBQ2xDLENBQUM7b0JBQ0wsQ0FBQzs7O21CQUFBO2dCQUVELHNCQUFjLHFDQUFpQjt5QkFBL0I7d0JBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDbkYsQ0FBQzs7O21CQUFBO2dCQUVELHNCQUFjLHFDQUFpQjt5QkFBL0I7d0JBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztvQkFDcEYsQ0FBQzs7O21CQUFBO2dCQUVELHNCQUFXLDRCQUFRO3lCQUFuQjt3QkFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7b0JBQzFCLENBQUM7OzttQkFBQTtnQkFFRCxzQkFBVyw2QkFBUzt5QkFBcEI7d0JBQ0ksTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDO29CQUMzQixDQUFDOzs7bUJBQUE7Z0JBQ0wsYUFBQztZQUFELENBbktBLEFBbUtDLENBbksyQixlQUFLLEdBbUtoQztZQW5LRCw0QkFtS0MsQ0FBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O1lDdk5EO2dCQUEwQyxnQ0FBUztnQkFNL0M7b0JBQ0ksa0JBQU0sbUJBQVMsQ0FBQyxhQUFhLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO2dCQUN6QyxDQUFDO2dCQUlNLHFDQUFjLEdBQXJCO29CQUNJLElBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxFQUFFLGtCQUFrQixDQUFDLENBQUM7b0JBQ2pHLElBQUksQ0FBQyxFQUFFLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFFMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLGlCQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsRUFBRSxJQUFJLEVBQUUsa0JBQWtCLEVBQUUsb0JBQW9CLEVBQUUsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUMsQ0FBQyxDQUFDO29CQUNqTCxJQUFJLENBQUMsU0FBUyxDQUFDLDBCQUEwQixDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFDbkQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO29CQUNsRCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBQzFDLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUMzRCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7b0JBRXJDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxpQkFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLEVBQUUsSUFBSSxFQUFFLGtCQUFrQixFQUFFLG9CQUFvQixFQUFFLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUMsQ0FBQyxDQUFDO29CQUN2TCxJQUFJLENBQUMsV0FBVyxDQUFDLDBCQUEwQixDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQztvQkFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDO29CQUN0RCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7b0JBQzVDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQy9ELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFFdkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQztvQkFDaEUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztvQkFFakUsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLEVBQUUsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUM5SSxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3hDLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdkMsQ0FBQztnQkFFTyxnQ0FBUyxHQUFqQixVQUFrQixDQUFTLEVBQUUsQ0FBUyxFQUFFLFFBQWEsRUFBRSxNQUFjO29CQUNqRSxJQUFJLE1BQU0sR0FBRyxJQUFJLDJCQUFnQixDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLEVBQUUsY0FBYyxFQUFFLGNBQWMsRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO29CQUNwSSxNQUFNLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxFQUFFLG1CQUFTLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztvQkFDMUUsTUFBTSxDQUFDLFdBQVcsRUFBRSxDQUFDO29CQUNyQixNQUFNLENBQUMsTUFBTSxDQUFDO2dCQUNsQixDQUFDO2dCQUVPLHdDQUFpQixHQUF6QjtvQkFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLENBQUM7b0JBQ3BFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLENBQUM7Z0JBQ2hFLENBQUM7Z0JBRU8sb0NBQWEsR0FBckI7b0JBQ0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxDQUFDO29CQUNuRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDM0MsQ0FBQztnQkFJTSx3Q0FBaUIsR0FBeEI7b0JBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxDQUFDO29CQUM3RCxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2dCQUMxRSxDQUFDO2dCQUVNLHNDQUFlLEdBQXRCO29CQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQztvQkFDNUQsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2dCQUM3QyxDQUFDO2dCQUVNLGtDQUFXLEdBQWxCO29CQUNJLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQy9CLENBQUM7Z0JBRU0sbUNBQVksR0FBbkI7b0JBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsbUJBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDckQsQ0FBQztnQkFJTSwyQkFBSSxHQUFYO29CQUNJLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO3dCQUNuQixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztvQkFDN0IsQ0FBQztvQkFDRCxJQUFJLENBQUMsQ0FBQzt3QkFDRixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7b0JBQ3pCLENBQUM7b0JBQ0QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztvQkFDeEMsTUFBTSxDQUFDLGdCQUFLLENBQUMsSUFBSSxXQUFFLENBQUM7Z0JBQ3hCLENBQUM7Z0JBRVMsbUNBQVksR0FBdEI7b0JBQ0ksZ0JBQUssQ0FBQyxZQUFZLFdBQUUsQ0FBQztvQkFDckIsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxLQUFLLG1CQUFTLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQzt3QkFDbkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO29CQUNyQyxDQUFDO29CQUNELElBQUksQ0FBQyxRQUFRLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztnQkFDekMsQ0FBQztnQkFFUyxrQ0FBVyxHQUFyQjtvQkFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLG1CQUFTLENBQUMsZ0JBQWdCLENBQUMsOEJBQW9CLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUN0RixFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7d0JBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSw4QkFBb0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLDhCQUFvQixDQUFDLGFBQWEsQ0FBQyxDQUFDO29CQUM5RixDQUFDO29CQUNELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDcEIsQ0FBQztnQkFFTSxrQ0FBVyxHQUFsQixVQUFtQixFQUFVO29CQUN6QixNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDMUQsQ0FBQztnQkFJRCxzQkFBVyxrQ0FBUTt5QkFBbkI7d0JBQ0ksTUFBTSxDQUF1QixJQUFJLENBQUMsU0FBUyxDQUFDO29CQUNoRCxDQUFDOzs7bUJBQUE7Z0JBRUQsc0JBQVcscUNBQVc7eUJBQXRCO3dCQUNJLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sS0FBSyxtQkFBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO29CQUM5RCxDQUFDOzs7bUJBQUE7Z0JBRUwsbUJBQUM7WUFBRCxDQXRIQSxBQXNIQyxDQXRIeUMsbUJBQVMsR0FzSGxEO1lBdEhELG1DQXNIQyxDQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7WUNySEQ7Z0JBQWtELHdDQUFZO2dCQUE5RDtvQkFBa0QsOEJBQVk7Z0JBaUM5RCxDQUFDO2dCQTlCVSxpREFBa0IsR0FBekIsVUFBMEIsWUFBMkI7b0JBQ2pELElBQU0sUUFBUSxHQUFHLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFDeEMsSUFBTSxRQUFRLEdBQUcsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDO29CQUN4QyxNQUFNLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO3dCQUNmLEtBQUssdUJBQWEsQ0FBQyxXQUFXOzRCQUMxQixJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxDQUFDOzRCQUM1QixLQUFLLENBQUM7b0JBQ2QsQ0FBQztnQkFDTCxDQUFDO2dCQUlNLG1EQUFvQixHQUEzQjtvQkFDSSxJQUFJLENBQUMsU0FBUyxDQUFDLGtCQUFrQixFQUFFLENBQUM7Z0JBQ3hDLENBQUM7Z0JBRU0seUNBQVUsR0FBakI7b0JBQ0ksSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO29CQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxFQUFFLENBQUMsbUJBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztnQkFDbEQsQ0FBQztnQkFJRCxzQkFBVyxzQ0FBSTt5QkFBZjt3QkFDSSxNQUFNLENBQUMsb0JBQW9CLENBQUMsYUFBYSxDQUFDO29CQUM5QyxDQUFDOzs7bUJBQUE7Z0JBRUQsc0JBQVcseUNBQU87eUJBQWxCO3dCQUNJLE1BQU0sQ0FBZSxJQUFJLENBQUMsY0FBYyxDQUFDO29CQUM3QyxDQUFDOzs7bUJBQUE7Z0JBL0JhLGtDQUFhLEdBQVcsc0JBQXNCLENBQUM7Z0JBZ0NqRSwyQkFBQztZQUFELENBakNBLEFBaUNDLENBakNpRCxzQkFBWSxHQWlDN0Q7WUFqQ0QsMkNBaUNDLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQ2pDRDtnQkFBMEMsZ0NBQUs7Z0JBSzNDO29CQUNJLGtCQUFNLENBQUMsRUFBRSxDQUFDLEVBQUUsY0FBYyxFQUFFLEtBQUssQ0FBQyxDQUFDO29CQUg3QixnQkFBVyxHQUFZLEtBQUssQ0FBQztvQkFLbkMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO29CQUNaLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDMUIsQ0FBQztnQkFFTSwyQkFBSSxHQUFYO29CQUNJLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQVMsQ0FBQztvQkFDM0IsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7b0JBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyw4QkFBb0IsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ3RGLEVBQUUsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLDhCQUFvQixDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwRCxDQUFDO2dCQUNMLENBQUM7Z0JBRU0scUNBQWMsR0FBckI7b0JBQ0ksSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUNwQixDQUFDO2dCQUVNLCtCQUFRLEdBQWY7b0JBQ0ksRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQ2IsTUFBTSxDQUFDO29CQUNYLENBQUM7b0JBRUQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLGdCQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3BILElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxHQUFHLG1CQUFtQixDQUFDO29CQUN0QyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7b0JBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUU3RSxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUM7b0JBQ2hDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDL0IsQ0FBQztnQkFFTSxnQ0FBUyxHQUFoQjtvQkFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztvQkFDNUIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLEVBQUUsQ0FBQztvQkFDaEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO2dCQUM5QixDQUFDO2dCQUVNLGdDQUFTLEdBQWhCLFVBQWlCLFNBQXlCO29CQUF6Qix5QkFBeUIsR0FBekIsZ0JBQXlCO29CQUN0QyxFQUFFLENBQUMsQ0FBQyxTQUFTLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQzt3QkFDckIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO29CQUNyQyxDQUFDO29CQUNELElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDL0IsQ0FBQztnQkFFTSxnQ0FBUyxHQUFoQixVQUFpQixPQUF1QixFQUFFLE1BQXVCO29CQUFoRCx1QkFBdUIsR0FBdkIsY0FBdUI7b0JBQUUsc0JBQXVCLEdBQXZCLGNBQXVCO29CQUM3RCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQzt3QkFDYixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDNUIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO29CQUNyQixDQUFDO29CQUNELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUM7d0JBQzlCLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxPQUFPLEVBQUUsTUFBTSxDQUFDLENBQUM7b0JBQ3JELENBQUM7b0JBQ0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztnQkFDaEMsQ0FBQztnQkFFRCxzQkFBWSxrQ0FBUTt5QkFBcEI7d0JBQ0ksTUFBTSxDQUF1QixJQUFJLENBQUMsU0FBUyxDQUFDO29CQUNoRCxDQUFDOzs7bUJBQUE7Z0JBQ0wsbUJBQUM7WUFBRCxDQWxFQSxBQWtFQyxDQWxFeUMsZUFBSyxHQWtFOUM7WUFsRUQsbUNBa0VDLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQzdERDtnQkFBa0Qsd0NBQVk7Z0JBTzFELDhCQUFZLFFBQWE7b0JBQ3JCLGtCQUFNLFFBQVEsRUFBRSxJQUFJLEVBQUUsb0JBQW9CLENBQUMsYUFBYSxDQUFDLENBQUM7b0JBTHRELFdBQU0sR0FBa0MsRUFBRSxDQUFDO29CQUMzQyxvQkFBZSxHQUFZLEtBQUssQ0FBQztvQkFLckMsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7Z0JBQzFCLENBQUM7Z0JBRU0sd0RBQXlCLEdBQWhDO29CQUNJLE1BQU0sQ0FBQzt3QkFDSCx1QkFBYSxDQUFDLGNBQWM7d0JBQzVCLHVCQUFhLENBQUMsY0FBYzt3QkFDNUIsdUJBQWEsQ0FBQyxVQUFVO3dCQUN4Qix1QkFBYSxDQUFDLFVBQVU7d0JBQ3hCLHVCQUFhLENBQUMsZUFBZTtxQkFDaEMsQ0FBQztnQkFDTixDQUFDO2dCQUVNLGlEQUFrQixHQUF6QixVQUEwQixZQUEyQjtvQkFBckQsaUJBc0NDO29CQXJDRyxJQUFNLFFBQVEsR0FBRyxZQUFZLENBQUMsT0FBTyxFQUFFLENBQUM7b0JBQ3hDLElBQU0sUUFBUSxHQUFHLFlBQVksQ0FBQyxPQUFPLEVBQUUsQ0FBQztvQkFFeEMsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQzt3QkFDZixLQUFLLHVCQUFhLENBQUMsY0FBYzs0QkFDN0IsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDOzRCQUNwQixLQUFLLENBQUM7d0JBRVYsS0FBSyx1QkFBYSxDQUFDLGNBQWM7NEJBQzdCLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7NEJBQzdCLEtBQUssQ0FBQzt3QkFFVixLQUFLLHVCQUFhLENBQUMsVUFBVTs0QkFDekIsSUFBSSxNQUFNLEdBQVcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQzs0QkFDN0MsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7Z0NBQ2xCLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLENBQUM7Z0NBQ3pCLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dDQUNoQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxjQUFRLEtBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFBLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0NBQ3hFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDOzRCQUNwQyxDQUFDOzRCQUNELEtBQUssQ0FBQzt3QkFFVixLQUFLLHVCQUFhLENBQUMsVUFBVTs0QkFDekIsSUFBSSxNQUFNLEdBQVcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQzs0QkFDN0MsRUFBRSxDQUFDLENBQUMsTUFBTSxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUM7Z0NBQ2xCLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztnQ0FDZCxJQUFJLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLENBQUM7NEJBQ3JDLENBQUM7NEJBQ0QsS0FBSyxDQUFDO3dCQUVWLEtBQUssdUJBQWEsQ0FBQyxlQUFlOzRCQUM5QixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRSxDQUFDO2dDQUNqQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQzs0QkFDakQsQ0FBQzs0QkFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzs0QkFDOUIsS0FBSyxDQUFDO29CQUNkLENBQUM7Z0JBQ0wsQ0FBQztnQkFFTSwwQ0FBVyxHQUFsQjtvQkFDSSxJQUFJLENBQUMsTUFBTSxHQUFHLEVBQUUsQ0FBQztnQkFDckIsQ0FBQztnQkFFTSx1Q0FBUSxHQUFmO29CQUNJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBYSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7Z0JBQ25HLENBQUM7Z0JBRU0sMkNBQVksR0FBbkI7b0JBQ0ksRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLHNCQUFzQixLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUM7d0JBQ3pFLE1BQU0sQ0FBQztvQkFDWCxDQUFDO29CQUNELElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztnQkFDcEIsQ0FBQztnQkFFTSw2Q0FBYyxHQUFyQixVQUFzQixPQUFlO29CQUNqQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxJQUFJLEdBQUcsS0FBSyxDQUFDO2dCQUNqRCxDQUFDO2dCQUdPLDJDQUFZLEdBQXBCO29CQUNJLElBQUksQ0FBQyxHQUFHLElBQUksc0JBQVksRUFBRSxDQUFDO29CQUUzQixJQUFJLENBQUMsWUFBWSxHQUFHLGNBQVEsQ0FBQyxDQUFDO2dCQUNsQyxDQUFDO2dCQUVPLHVDQUFRLEdBQWhCLFVBQWlCLE9BQWU7b0JBQzVCLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJLElBQUksQ0FBQztnQkFDeEMsQ0FBQztnQkFFTyw0Q0FBYSxHQUFyQixVQUFzQixLQUFhO29CQUMvQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUM7b0JBQzlCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM1QixDQUFDO2dCQUVPLGdEQUFpQixHQUF6QixVQUEwQixRQUFnQjtvQkFDdEMsSUFBSSxLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ25ELEVBQUUsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7d0JBQ2QsSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQzt3QkFDekQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO3dCQUN4RSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQzt3QkFDL0IsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsQ0FBQztvQkFDM0IsQ0FBQztvQkFDRCxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sS0FBSyxDQUFDLElBQUksS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzt3QkFDaEQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsQ0FBQzt3QkFDekIsSUFBSSxDQUFDLGVBQWUsR0FBRyxLQUFLLENBQUM7b0JBQ2pDLENBQUM7Z0JBQ0wsQ0FBQztnQkFJRCxzQkFBWSx5Q0FBTzt5QkFBbkI7d0JBQ0ksTUFBTSxDQUFlLElBQUksQ0FBQyxhQUFhLENBQUM7b0JBQzVDLENBQUM7OzttQkFBQTtnQkFFRCxzQkFBWSx3REFBc0I7eUJBQWxDO3dCQUNJLE1BQU0sQ0FBQyxLQUFLLENBQUM7b0JBQ2pCLENBQUM7OzttQkFBQTtnQkFFRCxzQkFBVyxzQ0FBSTt5QkFBZjt3QkFDSSxNQUFNLENBQUMsb0JBQW9CLENBQUMsYUFBYSxDQUFDO29CQUM5QyxDQUFDOzs7bUJBQUE7Z0JBM0hhLGtDQUFhLEdBQVcsc0JBQXNCLENBQUM7Z0JBNEhqRSwyQkFBQztZQUFELENBN0hBLEFBNkhDLENBN0hpRCxzQkFBWSxHQTZIN0Q7WUE3SEQsMkNBNkhDLENBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7OztZQ25JRDtnQkFBNkMsbUNBQVk7Z0JBQXpEO29CQUE2Qyw4QkFBWTtnQkFjekQsQ0FBQztnQkFSVSw4QkFBSSxHQUFYO29CQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEVBQUUsQ0FBQyxtQkFBUyxDQUFDLFVBQVUsQ0FBQyxDQUFDO2dCQUNsRCxDQUFDO2dCQUdELHNCQUFXLGlDQUFJO3lCQUFmO3dCQUNJLE1BQU0sQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDO29CQUN6QyxDQUFDOzs7bUJBQUE7Z0JBWmEsNkJBQWEsR0FBVyxpQkFBaUIsQ0FBQztnQkFhNUQsc0JBQUM7WUFBRCxDQWRBLEFBY0MsQ0FkNEMsc0JBQVksR0FjeEQ7WUFkRCxzQ0FjQyxDQUFBIiwiZmlsZSI6ImFwcC5qcyIsInNvdXJjZXNDb250ZW50IjpbImV4cG9ydCBkZWZhdWx0IGNsYXNzIE5vdGlmaWNhdGlvbnMge1xuICAgIHN0YXRpYyBCT09UX0lOSVQ6IHN0cmluZyA9ICdib290SW5pdCc7XG4gICAgc3RhdGljIEJPT1RfQ09NUExFVEU6IHN0cmluZyA9ICdib290Q29tcGxldGUnO1xuICAgIHN0YXRpYyBUUkFDS19FVkVOVDogc3RyaW5nID0gJ3RyYWNrRXZlbnQnO1xuICAgIHN0YXRpYyBBUFBfUkVTSVpFRDogc3RyaW5nID0gJ2FwcFJlc2l6ZWQnO1xuICAgIFxuICAgIHN0YXRpYyBQQVVTRV9HQU1FOiBzdHJpbmcgPSAncGF1c2VHYW1lJztcbiAgICBzdGF0aWMgUkVTVU1FX0dBTUU6IHN0cmluZyA9ICdyZXN1bWVHYW1lJztcbiAgICBzdGF0aWMgU1RBUlRfR0FNRTogc3RyaW5nID0gJ3N0YXJ0R2FtZSc7XG5cbiAgICBzdGF0aWMgUkVHSVNURVJfUE9QVVA6IHN0cmluZyA9ICdyZWdpc3RlclBvcHVwOydcbiAgICBzdGF0aWMgSElERV9QT1BVUDogc3RyaW5nID0gJ2hpZGVQb3B1cCc7XG4gICAgc3RhdGljIFNIT1dfUE9QVVA6IHN0cmluZyA9ICdzaG93UG9wdXAnO1xuICAgIHN0YXRpYyBBRERfQUxMX1BPUFVQUzogc3RyaW5nID0gJ2FkZEFsbFBvcHVwcyc7XG4gICAgc3RhdGljIEhJREVfQUxMX1BPUFVQUzogc3RyaW5nID0gJ2hpZGVBbGxQb3B1cHMnO1xuICAgIFxufSIsImV4cG9ydCBjbGFzcyBDb25zdGFudHMge1xuICAgIC8vIFN0YXRlc1xuICAgIHN0YXRpYyBTVEFURV9CT09UOiBzdHJpbmcgPSAnYm9vdCc7XG4gICAgc3RhdGljIFNUQVRFX01FTlU6IHN0cmluZyA9ICdtZW51JztcbiAgICBzdGF0aWMgU1RBVEVfR0FNRTogc3RyaW5nID0gJ2dhbWUnO1xuICAgIFxuICAgIC8vIEtleXNcbiAgICBzdGF0aWMgU0FWRV9EQVRBX0tFWTogc3RyaW5nID0gJ3NhdmVEYXRhJztcblxuICAgIC8vIEZvbnRzXG4gICAgc3RhdGljIEZPTlRfUFJFU1NfU1RBUlQ6IHN0cmluZyA9IFwiJ1ByZXNzIFN0YXJ0IDJQJywgY3Vyc2l2ZVwiO1xuXG4gICAgLy8gUG9wdXBzXG4gICAgc3RhdGljIFBPUFVQX09QVElPTlM6IHN0cmluZyA9ICdvcHRpb25zUG9wdXAnO1xuICAgIHN0YXRpYyBQT1BVUF9IRUxQOiBzdHJpbmcgPSAnaGVscFBvcHVwJztcblxuICAgIC8vIENvbG91cnNcbiAgICBzdGF0aWMgQ09MT1JfTUVOVV9DT1BZOiBzdHJpbmcgPSAnIzAwZmYwMCc7XG4gICAgc3RhdGljIENPTE9SX1NMSURFUl9MQUJFTDogc3RyaW5nID0gJyNmZjk3MGYnO1xufVxuXG4vLyBVc2UgdGhpcyB0byBob3VzZSBhbnkgZ2xvYmFsIHN0YXRpYyB2YXJpYWJsZXMgeW91IG1heSB3YW50LlxuLy8gSUUsIGEgZ2xvYmFsIFNGWCBtdXRlIG9yIHZvbHVtZSBwcm9wZXJ0eS5cbmV4cG9ydCBjbGFzcyBHbG9iYWxzIHtcbiAgICBzdGF0aWMgU0NBTEVfUkFUSU86IG51bWJlciA9IDE7XG59IiwiaW1wb3J0IHtNb2RlbH0gZnJvbSAnZGlqb24vbXZjJztcbmltcG9ydCB7IElQbGF5ZXJEYXRhLCBJTWFpbkRhdGEgfSBmcm9tICcuLi91dGlscy9JbnRlcmZhY2VzJztcbmltcG9ydCB7Q29uc3RhbnRzfSBmcm9tICcuLi91dGlscy9TdGF0aWNzJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR2FtZU1vZGVsIGV4dGVuZHMgTW9kZWwge1xuICAgIHB1YmxpYyBzdGF0aWMgTU9ERUxfTkFNRTogc3RyaW5nID0gXCJnYW1lTW9kZWxcIjtcblxuICAgIC8vIFRoaXMgc3RvcmVzIHBsYXllcnMgYXVkaW8gc2V0dGluZ3MgYXMgd2VsbCBhcyBhY2hpZXZlbWVudHMuXG4gICAgcHJvdGVjdGVkIHBsYXllckRhdGE6IElQbGF5ZXJEYXRhID0gbnVsbDtcblxuICAgIC8qIFBMQVlFUiBEQVRBICovXG5cbiAgICBwdWJsaWMgcmVzZXRTYXZlRGF0YSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5wbGF5ZXJEYXRhID0gdGhpcy5jcmVhdGVQbGF5ZXJEYXRhKCk7XG4gICAgICAgIHRoaXMudXBkYXRlQXBwU2V0dGluZ3MoKTtcbiAgICAgICAgdGhpcy5zYXZlUGxheWVyRGF0YSgpO1xuICAgIH0gICBcbiAgICBcbiAgICBwdWJsaWMgdXBkYXRlQXBwU2V0dGluZ3MoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuZ2FtZS5hdWRpby5zcHJpdGVWb2x1bWUgPSB0aGlzLnBsYXllckRhdGEuc2Z4Vm9sdW1lO1xuICAgICAgICB0aGlzLmdhbWUuYXVkaW8uc291bmRWb2x1bWUgPSB0aGlzLnBsYXllckRhdGEubXVzaWNWb2x1bWU7XG4gICAgfVxuXG4gICAgcHVibGljIGxvYWRQbGF5ZXJEYXRhKCk6IHZvaWQge1xuICAgICAgICAvL2NvbnNvbGUud2FybihcIlBsYXllciBEYXRhIGN1cnJlbnRseSBub3QgbG9hZGVkLCB0byBhdm9pZCBpc3N1ZXMgd2l0aCBkYXRhIGtleSBjaGFuZ2VzXCIpO1xuICAgICAgICB0aGlzLnBsYXllckRhdGEgPSB0aGlzLmdhbWUuc3RvcmFnZS5nZXRGcm9tTG9jYWxTdG9yYWdlKENvbnN0YW50cy5TQVZFX0RBVEFfS0VZLCB0cnVlKTtcbiAgICAgICAgXG4gICAgICAgIGlmICh0aGlzLnBsYXllckRhdGEgPT0gbnVsbCkge1xuICAgICAgICAgICAgdGhpcy5wbGF5ZXJEYXRhID0gdGhpcy5jcmVhdGVQbGF5ZXJEYXRhKCk7XG4gICAgICAgIH1cbiAgICBcbiAgICAgICAgdGhpcy51cGRhdGVBcHBTZXR0aW5ncygpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBzYXZlUGxheWVyRGF0YSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5nYW1lLnN0b3JhZ2Uuc2F2ZVRvTG9jYWxTdG9yYWdlKENvbnN0YW50cy5TQVZFX0RBVEFfS0VZLCB0aGlzLnBsYXllckRhdGEpO1xuICAgIH1cblxuICAgIHB1YmxpYyBzYXZlUGxheWVyU2V0dGluZ3MoKTogdm9pZCB7XG4gICAgICAgIHRoaXMucGxheWVyRGF0YS5zZnhWb2x1bWUgPSB0aGlzLmdhbWUuYXVkaW8uc3ByaXRlVm9sdW1lO1xuICAgICAgICB0aGlzLnBsYXllckRhdGEubXVzaWNWb2x1bWUgPSB0aGlzLmdhbWUuYXVkaW8uc291bmRWb2x1bWU7XG4gICAgICAgIHRoaXMuc2F2ZVBsYXllckRhdGEoKTtcbiAgICB9ICAgXG4gICAgXG4gICAgcHJvdGVjdGVkIGNyZWF0ZVBsYXllckRhdGEoKTogSVBsYXllckRhdGEge1xuICAgICAgICBsZXQgZGF0YSA9IDxJUGxheWVyRGF0YT5uZXcgT2JqZWN0KCk7XG4gICAgICAgIFxuICAgICAgICBkYXRhLnNmeFZvbHVtZSA9IDE7XG4gICAgICAgIGRhdGEubXVzaWNWb2x1bWUgPSAxO1xuICAgICAgICBkYXRhLmNhc2ggPSAwO1xuICAgICAgICBkYXRhLnVwZ3JhZGVUaWVycyA9IFswLCAwLCAwXTtcblxuICAgICAgICByZXR1cm4gZGF0YTtcbiAgICB9XG5cbiAgICAvKiBHRVQvU0VUUyAqL1xuXG4gICAgcHVibGljIGdldCBuYW1lKCk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiBHYW1lTW9kZWwuTU9ERUxfTkFNRTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IG1haW5EYXRhKCk6IElNYWluRGF0YSB7XG4gICAgICAgIHJldHVybiA8SU1haW5EYXRhPnRoaXMuX2RhdGFbJ21haW4nXTtcbiAgICB9ICAgIFxufSIsImltcG9ydCB7TWVkaWF0b3IsIENvcHlNb2RlbH0gZnJvbSBcImRpam9uL212Y1wiO1xuaW1wb3J0IHtBcHBsaWNhdGlvbn0gZnJvbSBcImRpam9uL2FwcGxpY2F0aW9uXCI7XG5pbXBvcnQgTm90aWZpY2F0aW9ucyBmcm9tICcuLi91dGlscy9Ob3RpZmljYXRpb25zJztcbmltcG9ydCBHYW1lTW9kZWwgZnJvbSBcIi4uL21vZGVsL0dhbWVNb2RlbFwiO1xuaW1wb3J0IE1EQXBwbGljYXRpb24gZnJvbSAnLi4vTURBcHBsaWNhdGlvbic7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJhc2VNZWRpYXRvciBleHRlbmRzIE1lZGlhdG9yIHtcbiAgICAvKipcbiAgICAgKiBBdHRlbXB0IHRvIHJldHJpZXZlIGFuIGV4aXN0aW5nIG1lZGlhdG9yLCBiYXNlZCBvbiB0aGUgbWVkaWF0b3JzIG5hbWUuXG4gICAgICogQHBhcmVtKG5hbWUsIHN0cmluZyk6IFRoZSBuYW1lIG9mIHRoZSBtZWRpYXRvciB5b3UgYXJlIHRyeWluZyB0byByZXRyaWV2ZS5cbiAgICAgKiBAcGFyZW0odmlld0NvbXAsIGFueSk6IFRoZSB2aWV3IGNvbXBvbmVudCB5b3Ugd2lzaCB0byBiaW5kIHRvIHRoZSBtZWRpYXRvciB5b3UgYXJlIHJldHJpZXZpbmcuXG4gICAgICogUmV0dXJucyB0aGUgbWVkaWF0b3IgaW4gcXVlc3Rpb24sIGlmIGZvdW5kLCB3aXRoIHRoZSBuZXcgdmlld0NvbXBvbmVudCBib3VuZCB0byBpdC4gTnVsbCBvdGhlcndpc2UuXG4gICAgICovXG4gICAgcHVibGljIHN0YXRpYyByZXRyaWV2ZU1lZGlhdG9yKG5hbWU6IHN0cmluZywgdmlld0NvbXA6IGFueSk6IEJhc2VNZWRpYXRvciB7XG4gICAgICAgIGxldCBiYXNlTWVkOiBCYXNlTWVkaWF0b3IgPSA8QmFzZU1lZGlhdG9yPkFwcGxpY2F0aW9uLmdldEluc3RhbmNlKCkucmV0cmlldmVNZWRpYXRvcihuYW1lKTtcbiAgICAgICAgaWYgKGJhc2VNZWQgIT09IG51bGwpIHtcbiAgICAgICAgICAgIGJhc2VNZWQuX3ZpZXdDb21wb25lbnQgPSB2aWV3Q29tcDtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gYmFzZU1lZDtcbiAgICB9XG5cbiAgICAvLyBOZWFybHkgZXZlcnkgbWVkaWF0b3Igd2lsbCBuZWVkIHRvIGxpc3RlbiBmb3IgYW5kIGhhbmRsZSB0aGVzZSB0d28gZXZlbnRzLlxuICAgIC8vIFRoZSByYXJlIGNhc2VzIHRoYXQgZG8gbm90IHNpbXBseSBkbyBub3QgY2FsbCB0aGUgc3VwZXIgaW4gdGhlaXIgbGlzdE5vdGlmaWNhdGlvbkludGVyZXN0cyBmdW5jXG4gICAgcHVibGljIGxpc3ROb3RpZmljYXRpb25JbnRlcmVzdHMoKTogc3RyaW5nW10ge1xuICAgICAgICByZXR1cm4gWyBcbiAgICAgICAgICAgIE5vdGlmaWNhdGlvbnMuQVBQX1JFU0laRURcbiAgICAgICAgXTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0Q29weShncm91cElkOiBzdHJpbmcsIHRleHRJZDogc3RyaW5nKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuY29weU1vZGVsLmdldENvcHkoZ3JvdXBJZCwgdGV4dElkKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0Q29weUFycmF5KGdyb3VwSWQ6IHN0cmluZywgdGV4dElkOiBzdHJpbmcpOiBzdHJpbmdbXSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvcHlNb2RlbC5nZXRDb3B5R3JvdXAoZ3JvdXBJZClbdGV4dElkXTtcbiAgICB9XG5cdFx0XG4gICAgcHVibGljIHJlcXVlc3RQb3B1cChwb3B1cElkOiBzdHJpbmcpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zZW5kTm90aWZpY2F0aW9uKE5vdGlmaWNhdGlvbnMuU0hPV19QT1BVUCwgcG9wdXBJZCk7XG4gICAgfVxuXG4gICAgcHVibGljIGNsb3NlQWxsUG9wdXBzKCk6IHZvaWQge1xuICAgICAgICB0aGlzLnNlbmROb3RpZmljYXRpb24oTm90aWZpY2F0aW9ucy5ISURFX0FMTF9QT1BVUFMpO1xuICAgIH0gICBcbiAgICBcbiAgICBwdWJsaWMgbm90aWZ5UmVzdW1lR2FtZSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zZW5kTm90aWZpY2F0aW9uKE5vdGlmaWNhdGlvbnMuUkVTVU1FX0dBTUUpO1xuICAgIH0gIFxuXG4gICAgcHVibGljIG5vdGlmeVBhdXNlR2FtZSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zZW5kTm90aWZpY2F0aW9uKE5vdGlmaWNhdGlvbnMuUEFVU0VfR0FNRSk7XG4gICAgfVxuICAgIFxuICAgIC8vIGdldHRlciAvIHNldHRlclxuICAgIC8vIG9mZmVyIGFjY2VzcyB0byB0aGUgR2FtZU1vZGVsIGFuZCBDb3B5TW9kZWwgZnJvbSBhbnkgbWVkaWF0b3IgZXh0ZW5kaW5nIEJhc2VNZWRpYXRvclxuICAgIHB1YmxpYyBnZXQgZ2FtZU1vZGVsKCk6IEdhbWVNb2RlbCB7XG4gICAgICAgIHJldHVybiB0aGlzLmxjQXBwLmdhbWVNb2RlbDtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IGNvcHlNb2RlbCgpOiBDb3B5TW9kZWwge1xuICAgICAgICByZXR1cm4gdGhpcy5sY0FwcC5jb3B5TW9kZWw7XG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBnZXQgbGNBcHAoKTogTURBcHBsaWNhdGlvbiB7XG4gICAgICAgIHJldHVybiA8TURBcHBsaWNhdGlvbj5BcHBsaWNhdGlvbi5nZXRJbnN0YW5jZSgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBnZXQgbmFtZSgpOiBzdHJpbmcgeyBcbiAgICAgICAgcmV0dXJuIFwiYmFzZU1lZGlhdG9yX1wiICsgdGhpcy5nYW1lLnJuZC51dWlkKCk7XG4gICAgfVxufSIsImltcG9ydCB7TG9nZ2VyfSBmcm9tIFwiZGlqb24vdXRpbHNcIjtcbmltcG9ydCB7SU5vdGlmaWNhdGlvbn0gZnJvbSBcImRpam9uL2ludGVyZmFjZXNcIjtcbmltcG9ydCB7IEFuYWx5dGljc0V2ZW50TW9kZWwgfSBmcm9tIFwiZGlqb24vY29yZVwiO1xuaW1wb3J0IEJhc2VNZWRpYXRvciBmcm9tICcuL0Jhc2VNZWRpYXRvcic7XG5pbXBvcnQge0NvbnN0YW50c30gZnJvbSAnLi4vdXRpbHMvU3RhdGljcyc7XG5pbXBvcnQgTm90aWZpY2F0aW9ucyBmcm9tICcuLi91dGlscy9Ob3RpZmljYXRpb25zJztcbmltcG9ydCBNREFwcGxpY2F0aW9uIGZyb20gJy4uL01EQXBwbGljYXRpb24nO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBBcHBsaWNhdGlvbk1lZGlhdG9yIGV4dGVuZHMgQmFzZU1lZGlhdG9yIHtcbiAgICBwdWJsaWMgc3RhdGljIE1FRElBVE9SX05BTUU6IHN0cmluZyA9ICdhcHBsaWNhdGlvbk1lZGlhdG9yJztcblxuICAgIC8vIGRpam9uLm12Yy5NZWRpYXRvciBvdmVycmlkZXNcbiAgICBwdWJsaWMgbGlzdE5vdGlmaWNhdGlvbkludGVyZXN0cygpOiBzdHJpbmdbXSB7XG4gICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICBOb3RpZmljYXRpb25zLkJPT1RfSU5JVCxcbiAgICAgICAgICAgIE5vdGlmaWNhdGlvbnMuQk9PVF9DT01QTEVURSxcbiAgICAgICAgICAgIE5vdGlmaWNhdGlvbnMuVFJBQ0tfRVZFTlRcbiAgICAgICAgXVxuICAgIH1cblxuICAgIHB1YmxpYyBoYW5kbGVOb3RpZmljYXRpb24obm90aWZpY2F0aW9uOiBJTm90aWZpY2F0aW9uKSB7XG4gICAgICAgIHN3aXRjaCAobm90aWZpY2F0aW9uLmdldE5hbWUoKSkge1xuICAgICAgICAgICAgY2FzZSBOb3RpZmljYXRpb25zLkJPT1RfSU5JVDpcbiAgICAgICAgICAgICAgICBMb2dnZXIubG9nKHRoaXMsICdOb3RpZmljYXRpb25zLkJPT1RfSU5JVCcpO1xuICAgICAgICAgICAgICAgIHRoaXMudmlld0NvbXBvbmVudC5hZGp1c3RTY2FsZVNldHRpbmdzKCk7XG4gICAgICAgICAgICAgICAgdGhpcy52aWV3Q29tcG9uZW50LmFkanVzdFJlbmRlcmVyU2V0dGluZ3MoKTtcbiAgICAgICAgICAgICAgICB0aGlzLnZpZXdDb21wb25lbnQuYWRkUGx1Z2lucygpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgY2FzZSBOb3RpZmljYXRpb25zLkJPT1RfQ09NUExFVEU6XG4gICAgICAgICAgICAgICAgTG9nZ2VyLmxvZyh0aGlzLCAnTm90aWZpY2F0aW9ucy5CT09UX0NPTVBMRVRFJyk7XG4gICAgICAgICAgICAgICAgdGhpcy5nYW1lLmFzc2V0LnNldERhdGEodGhpcy5nYW1lLmNhY2hlLmdldEpTT04oJ2Fzc2V0cycpKTtcbiAgICAgICAgICAgICAgICB0aGlzLnZpZXdDb21wb25lbnQucmVnaXN0ZXJNb2RlbHMoKTtcbiAgICAgICAgICAgICAgICB0aGlzLnZpZXdDb21wb25lbnQuYm9vdENvbXBsZXRlKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgICBcbiAgICAgICAgICAgIGNhc2UgTm90aWZpY2F0aW9ucy5UUkFDS19FVkVOVDpcbiAgICAgICAgICAgICAgICBsZXQgbW9kZWw6IEFuYWx5dGljc0V2ZW50TW9kZWwgPSA8QW5hbHl0aWNzRXZlbnRNb2RlbD5ub3RpZmljYXRpb24uZ2V0Qm9keSgpO1xuICAgICAgICAgICAgICAgIGlmIChtb2RlbCAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnZpZXdDb21wb25lbnQudHJhY2tFdmVudChtb2RlbCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGJyZWFrOyAgICBcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBub3RpZnlBcHBSZXNpemVkKCk6IHZvaWQge1xuICAgICAgICB0aGlzLnNlbmROb3RpZmljYXRpb24oTm90aWZpY2F0aW9ucy5BUFBfUkVTSVpFRCk7XG4gICAgfVxuICAgIFxuICAgIC8vIGdldHRlciAvIHNldHRlclxuICAgIHB1YmxpYyBnZXQgdmlld0NvbXBvbmVudCgpOiBNREFwcGxpY2F0aW9uIHtcbiAgICAgICAgcmV0dXJuIDxNREFwcGxpY2F0aW9uPnRoaXMuX3ZpZXdDb21wb25lbnQ7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCBuYW1lKCk6c3RyaW5nIHtcbiAgICAgICAgcmV0dXJuIEFwcGxpY2F0aW9uTWVkaWF0b3IuTUVESUFUT1JfTkFNRTtcbiAgICB9XG59IiwiaW1wb3J0IHtTdGF0ZX0gZnJvbSBcImRpam9uL2NvcmVcIjtcbmltcG9ydCBCYXNlTWVkaWF0b3IgZnJvbSBcIi4uL21lZGlhdG9yL0Jhc2VNZWRpYXRvclwiO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBCYXNlU3RhdGUgZXh0ZW5kcyBTdGF0ZSB7XG4gICAgcHVibGljIGFmdGVyQnVpbGQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMucmVzdW1lKCk7XG4gICAgfVxufSIsImltcG9ydCBCYXNlTWVkaWF0b3IgZnJvbSAnLi9CYXNlTWVkaWF0b3InO1xuaW1wb3J0IE5vdGlmaWNhdGlvbnMgZnJvbSAnLi4vdXRpbHMvTm90aWZpY2F0aW9ucyc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEJvb3RNZWRpYXRvciBleHRlbmRzIEJhc2VNZWRpYXRvciB7XG4gICAgcHVibGljIHN0YXRpYyBNRURJQVRPUl9OQU1FOiBzdHJpbmcgPSAnYm9vdE1lZGlhdG9yJztcblx0XHRcbiAgICAvLyBkaWpvbi5tdmMuTWVkaWF0b3Igb3ZlcnJpZGVzXG4gICAgcHVibGljIG9uUmVnaXN0ZXIoKSB7XG4gICAgICAgIHRoaXMuc2VuZE5vdGlmaWNhdGlvbihOb3RpZmljYXRpb25zLkJPT1RfSU5JVCk7XG4gICAgfVxuXHRcdFxuICAgIC8vIHB1YmxpYyBtZXRob2RzXG4gICAgLy8gY2FsbGVkIGZyb20gdmlld0NvbXBvbmVudFxuICAgIHB1YmxpYyBib290Q29tcGxldGUoKSB7XG4gICAgICAgIHRoaXMuc2VuZE5vdGlmaWNhdGlvbihOb3RpZmljYXRpb25zLkJPT1RfQ09NUExFVEUpO1xuICAgIH1cblx0XHRcbiAgICAvLyBnZXR0ZXIgLyBzZXR0ZXJcbiAgICBwdWJsaWMgZ2V0IG5hbWUoKTpzdHJpbmcge1xuICAgICAgICByZXR1cm4gQm9vdE1lZGlhdG9yLk1FRElBVE9SX05BTUU7XG4gICAgfVxufSIsImltcG9ydCBCYXNlU3RhdGUgZnJvbSBcIi4vQmFzZVN0YXRlXCI7XG5pbXBvcnQgQm9vdE1lZGlhdG9yIGZyb20gXCIuLi9tZWRpYXRvci9Cb290TWVkaWF0b3JcIjtcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQm9vdCBleHRlbmRzIEJhc2VTdGF0ZSB7XG4gICAgLy8gUGhhc2VyLlN0YXRlIG92ZXJyaWRlc1xuICAgIHB1YmxpYyBpbml0KCk6IHZvaWQge1xuICAgICAgICB0aGlzLl9tZWRpYXRvciA9IG5ldyBCb290TWVkaWF0b3IodGhpcyk7XG4gICAgfVxuXG4gICAgcHVibGljIHByZWxvYWQoKTogdm9pZCB7XG4gICAgICAgIGlmICh3aW5kb3dbJ3ZlcnNpb24nXSAhPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICAgICB0aGlzLmdhbWUuYXNzZXQuY2FjaGVCdXN0VmVyc2lvbiA9ICdAQHZlcnNpb24nO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuZ2FtZS5hc3NldC5sb2FkSlNPTignZ2FtZV9kYXRhJyk7XG4gICAgICAgIHRoaXMuZ2FtZS5hc3NldC5sb2FkSlNPTignYXNzZXRzJyk7XG4gICAgICAgIHRoaXMuZ2FtZS5hc3NldC5sb2FkSlNPTignY29weScpO1xuICAgICAgICB0aGlzLmdhbWUuYXNzZXQubG9hZFBoeXNpY3MoJ3BoeXNpY3MnKTtcbiAgICB9XG5cbiAgICAvLyBkaWpvbi5jb3JlLlN0YXRlIG92ZXJyaWRlc1xuICAgIHB1YmxpYyBidWlsZEludGVyZmFjZSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5tZWRpYXRvci5ib290Q29tcGxldGUoKTtcbiAgICB9XG5cbiAgICAvLyBwcml2YXRlIG1ldGhvZHNcblxuICAgIC8vIGdldHRlciAvIHNldHRlclxuICAgIHByb3RlY3RlZCBnZXQgbWVkaWF0b3IoKTogQm9vdE1lZGlhdG9yIHtcbiAgICAgICAgcmV0dXJuIDxCb290TWVkaWF0b3I+dGhpcy5fbWVkaWF0b3I7XG4gICAgfVxufSIsImltcG9ydCB7SU5vdGlmaWNhdGlvbn0gZnJvbSAnZGlqb24vaW50ZXJmYWNlcyc7XG5pbXBvcnQge0NvbnN0YW50c30gZnJvbSBcIi4uL3V0aWxzL1N0YXRpY3NcIjtcbmltcG9ydCBCYXNlTWVkaWF0b3IgZnJvbSAnLi9CYXNlTWVkaWF0b3InO1xuaW1wb3J0IE5vdGlmaWNhdGlvbnMgZnJvbSAnLi4vdXRpbHMvTm90aWZpY2F0aW9ucyc7XG5pbXBvcnQgeyBJTWFpbkRhdGEgfSBmcm9tICcuLi91dGlscy9JbnRlcmZhY2VzJztcbmltcG9ydCBHYW1lcGxheSBmcm9tICcuLi9zdGF0ZS9HYW1lcGxheSc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEdhbWVwbGF5TWVkaWF0b3IgZXh0ZW5kcyBCYXNlTWVkaWF0b3Ige1xuICAgIHB1YmxpYyBzdGF0aWMgTUVESUFUT1JfTkFNRTogc3RyaW5nID0gJ2dhbWVNZWRpYXRvcic7XG5cdFx0XG4gICAgcHVibGljIGxpc3ROb3RpZmljYXRpb25JbnRlcmVzdHMoKTogc3RyaW5nW10ge1xuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgLy8gQWRkIE5vdGlmaWNhdGlvbnMgdG8gbGlzdGVuIGZvci5cbiAgICAgICAgIF07XG4gICAgfVxuXG4gICAgcHVibGljIGhhbmRsZU5vdGlmaWNhdGlvbihub3RpZmljYXRpb246IElOb3RpZmljYXRpb24pIHtcbiAgICAgICAgY29uc3Qgbm90ZU5hbWUgPSBub3RpZmljYXRpb24uZ2V0TmFtZSgpO1xuICAgICAgICBjb25zdCBub3RlQm9keSA9IG5vdGlmaWNhdGlvbi5nZXRCb2R5KCk7XG4gICAgICAgIHN3aXRjaCAobm90ZU5hbWUpIHtcbiAgICAgICAgICAgIC8vIEhhbmRsZSBub3RpZmljYXRpb24gY2FzZXMuXG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBwdWJsaWMgbWV0aG9kc1xuXG4gICAgcHVibGljIGdldCBtYWluRGF0YSgpOiBJTWFpbkRhdGEge1xuICAgICAgICByZXR1cm4gdGhpcy5nYW1lTW9kZWwubWFpbkRhdGE7XG4gICAgfVxuXHRcdFxuICAgIC8qIEdFVC9TRVQgKi9cblxuICAgIHB1YmxpYyBnZXQgbmFtZSgpIHtcbiAgICAgICAgcmV0dXJuIEdhbWVwbGF5TWVkaWF0b3IuTUVESUFUT1JfTkFNRTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IGdhbWVwbGF5KCk6IEdhbWVwbGF5IHtcbiAgICAgICAgcmV0dXJuIDxHYW1lcGxheT50aGlzLl92aWV3Q29tcG9uZW50O1xuICAgIH1cbn0iLCJpbXBvcnQge1Nwcml0ZX0gZnJvbSAnZGlqb24vZGlzcGxheSc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEFjdG9yIGV4dGVuZHMgU3ByaXRlIHtcbiAgICBjb25zdHJ1Y3Rvcih4OiBudW1iZXIsIHk6IG51bWJlciwga2V5OiBzdHJpbmcsIGZyYW1lOiBzdHJpbmcsIGJvZHlLZXk6IHN0cmluZywga2luZW1hdGljOiBib29sZWFuID0gZmFsc2UpIHtcbiAgICAgICAgc3VwZXIoeCwgeSwga2V5LCBmcmFtZSk7XG5cbiAgICAgICAgdGhpcy5nYW1lLnBoeXNpY3MucDIuZW5hYmxlKHRoaXMpOyAgIFxuICAgICAgICB0aGlzLnAyQm9keS5jbGVhclNoYXBlcygpOyBcbiAgICAgICAgdGhpcy5wMkJvZHkubG9hZFBvbHlnb24oXCJwaHlzaWNzXCIsIGJvZHlLZXkpO1xuICAgICAgICB0aGlzLnAyQm9keS5raW5lbWF0aWMgPSBraW5lbWF0aWM7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCBwMkJvZHkoKTogUGhhc2VyLlBoeXNpY3MuUDIuQm9keSB7XG4gICAgICAgIHJldHVybiA8UGhhc2VyLlBoeXNpY3MuUDIuQm9keT50aGlzLmJvZHk7XG4gICAgfVxufSIsImltcG9ydCBBY3RvciBmcm9tICcuL0FjdG9yJztcbmltcG9ydCB7R3JvdXB9IGZyb20gJ2Rpam9uL2Rpc3BsYXknO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBWZWhpY2xlIGV4dGVuZHMgQWN0b3Ige1xuXG4gICAgcHJvdGVjdGVkIF9tYXhTcGVlZDogbnVtYmVyID0gMzAwO1xuICAgIHByb3RlY3RlZCBfYWNjZWw6IG51bWJlciA9IDU7XG5cbiAgICBwcm90ZWN0ZWQgX3doZWVsczogR3JvdXA7XG4gICAgcHJvdGVjdGVkIF93aGVlbE1hdGVyaWFsOiBQaGFzZXIuUGh5c2ljcy5QMi5NYXRlcmlhbDtcbiAgICBwcm90ZWN0ZWQgX2N1cnNvcnM6IFBoYXNlci5DdXJzb3JLZXlzO1xuICAgIHByb3RlY3RlZCBfY2FuQm91bmNlOiBib29sZWFuID0gdHJ1ZTtcbiAgICBwcm90ZWN0ZWQgX3JvdGF0ZVNwZWVkOiBudW1iZXIgPSAwO1xuXG4gICAgY29uc3RydWN0b3IoeDogbnVtYmVyLCB5OiBudW1iZXIsIGtleTogc3RyaW5nLCBmcmFtZTogc3RyaW5nLCBib2R5S2V5OiBzdHJpbmcpIHtcbiAgICAgICAgc3VwZXIoeCwgeSwga2V5LCBmcmFtZSwgYm9keUtleSwgZmFsc2UpO1xuXG4gICAgICAgIHRoaXMuX3doZWVscyA9IHRoaXMuZ2FtZS5hZGQuZEdyb3VwKCk7XG4gICAgICAgIHRoaXMuX3doZWVsTWF0ZXJpYWwgPSB0aGlzLmdhbWUucGh5c2ljcy5wMi5jcmVhdGVNYXRlcmlhbCgnd2hlZWxNYXRlcmlhbCcpO1xuXHQgICAgdGhpcy5fY3Vyc29ycyA9IHRoaXMuZ2FtZS5pbnB1dC5rZXlib2FyZC5jcmVhdGVDdXJzb3JLZXlzKCk7XG5cblx0ICAgIHRoaXMuX2luaXRXaGVlbCg1NSk7XG5cdCAgICB0aGlzLl9pbml0V2hlZWwoLTUyKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgdXBkYXRlKCk6IHZvaWQge1xuICAgICAgICAvKlxuICAgICAgICAqIHJvdGF0ZSB0cnVjayB3aGVlbHMgcmlnaHQgYW5kIGxlZnQgYmFzZWQgb24ga2V5Ym9hcmQgYXJyb3dzXG4gICAgICAgICogYnkgaXRlcmF0aW5nIHRocm91Z2ggdGhlIHdoZWVsIGdyb3VwXG4gICAgICAgICovXG4gICAgICAgIGlmICh0aGlzLl9jdXJzb3JzLmxlZnQuaXNEb3duKSB7XG4gICAgICAgICAgICB0aGlzLl9hY2NlbEJhY2t3YXJkKCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAodGhpcy5fY3Vyc29ycy5yaWdodC5pc0Rvd24pIHtcbiAgICAgICAgICAgIHRoaXMuX2FjY2VsRm9yd2FyZCgpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5fZGVjZWxlcmF0ZSgpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuX3VwZGF0ZVdoZWVscygpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBfZGVjZWxlcmF0ZSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5fcm90YXRlU3BlZWQgPSBNYXRoLmZsb29yKHRoaXMuX3JvdGF0ZVNwZWVkICogMC45NSk7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIF9hY2NlbEZvcndhcmQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuX3JvdGF0ZVNwZWVkIC09IHRoaXMuX2FjY2VsO1xuICAgICAgICBpZiAodGhpcy5fcm90YXRlU3BlZWQgPCAtdGhpcy5fbWF4U3BlZWQpIHtcbiAgICAgICAgICAgIHRoaXMuX3JvdGF0ZVNwZWVkID0gLXRoaXMuX21heFNwZWVkO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIF9hY2NlbEJhY2t3YXJkKCk6IHZvaWQge1xuICAgICAgICB0aGlzLl9yb3RhdGVTcGVlZCArPSB0aGlzLl9hY2NlbDtcbiAgICAgICAgaWYgKHRoaXMuX3JvdGF0ZVNwZWVkID4gdGhpcy5fbWF4U3BlZWQgKiAwLjUpIHtcbiAgICAgICAgICAgIHRoaXMuX3JvdGF0ZVNwZWVkID0gdGhpcy5fbWF4U3BlZWQgKiAwLjU7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgX3VwZGF0ZVdoZWVscygpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5fd2hlZWxzLmNoaWxkcmVuLmZvckVhY2goICh3aGVlbDogYW55KSA9PiB7XG4gICAgICAgICAgICB3aGVlbC5ib2R5LnJvdGF0ZUxlZnQodGhpcy5fcm90YXRlU3BlZWQpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgX2luaXRXaGVlbChvZmZzZXRYOiBudW1iZXIsIGRyaXZlV2hlZWw6IGJvb2xlYW4gPSB0cnVlKTogdm9pZCB7XG4gICAgICAgIGxldCBvZmZzZXRZID0gMjQ7XG4gICAgICAgIC8vcG9zaXRpb24gd2hlZWwgcmVsYXRpdmUgdG8gdGhlIHRydWNrXG4gICAgICAgIHZhciB3aGVlbCA9IHRoaXMuZ2FtZS5hZGQuc3ByaXRlKHRoaXMucG9zaXRpb24ueCArIG9mZnNldFgsIHRoaXMucG9zaXRpb24ueSArIG9mZnNldFksICdnYW1lcGxheScsICd3aGVlbCcpO1xuXG4gICAgICAgIHRoaXMuZ2FtZS5waHlzaWNzLnAyLmVuYWJsZSh3aGVlbCk7XG4gICAgICAgIHdoZWVsLmJvZHkuY2xlYXJTaGFwZXMoKTtcbiAgICAgICAgd2hlZWwuYm9keS5hZGRDaXJjbGUoMTUuNSk7XG4gICAgICAgIFxuICAgICAgICAvKlxuICAgICAgICAqIENvbnN0cmFpbiB0aGUgd2hlZWwgdG8gdGhlIHRydWNrIHNvIHRoYXQgaXQgY2FuIHJvdGF0ZSBmcmVlbHkgb24gaXRzIHBpdm90XG4gICAgICAgICogY3JlYXRlUmV2b2x1dGVDb25zdHJhaW50KGJvZHlBLCBwaXZvdEEsIGJvZHlCLCBwaXZvdEIsIG1heEZvcmNlKVxuICAgICAgICAqIGNoYW5nZSBtYXhGb3JjZSB0byBzZWUgaG93IGl0IGFmZmVjdHMgY2hhc3NpcyBib3VuY2luZXNzXG4gICAgICAgICovXG4gICAgICAgIHZhciBtYXhGb3JjZSA9IDUwO1xuICAgICAgICB2YXIgcmV2ID0gdGhpcy5nYW1lLnBoeXNpY3MucDIuY3JlYXRlUmV2b2x1dGVDb25zdHJhaW50KHRoaXMuYm9keSwgW29mZnNldFgsIG9mZnNldFldLCAgd2hlZWwuYm9keSwgWzAsMF0sIG1heEZvcmNlKTtcblxuICAgICAgICBpZiAoZHJpdmVXaGVlbCA9PT0gdHJ1ZSkge1xuICAgICAgICAgICAgdGhpcy5fd2hlZWxzLmFkZCh3aGVlbCk7XG4gICAgICAgIH1cblxuICAgICAgICAvL2NhbGwgb25XaGVlbENvbnRhY3Qgd2hlbiB0aGUgd2hlZWwgYmVnaW5zIGNvbnRhY3Qgd2l0aCBzb21ldGhpbmdcbiAgICAgICAgd2hlZWwuYm9keS5vbkJlZ2luQ29udGFjdC5hZGQodGhpcy5vbldoZWVsQ29udGFjdCwgdGhpcyk7XG5cbiAgICAgICAgLypcbiAgICAgICAgKiBzZXQgdGhlIG1hdGVyaWFsIHRvIGJlIHRoZSB3aGVlbCBtYXRlcmlhbCBzbyB0aGF0IGl0IGNhbiBoYXZlXG4gICAgICAgICogaGlnaCBmcmljdGlvbiB3aXRoIHRoZSBncm91bmRcbiAgICAgICAgKi9cbiAgICAgICAgd2hlZWwuYm9keS5zZXRNYXRlcmlhbCh0aGlzLl93aGVlbE1hdGVyaWFsKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25XaGVlbENvbnRhY3QocGhhc2VyQm9keTogYW55LCBwMkJvZHk6IFBoYXNlci5QaHlzaWNzLlAyLkJvZHkpOiB2b2lkIHtcbiAgICAgICAgLypcblx0KiBhbGxvdyBhbm90aGVyIHRydWNrIGJvdW5jZSBpZiB0aGUgd2hlZWwgaGFzIHRvdWNoZWQgdGhlIGJvdHRvbSBib3VuZGFyeVxuXHQqICh3aGljaCBoYXMgbm8gcGhhc2VyIGJvZHkgYW5kIGhhcyBpZCA0IGluIFAyKSBvciB0aGUgaGlsbFxuXHQqL1xuXHRpZiAoKHBoYXNlckJvZHkgPT09IG51bGwgJiYgcDJCb2R5LmlkID09IDQpXG4gICAgICAgIHx8IChwaGFzZXJCb2R5ICYmIHBoYXNlckJvZHkuc3ByaXRlLmtleSA9PSBcImhpbGxcIikpIHtcbiAgICAgICAgICAgIHRoaXMuX2NhbkJvdW5jZSA9IHRydWU7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IHdoZWVsTWF0ZXJpYWwoKTogUGhhc2VyLlBoeXNpY3MuUDIuTWF0ZXJpYWwge1xuICAgICAgICByZXR1cm4gdGhpcy5fd2hlZWxNYXRlcmlhbDtcbiAgICB9XG59IiwiaW1wb3J0IEFjdG9yIGZyb20gJy4vQWN0b3InO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBSb2NrIGV4dGVuZHMgQWN0b3Ige1xuICAgIFxufSIsImltcG9ydCBCYXNlU3RhdGUgZnJvbSBcIi4vQmFzZVN0YXRlXCI7XG5pbXBvcnQgR2FtZXBsYXlNZWRpYXRvciBmcm9tIFwiLi4vbWVkaWF0b3IvR2FtZXBsYXlNZWRpYXRvclwiO1xuaW1wb3J0IFZlaGljbGUgZnJvbSAnLi4vYWN0b3JzL1ZlaGljbGUnO1xuaW1wb3J0IFJvY2sgZnJvbSAnLi4vYWN0b3JzL1JvY2snO1xuaW1wb3J0IEFjdG9yIGZyb20gJy4uL2FjdG9ycy9BY3Rvcic7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEdhbWVwbGF5IGV4dGVuZHMgQmFzZVN0YXRlIHtcblxuICAgIHByb3RlY3RlZCBfd29ybGRNYXQ6IFBoYXNlci5QaHlzaWNzLlAyLk1hdGVyaWFsO1xuICAgIHByb3RlY3RlZCBfdHJ1Y2s6IFZlaGljbGU7XG5cbiAgICAvLyBQaGFzZXIuU3RhdGUgb3ZlcnJpZGVzXG4gICAgcHVibGljIGluaXQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuX21lZGlhdG9yID0gR2FtZXBsYXlNZWRpYXRvci5yZXRyaWV2ZU1lZGlhdG9yKEdhbWVwbGF5TWVkaWF0b3IuTUVESUFUT1JfTkFNRSwgdGhpcyk7XG4gICAgICAgIGlmICh0aGlzLl9tZWRpYXRvciA9PT0gbnVsbCkge1xuICAgICAgICAgICAgdGhpcy5fbWVkaWF0b3IgPSBuZXcgR2FtZXBsYXlNZWRpYXRvcih0aGlzKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuZ2FtZS53b3JsZC5zZXRCb3VuZHMoMCwgMCwgdGhpcy5nYW1lLndpZHRoICogMywgdGhpcy5nYW1lLmhlaWdodCk7XG4gICAgICAgIHRoaXMuZ2FtZS5waHlzaWNzLnN0YXJ0U3lzdGVtKFBoYXNlci5QaHlzaWNzLlAySlMpO1xuICAgICAgICB0aGlzLmdhbWUucGh5c2ljcy5wMi5ncmF2aXR5LnkgPSAzMDA7XG4gICAgICAgIHRoaXMuX3dvcmxkTWF0ID0gdGhpcy5nYW1lLnBoeXNpY3MucDIuY3JlYXRlTWF0ZXJpYWwoXCJ3b3JsZE1hdGVyaWFsXCIpO1xuICAgIH1cblxuICAgIC8vIGRpam9uLmNvcmUuU3RhdGUgb3ZlcnJpZGVzXG4gICAgcHVibGljIGxpc3RCdWlsZFNlcXVlbmNlKCkge1xuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgdGhpcy5fYWRkQkcsIFxuICAgICAgICAgICAgdGhpcy5fYWRkVGVycmFpbixcbiAgICAgICAgICAgIHRoaXMuX2FkZFZlaGljbGUsXG4gICAgICAgICAgICB0aGlzLl9zZXRDb2xsaXNpb25zXG4gICAgICAgIF1cbiAgICB9XG4gICAgXG4gICAgcHJvdGVjdGVkIF9hZGRCRygpOiB2b2lkIHtcblxuICAgIH1cblxuICAgIHByb3RlY3RlZCBfYWRkVGVycmFpbigpOiB2b2lkIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCA2OyBpKyspIHtcbiAgICAgICAgICAgIC8vaW5pdGlhbGl6ZSB0aGUgaGlsbCBhbmQgcGxhY2UgaXQgaW4gdGhlIGNlbnRlciBvZiB0aGUgYm90dG9tIGJvdW5kYXJ5XG4gICAgICAgICAgICBsZXQgaGlsbCA9IG5ldyBBY3RvcigxNTAgKyAoaSAqIDM1MCkgKyAoTWF0aC5yYW5kb20oKSAqIDUwKSwgdGhpcy5nYW1lLmhlaWdodCAtIDMwLCAnZ2FtZXBsYXknLCAnaGlsbCcsICdoaWxsJywgdHJ1ZSk7XG4gICAgICAgICAgICAgLypcbiAgICAgICAgICAgICogZ2l2ZSBpdCB0aGUgc2FtZSBtYXRlcmlhbCBhcyB3b3JsZCBib3VuZGFyaWVzIHNvIHRoYXQgaXQgaGFzXG4gICAgICAgICAgICAqIGhpZ2ggZnJpY3Rpb24gd2l0aCB0aGUgd2hlZWxzXG4gICAgICAgICAgICAqL1xuICAgICAgICAgICAgaGlsbC5ib2R5LnNldE1hdGVyaWFsKHRoaXMuX3dvcmxkTWF0KTtcbiAgICAgICAgICAgIHRoaXMuYWRkLmV4aXN0aW5nKGhpbGwpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIF9hZGRWZWhpY2xlKCk6IHZvaWQge1xuICAgICAgICB0aGlzLl90cnVjayA9IG5ldyBWZWhpY2xlKDEwMCwgdGhpcy5nYW1lLmhlaWdodCAtIDI1MCwgJ2dhbWVwbGF5JywgJ3RydWNrJywgJ3RydWNrJyk7XG4gICAgICAgIHRoaXMuYWRkLmV4aXN0aW5nKHRoaXMuX3RydWNrKTtcbiAgICAgICAgdGhpcy5nYW1lLmNhbWVyYS5mb2xsb3codGhpcy5fdHJ1Y2spO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBfc2V0Q29sbGlzaW9ucygpOiB2b2lkIHtcbiAgICAgICAgbGV0IGNvbnRhY3RNYXRlcmlhbDogUGhhc2VyLlBoeXNpY3MuUDIuQ29udGFjdE1hdGVyaWFsID0gdGhpcy5nYW1lLnBoeXNpY3MucDIuY3JlYXRlQ29udGFjdE1hdGVyaWFsKHRoaXMuX3RydWNrLndoZWVsTWF0ZXJpYWwsIHRoaXMuX3dvcmxkTWF0KTtcbiAgICBcdGNvbnRhY3RNYXRlcmlhbC5mcmljdGlvbiA9IDEwMDA7XG4gICAgICAgIC8vIEJvdW5jZVxuXHQgICAgY29udGFjdE1hdGVyaWFsLnJlc3RpdHV0aW9uID0gMC40O1xuXG4gICAgICAgIC8vIFBhc3MgdGhyb3VnaCBtYXRlcmlhbD9cbiAgICAgICAgLy8gY29udGFjdE1hdGVyaWFsLnN0aWZmbmVzcyA9IDEwO1xuICAgICAgICAvLyBDb252ZXlvciBCZWx0XG4gICAgICAgIC8vIGNvbnRhY3RNYXRlcmlhbC5zdXJmYWNlVmVsb2NpdHkgPSAxMDtcbiAgICB9XG5cbiAgICAvKiBHRVQvU0VUKi9cblxuICAgIHByb3RlY3RlZCBnZXQgbWVkaWF0b3IoKTogR2FtZXBsYXlNZWRpYXRvciB7XG4gICAgICAgIHJldHVybiA8R2FtZXBsYXlNZWRpYXRvcj50aGlzLl9tZWRpYXRvcjtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IHByZWxvYWRJRCgpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gJ2dhbWUnO1xuICAgIH1cbn0iLCJpbXBvcnQge0lOb3RpZmljYXRpb259IGZyb20gJ2Rpam9uL2ludGVyZmFjZXMnO1xuaW1wb3J0IHtDb25zdGFudHN9IGZyb20gXCIuLi91dGlscy9TdGF0aWNzXCI7XG5pbXBvcnQgQmFzZU1lZGlhdG9yIGZyb20gJy4vQmFzZU1lZGlhdG9yJztcbmltcG9ydCBOb3RpZmljYXRpb25zIGZyb20gJy4uL3V0aWxzL05vdGlmaWNhdGlvbnMnO1xuaW1wb3J0IHsgSU1haW5EYXRhIH0gZnJvbSAnLi4vdXRpbHMvSW50ZXJmYWNlcyc7XG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1lbnVNZWRpYXRvciBleHRlbmRzIEJhc2VNZWRpYXRvciB7XG4gICAgcHVibGljIHN0YXRpYyBNRURJQVRPUl9OQU1FOiBzdHJpbmcgPSAnbWVudU1lZGlhdG9yJztcblx0XHRcbiAgICBwdWJsaWMgbGlzdE5vdGlmaWNhdGlvbkludGVyZXN0cygpOiBzdHJpbmdbXSB7XG4gICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICAvLyBBZGQgTm90aWZpY2F0aW9ucyB0byBsaXN0ZW4gZm9yLlxuICAgICAgICAgXTtcbiAgICB9XG5cbiAgICBwdWJsaWMgaGFuZGxlTm90aWZpY2F0aW9uKG5vdGlmaWNhdGlvbjogSU5vdGlmaWNhdGlvbikge1xuICAgICAgICBjb25zdCBub3RlTmFtZSA9IG5vdGlmaWNhdGlvbi5nZXROYW1lKCk7XG4gICAgICAgIGNvbnN0IG5vdGVCb2R5ID0gbm90aWZpY2F0aW9uLmdldEJvZHkoKTtcbiAgICAgICAgc3dpdGNoIChub3RlTmFtZSkge1xuICAgICAgICAgICAgLy8gSGFuZGxlIG5vdGlmaWNhdGlvbiBjYXNlcy5cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIHB1YmxpYyBtZXRob2RzXG4gICAgcHVibGljIGFkZFBvcHVwcygpOiB2b2lke1xuICAgICAgICB0aGlzLnNlbmROb3RpZmljYXRpb24oTm90aWZpY2F0aW9ucy5BRERfQUxMX1BPUFVQUyk7XG4gICAgfVxuXHRcdFxuICAgIC8vIGdldHRlciAvIHNldHRlclxuICAgIHB1YmxpYyBnZXQgbmFtZSgpIHtcbiAgICAgICAgcmV0dXJuIE1lbnVNZWRpYXRvci5NRURJQVRPUl9OQU1FO1xuICAgIH1cbn0iLCJpbXBvcnQgQmFzZVN0YXRlIGZyb20gXCIuL0Jhc2VTdGF0ZVwiO1xuaW1wb3J0IE1lbnVNZWRpYXRvciBmcm9tICcuLi9tZWRpYXRvci9NZW51TWVkaWF0b3InO1xuaW1wb3J0IHsgQ29uc3RhbnRzIH0gZnJvbSAnLi4vdXRpbHMvU3RhdGljcyc7XG5pbXBvcnQge0xhYmVsbGVkQnV0dG9uLCBUZXh0fSBmcm9tICdkaWpvbi9kaXNwbGF5JztcblxuLy8gVGhpcyBjbGFzcyBjdXJyZW50bHkgdXNlcyBhIGxvdCBvZiBkcmF3IGNhbGxzLCBiZWNhdXNlIG9mIGFsbCB0aGUgdGV4dCBvYmplY3RzIGl0IGNyZWF0ZXMuXG4vLyBBbnkgb2YgdGhlc2UgdGhhdCBkbyBub3QgY2hhbmdlIGFmdGVyIHRoZWlyIGNyZWF0aW9uIGNhbiBiZSBjYWNoZWQgaW50byBhIHNpbmdsZSBkcmF3IGNhbGwuXG4vLyBEbyB5b3Uga25vdyBob3cgdG8gZG8gdGhhdD9cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE1lbnUgZXh0ZW5kcyBCYXNlU3RhdGUge1xuICAgIC8vIFBoYXNlci5TdGF0ZSBvdmVycmlkZXNcbiAgICBwdWJsaWMgaW5pdCgpIHtcbiAgICAgICAgdGhpcy5fbWVkaWF0b3IgPSBNZW51TWVkaWF0b3IucmV0cmlldmVNZWRpYXRvcihNZW51TWVkaWF0b3IuTUVESUFUT1JfTkFNRSwgdGhpcyk7XG4gICAgICAgIGlmICh0aGlzLl9tZWRpYXRvciA9PT0gbnVsbCkge1xuICAgICAgICAgICAgdGhpcy5fbWVkaWF0b3IgPSBuZXcgTWVudU1lZGlhdG9yKHRoaXMpO1xuICAgICAgICB9XG4gICAgfVxuXHRcdFxuICAgIC8vIGRpam9uLmNvcmUuU3RhdGUgb3ZlcnJpZGVzXG4gICAgcHVibGljIGxpc3RCdWlsZFNlcXVlbmNlKCkge1xuICAgICAgICByZXR1cm4gW1xuICAgICAgICAgICAgdGhpcy5fYWRkQkcsIFxuICAgICAgICAgICAgdGhpcy5fYWRkUG9wdXBzLFxuICAgICAgICAgICAgdGhpcy5fYWRkQnV0dG9uc1xuICAgICAgICBdXG4gICAgfVxuXG4gICAgcHVibGljIGFmdGVyQnVpbGQoKSB7XG4gICAgICAgIHN1cGVyLmFmdGVyQnVpbGQoKTtcbiAgICAgICAgdGhpcy5yZXN1bWUoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIF9hZGRCRygpOiB2b2lkIHtcbiAgICAgICAgbGV0IHRpdGxlID0gdGhpcy5hZGQuZFRleHQodGhpcy5nYW1lLmNlbnRlclgsIHRoaXMuZ2FtZS5jZW50ZXJZIC0gMjAwLCB0aGlzLmdldENvcHlCeUlEKCd0aXRsZScpLCBDb25zdGFudHMuRk9OVF9QUkVTU19TVEFSVCwgMzAsICcjZmZmZmZmJyk7XG4gICAgICAgIHRpdGxlLmNlbnRlclBpdm90KCk7XG4gICAgfSAgIFxuXG4gICAgcHJpdmF0ZSBfYWRkQnV0dG9ucygpOiB2b2lkIHtcbiAgICAgICAgbGV0IHN0YXJ0QnRuID0gbmV3IExhYmVsbGVkQnV0dG9uKHRoaXMuZ2FtZS5jZW50ZXJYLCB0aGlzLmdhbWUuY2VudGVyWSwgdGhpcy5zdGFydEdhbWUsIHRoaXMsICdtZW51JywgJ3JlZF9idG5fbm9ybWFsJywgJ3JlZF9idG5fZG93bicsICdyZWRfYnRuX292ZXInLCAncmVkX2J0bl9ub3JtYWwnKTtcbiAgICAgICAgc3RhcnRCdG4uYWRkTGFiZWwodGhpcy5nZXRDb3B5QnlJRCgnc3RhcnRfZ2FtZScpLCAxNCwgQ29uc3RhbnRzLkZPTlRfUFJFU1NfU1RBUlQpO1xuICAgICAgICBzdGFydEJ0bi5jZW50ZXJQaXZvdCgpO1xuICAgICAgICB0aGlzLmFkZC5leGlzdGluZyhzdGFydEJ0bik7XG4gICAgfVxuICAgIFxuICAgIHByaXZhdGUgX2FkZFBvcHVwcygpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5tZWRpYXRvci5hZGRQb3B1cHMoKTtcbiAgICB9XG5cbiAgICAvKiBFVkVOVFMgKi9cblxuICAgIHB1YmxpYyBzdGFydEdhbWUoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuZ2FtZS50cmFuc2l0aW9uLnRvKENvbnN0YW50cy5TVEFURV9HQU1FKTtcbiAgICB9XG5cbiAgICAvKiBHRVQvU0VUICovXG5cbiAgICBwdWJsaWMgZ2V0Q29weUJ5SUQoaWQ6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLm1lZGlhdG9yLmNvcHlNb2RlbC5nZXRDb3B5KCdtZW51JywgaWQpO1xuICAgIH1cblxuICAgIHB1YmxpYyBnZXQgbWVkaWF0b3IoKTogTWVudU1lZGlhdG9yIHtcbiAgICAgICAgcmV0dXJuIDxNZW51TWVkaWF0b3I+dGhpcy5fbWVkaWF0b3I7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCBwcmVsb2FkSUQoKTogc3RyaW5nIHtcbiAgICAgICAgcmV0dXJuICdtZW51JztcbiAgICB9XG59XG4gICIsImltcG9ydCB7QXBwbGljYXRpb259IGZyb20gJ2Rpam9uL2FwcGxpY2F0aW9uJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUGllUHJvZ3Jlc3NCYXIgZXh0ZW5kcyBQaGFzZXIuU3ByaXRlIHtcblxuICAgIHByb3RlY3RlZCBfYm1wOiBQaGFzZXIuQml0bWFwRGF0YTtcbiAgICBwcm90ZWN0ZWQgX3JhZGl1czogbnVtYmVyO1xuICAgIHByb3RlY3RlZCBfY29sb3I6IHN0cmluZztcbiAgICBcbiAgICAvLyBTdXBwbHkgYSB3ZWlnaHQgcGFyYW1ldGVyIGlmIHlvdSBkbyBub3Qgd2FudCBhIGZpbGxlZCBjaXJjbGUuIEV4cGVjdHMgMCB0byAxIGRlY2ltYWwgdmFsdWVcbiAgICAvLyBiZWNhdXNlIGl0IGlzIHVzZWQgYXMgYSBwZXJjZW50YWdlIG9mIHRoZSByYWRpdXMgYXMgdGhlIGNpcmNsZSB0aGlja25lc3MuXG4gICAgcHJvdGVjdGVkIF93ZWlnaHQ6IG51bWJlcjtcbiAgICBcbiAgICBjb25zdHJ1Y3Rvcih4OiBudW1iZXIsIHk6IG51bWJlciwgcmFkaXVzOiBudW1iZXIgPSAyLCB3ZWlnaHQ6IG51bWJlciA9IDAsIGNvbG9yOiBzdHJpbmcgPSAnI2ZmZmZmZicsIGFuZ2xlOiBudW1iZXIgPSAtOTApIHtcbiAgICAgICAgc3VwZXIoQXBwbGljYXRpb24uZ2V0SW5zdGFuY2UoKS5nYW1lLCB4LCB5KTtcbiAgICAgICAgdGhpcy5hbmNob3Iuc2V0KDAuNSk7XG4gICAgICAgIHRoaXMuX3dlaWdodCA9IFBoYXNlci5NYXRoLmNsYW1wKHdlaWdodCwgMCwgMSk7XG4gICAgICAgIHRoaXMuX3JhZGl1cyA9IHJhZGl1cztcbiAgICAgICAgdGhpcy5hbmdsZSA9IGFuZ2xlO1xuICAgICAgICB0aGlzLl9jb2xvciA9IGNvbG9yO1xuXG4gICAgICAgIGlmICh0aGlzLl93ZWlnaHQgPiAwKSB7XG4gICAgICAgICAgICB0aGlzLl9ibXAgPSB0aGlzLmdhbWUuYWRkLmJpdG1hcERhdGEoKHRoaXMuX3JhZGl1cyAqIDIpICsgKHRoaXMuX3dlaWdodCAqICh0aGlzLl9yYWRpdXMgKiAwLjYpKSwgKHRoaXMuX3JhZGl1cyAqIDIpICsgKHRoaXMuX3dlaWdodCAqICh0aGlzLl9yYWRpdXMgKiAwLjYpKSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLl9ibXAgPSB0aGlzLmdhbWUuYWRkLmJpdG1hcERhdGEocmFkaXVzICogMiwgcmFkaXVzICogMik7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5sb2FkVGV4dHVyZSh0aGlzLl9ibXApO1xuICAgICAgICB0aGlzLnVwZGF0ZVByb2dyZXNzKDApO1xuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgdXBkYXRlUHJvZ3Jlc3MocGVyY2VudDogbnVtYmVyKTogdm9pZCB7XG4gICAgICAgIHZhciBwcm9ncmVzczogbnVtYmVyID0gcGVyY2VudDtcbiAgICAgICAgdGhpcy5fYm1wLmNsZWFyKCk7XG4gICAgICAgIGlmICh0aGlzLl93ZWlnaHQgPiAwKSB7XG4gICAgICAgICAgICBwcm9ncmVzcyA9IFBoYXNlci5NYXRoLmNsYW1wKHBlcmNlbnQsIDAsIDAuOTk5OSk7XG4gICAgICAgICAgICB0aGlzLl9ibXAuY3R4LmZpbGxTdHlsZSA9IHRoaXMuX2NvbG9yO1xuICAgICAgICAgICAgdGhpcy5fYm1wLmN0eC5zdHJva2VTdHlsZSA9IHRoaXMuX2NvbG9yO1xuICAgICAgICAgICAgdGhpcy5fYm1wLmN0eC5saW5lV2lkdGggPSB0aGlzLl93ZWlnaHQgKiB0aGlzLl9yYWRpdXM7XG4gICAgICAgICAgICB0aGlzLl9ibXAuY3R4LmJlZ2luUGF0aCgpO1xuICAgICAgICAgICAgdGhpcy5fYm1wLmN0eC5hcmModGhpcy5fYm1wLndpZHRoICogMC41LCB0aGlzLl9ibXAuaGVpZ2h0ICogMC41LCB0aGlzLl9yYWRpdXMgLSAxNSwgMCwgKE1hdGguUEkgKiAyKSAqIHByb2dyZXNzLCBmYWxzZSk7XG4gICAgICAgICAgICB0aGlzLl9ibXAuY3R4LnN0cm9rZSgpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgcHJvZ3Jlc3MgPSAxIC0gUGhhc2VyLk1hdGguY2xhbXAocGVyY2VudCwgMC4wMDAxLCAxKTtcbiAgICAgICAgICAgIHRoaXMuX2JtcC5jdHguZmlsbFN0eWxlID0gdGhpcy5fY29sb3I7XG4gICAgICAgICAgICB0aGlzLl9ibXAuY3R4LmJlZ2luUGF0aCgpO1xuICAgICAgICAgICAgdGhpcy5fYm1wLmN0eC5hcmModGhpcy5fcmFkaXVzLCB0aGlzLl9yYWRpdXMsIHRoaXMuX3JhZGl1cywgMCwgKE1hdGguUEkgKiAyKSAqIHByb2dyZXNzLCB0cnVlKTtcbiAgICAgICAgICAgIHRoaXMuX2JtcC5jdHgubGluZVRvKHRoaXMuX3JhZGl1cywgdGhpcy5fcmFkaXVzKTtcbiAgICAgICAgICAgIHRoaXMuX2JtcC5jdHguY2xvc2VQYXRoKCk7XG4gICAgICAgICAgICB0aGlzLl9ibXAuY3R4LmZpbGwoKTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgdGhpcy5fYm1wLmRpcnR5ID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc2V0IHJhZGl1cyh2YWx1ZTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMuX3JhZGl1cyA9ICh2YWx1ZSA+IDAgPyB2YWx1ZSA6IDApO1xuICAgICAgICB0aGlzLl9ibXAucmVzaXplKHRoaXMuX3JhZGl1cyAqIDIsIHRoaXMuX3JhZGl1cyAqIDIpO1xuICAgIH1cblxuICAgIHB1YmxpYyBnZXQgcmFkaXVzKCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiB0aGlzLl9yYWRpdXM7XG4gICAgfVxufSIsImltcG9ydCB7R3JvdXAsIFRleHR9IGZyb20gJ2Rpam9uL2Rpc3BsYXknO1xuaW1wb3J0IHtJUHJlbG9hZEhhbmRsZXJ9IGZyb20gJ2Rpam9uL2ludGVyZmFjZXMnO1xuaW1wb3J0IFBpZVByb2dyZXNzQmFyIGZyb20gJy4vUGllUHJvZ3Jlc3NCYXInO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQcmVsb2FkZXIgZXh0ZW5kcyBHcm91cCBpbXBsZW1lbnRzIElQcmVsb2FkSGFuZGxlciB7XG4gICAgcHJpdmF0ZSBfd2lwZXI6IFBoYXNlci5JbWFnZTtcbiAgICBwcml2YXRlIF9wcm9ncmVzc0JhcjE6IFBpZVByb2dyZXNzQmFyO1xuXG4gICAgcHVibGljIHRyYW5zaXRpb25JbkNvbXBsZXRlOiBQaGFzZXIuU2lnbmFsID0gbmV3IFBoYXNlci5TaWduYWwoKTtcbiAgICBwdWJsaWMgdHJhbnNpdGlvbk91dENvbXBsZXRlOiBQaGFzZXIuU2lnbmFsID0gbmV3IFBoYXNlci5TaWduYWwoKTtcblxuICAgIHByaXZhdGUgX2luVHdlZW46IFBoYXNlci5Ud2VlbjtcbiAgICBwcml2YXRlIF9vdXRUd2VlbjogUGhhc2VyLlR3ZWVuO1xuXG4gICAgY29uc3RydWN0b3IoeDogbnVtYmVyLCB5OiBudW1iZXIsIG5hbWU6IHN0cmluZykge1xuICAgICAgICBzdXBlcih4LCB5LCBuYW1lLCB0cnVlKTtcbiAgICAgICAgdGhpcy5pbml0KCk7XG4gICAgICAgIHRoaXMuYnVpbGRJbnRlcmZhY2UoKTtcbiAgICB9XG5cbiAgICAvLyBHcm91cCBvdmVycmlkZXNcbiAgICBwcm90ZWN0ZWQgYnVpbGRJbnRlcmZhY2UoKSB7XG4gICAgICAgIGxldCBnZnggPSB0aGlzLmdhbWUuYWRkLmdyYXBoaWNzKCk7XG4gICAgICAgIGdmeC5iZWdpbkZpbGwoMHhhYWFhYWEsIDEpO1xuICAgICAgICBnZnguZHJhd1JlY3QoMCwgMCwgdGhpcy5nYW1lLndpZHRoLCB0aGlzLmdhbWUuaGVpZ2h0KTtcbiAgICAgICAgZ2Z4LmVuZEZpbGwoKTtcblxuICAgICAgICB0aGlzLl93aXBlciA9IHRoaXMuYWRkSW50ZXJuYWwuaW1hZ2UoMCwgMCwgZ2Z4LmdlbmVyYXRlVGV4dHVyZSgpKTtcblxuICAgICAgICB0aGlzLmdhbWUud29ybGQucmVtb3ZlKGdmeCwgdHJ1ZSk7XG5cbiAgICAgICAgdGhpcy5hbHBoYSA9IDA7XG4gICAgICAgIHRoaXMudmlzaWJsZSA9IGZhbHNlO1xuXG4gICAgICAgIHRoaXMuX3Byb2dyZXNzQmFyMSA9IG5ldyBQaWVQcm9ncmVzc0Jhcih0aGlzLmdhbWUud2lkdGggKiAwLjUsIHRoaXMuZ2FtZS5oZWlnaHQgKiAwLjc1LCA1MCk7XG4gICAgICAgIHRoaXMuYWRkSW50ZXJuYWwuZXhpc3RpbmcodGhpcy5fcHJvZ3Jlc3NCYXIxKTtcblxuICAgICAgICB0aGlzLl9pblR3ZWVuID0gdGhpcy5nYW1lLmFkZC50d2Vlbih0aGlzKS50byh7IGFscGhhOiAxIH0sIDMwMCwgUGhhc2VyLkVhc2luZy5RdWFkcmF0aWMuT3V0KTtcbiAgICAgICAgdGhpcy5fb3V0VHdlZW4gPSB0aGlzLmdhbWUuYWRkLnR3ZWVuKHRoaXMpLnRvKHsgYWxwaGE6IDAgfSwgMjAwLCBQaGFzZXIuRWFzaW5nLlF1YWRyYXRpYy5Jbik7XG5cbiAgICAgICAgdGhpcy5faW5Ud2Vlbi5vbkNvbXBsZXRlLmFkZCh0aGlzLl9pbiwgdGhpcyk7XG4gICAgICAgIHRoaXMuX291dFR3ZWVuLm9uQ29tcGxldGUuYWRkKHRoaXMuX291dCwgdGhpcyk7XG4gICAgfVxuXG4gICAgLy8gaVByZWxvYWRIYW5kbGVyIGltcGxlbWVudGF0aW9uc1xuICAgIHB1YmxpYyBsb2FkU3RhcnQoKSB7XG4gICAgICAgIHRoaXMuX3Byb2dyZXNzQmFyMS51cGRhdGVQcm9ncmVzcygwKTtcblxuICAgIH1cblxuICAgIHB1YmxpYyBsb2FkUHJvZ3Jlc3MocHJvZ3Jlc3M6IG51bWJlcikge1xuICAgICAgICB0aGlzLl9wcm9ncmVzc0JhcjEudXBkYXRlUHJvZ3Jlc3MocHJvZ3Jlc3MvMTAwKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgbG9hZENvbXBsZXRlKCkge1xuXG4gICAgfVxuXG4gICAgcHVibGljIHRyYW5zaXRpb25JbigpIHtcbiAgICAgICAgdGhpcy52aXNpYmxlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5faW5Ud2Vlbi5zdGFydCgpO1xuICAgIH1cblxuICAgIHB1YmxpYyB0cmFuc2l0aW9uT3V0KCkge1xuICAgICAgICB0aGlzLl9vdXRUd2Vlbi5zdGFydCgpO1xuICAgIH1cblxuICAgIC8vIHByaXZhdGUgbWV0aG9kc1xuICAgIHByb3RlY3RlZCBfaW4oKSB7XG4gICAgICAgIHRoaXMudHJhbnNpdGlvbkluQ29tcGxldGUuZGlzcGF0Y2goKTtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgX291dCgpIHtcbiAgICAgICAgdGhpcy52aXNpYmxlID0gZmFsc2U7XG4gICAgICAgIHRoaXMudHJhbnNpdGlvbk91dENvbXBsZXRlLmRpc3BhdGNoKCk7XG4gICAgfVxufSIsImltcG9ydCB7QXBwbGljYXRpb259IGZyb20gXCJkaWpvbi9hcHBsaWNhdGlvblwiO1xuaW1wb3J0IHtHYW1lfSBmcm9tIFwiZGlqb24vY29yZVwiO1xuaW1wb3J0IHtEZXZpY2V9IGZyb20gXCJkaWpvbi91dGlsc1wiO1xuaW1wb3J0IHtDb3B5TW9kZWx9IGZyb20gXCJkaWpvbi9tdmNcIjtcblxuaW1wb3J0IEFwcGxpY2F0aW9uTWVkaWF0b3IgZnJvbSBcIi4vbWVkaWF0b3IvQXBwbGljYXRpb25NZWRpYXRvclwiO1xuaW1wb3J0IHtDb25zdGFudHMsIEdsb2JhbHN9IGZyb20gXCIuL3V0aWxzL1N0YXRpY3NcIjtcbmltcG9ydCBOb3RpZmljYXRpb25zIGZyb20gXCIuL3V0aWxzL05vdGlmaWNhdGlvbnNcIjtcbmltcG9ydCBCb290IGZyb20gXCIuL3N0YXRlL0Jvb3RcIjtcbmltcG9ydCBHYW1lcGxheSBmcm9tIFwiLi9zdGF0ZS9HYW1lcGxheVwiO1xuaW1wb3J0IE1lbnUgZnJvbSBcIi4vc3RhdGUvTWVudVwiO1xuaW1wb3J0IEdhbWVNb2RlbCBmcm9tIFwiLi9tb2RlbC9HYW1lTW9kZWxcIjtcbmltcG9ydCBQcmVsb2FkZXIgZnJvbSAnLi91aS9QcmVsb2FkZXInO1xuXG5leHBvcnQgZW51bSBFSW5pdE9yaWVudGF0aW9uIHtcbiAgICBDT1JSRUNULFxuICAgIElOQ09SUkVDVCxcbiAgICBJUlJFTEVWQU5UXG59XG5cbmV4cG9ydCBlbnVtIEVEZXZpY2VPcmllbnRhdGlvbiB7XG4gICAgTEFORFNDQVBFLCBcbiAgICBQT1JUUkFJVCxcbiAgICBERVNLVE9QXG59XG5cbmV4cG9ydCBlbnVtIEVCb290U3RhdGUge1xuICAgIFBSRUxPQURJTkcsXG4gICAgUkVBRFlfRk9SX0JPT1QsXG4gICAgQk9PVF9DT01QTEVURVxufVxuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNREFwcGxpY2F0aW9uIGV4dGVuZHMgQXBwbGljYXRpb24ge1xuICAgIHB1YmxpYyBzdGF0aWMgUk9UQVRFX0lNQUdFOiBzdHJpbmcgPSBcIi9hc3NldHMvaW1nL3BsYXlsYW5kc2NhcGUucG5nXCI7XG4gICAgcHJpdmF0ZSBzdGF0aWMgREVTSVJFRF9PUklFTlRBVElPTjogRURldmljZU9yaWVudGF0aW9uID0gRURldmljZU9yaWVudGF0aW9uLkxBTkRTQ0FQRTtcblxuICAgIHB1YmxpYyBnYW1lSWQ6IHN0cmluZyA9IG51bGw7XG4gICAgcHVibGljIHByZWxvYWRlcjogUHJlbG9hZGVyO1xuXG4gICAgcHJpdmF0ZSBfbGFzdEhlaWdodDogbnVtYmVyID0gMDtcbiAgICBwcml2YXRlIF9pbml0T3JpZW50YXRpb246IEVJbml0T3JpZW50YXRpb247XG4gICAgcHJpdmF0ZSBfYm9vdFN0YXRlOiBFQm9vdFN0YXRlID0gRUJvb3RTdGF0ZS5QUkVMT0FESU5HO1xuICAgIHByaXZhdGUgX29yaWVudGF0aW9uOiBFRGV2aWNlT3JpZW50YXRpb247XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgc3VwZXIoKTtcbiAgICB9XG5cbiAgICAvLyBvdmVycmlkZXNcbiAgICBwdWJsaWMgY3JlYXRlR2FtZSgpIHtcbiAgICAgICAgdGhpcy5nYW1lID0gbmV3IEdhbWUoe1xuICAgICAgICAgICAgd2lkdGg6IHRoaXMuX2dldEdhbWVXaWR0aCgpLFxuICAgICAgICAgICAgaGVpZ2h0OiB0aGlzLl9nZXRHYW1lSGVpZ2h0KCksXG4gICAgICAgICAgICBwYXJlbnQ6ICdnYW1lLWNvbnRhaW5lcicsXG4gICAgICAgICAgICAvL3JlbmRlcmVyOiBQaGFzZXIuQ0FOVkFTLFxuICAgICAgICAgICAgcmVuZGVyZXI6IERldmljZS5jb2Nvb24gPyBQaGFzZXIuQ0FOVkFTIDogdGhpcy5fZ2V0UmVuZGVyZXJCeURldmljZSgpLFxuICAgICAgICAgICAgdHJhbnNwYXJlbnQ6IGZhbHNlLFxuICAgICAgICAgICAgLy8gdXNlIHRoaXMgaWYgeW91IHdhbnQgdG8gc3dpdGNoIGJldHdlZW4gQDJ4IGFuZCBAMXggZ3JhcGhpY3NcbiAgICAgICAgICAgIC8vcmVzb2x1dGlvbjogdGhpcy5fZ2V0UmVzb2x1dGlvbigpLFxuICAgICAgICAgICAgcmVzb2x1dGlvbjogMSxcbiAgICAgICAgICAgIHBsdWdpbnM6IERldmljZS5tb2JpbGUgPyBbXSA6IFsnRGVidWcnXVxuXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuX21lZGlhdG9yID0gbmV3IEFwcGxpY2F0aW9uTWVkaWF0b3IodGhpcyk7XG4gICAgICAgIHRoaXMuX2FkZFN0YXRlcygpO1xuICAgIH1cbiAgICAvLyBwdWJsaWMgbWV0aG9kc1xuICAgIHB1YmxpYyBzdGFydEdhbWUoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuZ2FtZS5zdGF0ZS5zdGFydChDb25zdGFudHMuU1RBVEVfQk9PVCk7XG4gICAgfVxuXG4gICAgLy8gY2FsbGVkIGZyb20gdGhlIGJvb3Qgc3RhdGUgYXMgd2UgY2FuJ3QgaW5pdGlhbGl6ZSBwbHVnaW5zIHVudGlsIHRoZSBnYW1lIGlzIGJvb3RlZFxuICAgIHB1YmxpYyByZWdpc3Rlck1vZGVscygpOiB2b2lkIHtcbiAgICAgICAgY29uc3QgZ2FtZU1vZGVsID0gbmV3IEdhbWVNb2RlbCgnZ2FtZV9kYXRhJyk7XG4gICAgICAgIGNvbnN0IGNvcHlNb2RlbCA9IG5ldyBDb3B5TW9kZWwoJ2NvcHknKTtcbiAgICB9XG5cbiAgICAvLyBwcml2YXRlIG1ldGhvZHNcbiAgICAvLyBhZGRzIHN0YXRlc1xuICAgIHByaXZhdGUgX2FkZFN0YXRlcygpIHtcbiAgICAgICAgdGhpcy5nYW1lLnN0YXRlLmFkZChDb25zdGFudHMuU1RBVEVfQk9PVCwgQm9vdCk7XG4gICAgICAgIHRoaXMuZ2FtZS5zdGF0ZS5hZGQoQ29uc3RhbnRzLlNUQVRFX0dBTUUsIEdhbWVwbGF5KTtcbiAgICAgICAgdGhpcy5nYW1lLnN0YXRlLmFkZChDb25zdGFudHMuU1RBVEVfTUVOVSwgTWVudSk7XG4gICAgfVxuXG4gICAgcHVibGljIGJvb3RDb21wbGV0ZSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5wcmVsb2FkZXIgPSBuZXcgUHJlbG9hZGVyKDAsIDAsICdwcmVsb2FkZXInKTtcbiAgICAgICAgdGhpcy5nYW1lLmFkZFRvVUkuZXhpc3RpbmcodGhpcy5wcmVsb2FkZXIpO1xuICAgICAgICB0aGlzLmdhbWUudHJhbnNpdGlvbi5hZGRBbGwodGhpcy5wcmVsb2FkZXIpO1xuICAgICAgICB0aGlzLmdhbWUudGltZS5ldmVudHMuYWRkKDEwMCwgdGhpcy5fbW92ZVRvTWVudSwgdGhpcyk7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIF9tb3ZlVG9NZW51KCk6IHZvaWQge1xuICAgICAgICB0aGlzLmdhbWUudHJhbnNpdGlvbi50byhDb25zdGFudHMuU1RBVEVfTUVOVSk7XG4gICAgfSAgICBcblxuICAgIC8qIElOSVRJQUwgT1JJRU5UQVRJT04gQU5EIFNDQUxFIEFESlVTVE1FTlQgRlVOQ1RJT05TICovXG5cbiAgICBwdWJsaWMgYWRqdXN0U2NhbGVTZXR0aW5ncygpOiB2b2lkIHtcbiAgICAgICAgaWYgKHRoaXMuZ2FtZS5kZXZpY2UuZGVza3RvcCkge1xuICAgICAgICAgICAgdGhpcy5nYW1lLnNjYWxlLnNjYWxlTW9kZSA9IFBoYXNlci5TY2FsZU1hbmFnZXIuU0hPV19BTEw7XG4gICAgICAgICAgICB0aGlzLmdhbWUuc2NhbGUuc2V0TWluTWF4KDMwMCwgMjAwLCAxMDUwLCA3MDApO1xuICAgICAgICAgICAgdGhpcy5nYW1lLnNjYWxlLnBhZ2VBbGlnbkhvcml6b250YWxseSA9IHRydWU7XG4gICAgICAgICAgICB0aGlzLl9pbml0T3JpZW50YXRpb24gPSBFSW5pdE9yaWVudGF0aW9uLkNPUlJFQ1Q7XG4gICAgICAgICAgICB0aGlzLl9vcmllbnRhdGlvbiA9IEVEZXZpY2VPcmllbnRhdGlvbi5ERVNLVE9QO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5nYW1lLnNjYWxlLnNjYWxlTW9kZSA9IFBoYXNlci5TY2FsZU1hbmFnZXIuU0hPV19BTEw7XG4gICAgICAgICAgICBpZiAodGhpcy5nYW1lLmRldmljZS5hbmRyb2lkKSB7XG4gICAgICAgICAgICAgICAgaWYgKHNjcmVlbi53aWR0aCA+IHNjcmVlbi5oZWlnaHQpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fb3JpZW50YXRpb24gPSBFRGV2aWNlT3JpZW50YXRpb24uTEFORFNDQVBFO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fb3JpZW50YXRpb24gPSBFRGV2aWNlT3JpZW50YXRpb24uUE9SVFJBSVQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMuZ2FtZS5zY2FsZS5zZXRSZXNpemVDYWxsYmFjayh0aGlzLl9jaGVja09yaWVudGF0aW9uQW5kcm9pZCwgdGhpcyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICh0aGlzLmdhbWUuZGV2aWNlLmlPUykge1xuICAgICAgICAgICAgICAgIGlmICh3aW5kb3cub3JpZW50YXRpb24gPT09IDAgfHwgd2luZG93Lm9yaWVudGF0aW9uID09PSAxODApIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fb3JpZW50YXRpb24gPSBFRGV2aWNlT3JpZW50YXRpb24uUE9SVFJBSVQ7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLl9vcmllbnRhdGlvbiA9IEVEZXZpY2VPcmllbnRhdGlvbi5MQU5EU0NBUEU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMuZ2FtZS5zY2FsZS5zZXRSZXNpemVDYWxsYmFjayh0aGlzLl9jaGVja09yaWVudGF0aW9uaU9TLCB0aGlzKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuZ2FtZS5zY2FsZS5wYWdlQWxpZ25WZXJ0aWNhbGx5ID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMuX2luaXRSb3RhdGlvbkltYWdlKCk7XG4gICAgICAgICAgICB0aGlzLl9kaXNhYmxlU2Nyb2xsRXZlbnRzKCk7XG4gICAgICAgICAgICB0aGlzLl9hc3NpZ25TY3JlZW5TaXplQnlEZXZpY2UoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIEFkZCB0aGUgJ3BsZWFzZSByb3RhdGUgZGV2aWNlJyBpbWFnZSB0byB0aGUgcGFnZSAob25seSBjYWxsZWQgZm9yIG1vYmlsZSBzZXR1cCkgICAgXG4gICAgcHJpdmF0ZSBfaW5pdFJvdGF0aW9uSW1hZ2UoKTogdm9pZCB7XG4gICAgICAgIGxldCBlbGVtZW50ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJ0dXJuXCIpO1xuICAgICAgICBsZXQgaW1hZ2VTdHIgPSBcInVybCgnXCIgKyAod2luZG93Lmhhc093blByb3BlcnR5KFwiYmFzZVVSTFwiKSA/IHdpbmRvd1tcImJhc2VVUkxcIl0gKyBNREFwcGxpY2F0aW9uLlJPVEFURV9JTUFHRSA6IFwiLlwiICsgTURBcHBsaWNhdGlvbi5ST1RBVEVfSU1BR0UpICsgXCInKSBuby1yZXBlYXQgY2VudGVyIGNlbnRlclwiO1xuICAgICAgICBlbGVtZW50LnN0eWxlLmJhY2tncm91bmQgPSBpbWFnZVN0cjtcbiAgICB9XG4gICAgICAgIFxuICAgIHByaXZhdGUgX2Rpc2FibGVTY3JvbGxFdmVudHMoKTogdm9pZCB7XG4gICAgICAgIGxldCBjb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnZ2FtZS1jb250YWluZXInKTtcbiAgICAgICAgY29udGFpbmVyLmFkZEV2ZW50TGlzdGVuZXIoJ3RvdWNobW92ZScsIGZ1bmN0aW9uKGUpIHtcbiAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgfSwgZmFsc2UpO1xuICAgIH1cblxuICAgIHByaXZhdGUgX2Fzc2lnblNjcmVlblNpemVCeURldmljZSgpOiB2b2lkIHtcbiAgICAgICAgaWYgKHRoaXMuX29yaWVudGF0aW9uID09PSBNREFwcGxpY2F0aW9uLkRFU0lSRURfT1JJRU5UQVRJT04pIHtcbiAgICAgICAgICAgIHRoaXMuX2luaXRPcmllbnRhdGlvbiA9IEVJbml0T3JpZW50YXRpb24uQ09SUkVDVDtcbiAgICAgICAgICAgIHRoaXMuZ2FtZS5zY2FsZS5zZXRNaW5NYXgod2luZG93LmlubmVyV2lkdGgsIHdpbmRvdy5pbm5lckhlaWdodCwgd2luZG93LmlubmVyV2lkdGgsIHdpbmRvdy5pbm5lckhlaWdodCk7XG4gICAgICAgICAgICBHbG9iYWxzLlNDQUxFX1JBVElPID0gdGhpcy5fb3JpZ2luYWxBc3BlY3RSYXRpbyAvICh3aW5kb3cuaW5uZXJXaWR0aCAvIHdpbmRvdy5pbm5lckhlaWdodCk7XG4gICAgICAgICAgICB0aGlzLl9oaWRlUm90YXRlSW1hZ2UoKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuX2luaXRPcmllbnRhdGlvbiA9IEVJbml0T3JpZW50YXRpb24uSU5DT1JSRUNUO1xuICAgICAgICAgICAgdGhpcy5fc2hvd1JvdGF0ZUltYWdlKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgYWRqdXN0UmVuZGVyZXJTZXR0aW5ncygpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5nYW1lLnN0YWdlLmRpc2FibGVWaXNpYmlsaXR5Q2hhbmdlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5nYW1lLmZvcmNlU2luZ2xlVXBkYXRlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5nYW1lLmNhbWVyYS5yb3VuZFB4ID0gZmFsc2U7XG4gICAgICAgIHRoaXMuZ2FtZS5yZW5kZXJlci5yZW5kZXJTZXNzaW9uLnJvdW5kUGl4ZWxzID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5nYW1lLmFudGlhbGlhcyA9IHRydWU7XG4gICAgICAgIHRoaXMuZ2FtZS5yZW5kZXJlci5jbGVhckJlZm9yZVJlbmRlciA9IHRoaXMuZ2FtZS5yZW5kZXJUeXBlID09PSBQaGFzZXIuQ0FOVkFTO1xuICAgIH1cblxuICAgIC8vIElmIG1vYmlsZSBhcHAgd2FzIHN0YXJ0ZWQgaW4gYSBsb2NrZWQgb3V0IG9yaWVudGF0aW9uLCB0aGlzIHdpbGwgYmUgY2FsbGVkIGFmdGVyIHRoZSBmaXJzdCAncm90YXRlJyB0byBwcm9wZXIgb3JpZW50YXRpb24gICAgXG4gICAgcHJpdmF0ZSBfaW5pdGlhbFJlc2l6ZSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5fcmVzaXplQXBwKCk7XG4gICAgICAgIGlmICh0aGlzLl9ib290U3RhdGUgPT09IEVCb290U3RhdGUuUkVBRFlfRk9SX0JPT1QpIHtcbiAgICAgICAgICAgIHRoaXMuYm9vdENvbXBsZXRlKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKiBPUklFTlRBVElPTiBIQU5ETElORyAqL1xuXG4gICAgcHJpdmF0ZSBfaGFuZGxlRW50ZXJQb3J0cmFpdCgpOiB2b2lkIHtcbiAgICAgICAgaWYgKHRoaXMuX29yaWVudGF0aW9uID09PSBFRGV2aWNlT3JpZW50YXRpb24uTEFORFNDQVBFKSB7XG4gICAgICAgICAgICBpZiAoTURBcHBsaWNhdGlvbi5ERVNJUkVEX09SSUVOVEFUSU9OID09PSBFRGV2aWNlT3JpZW50YXRpb24uUE9SVFJBSVQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9lbnRlckNvcnJlY3RPcmllbnRhdGlvbigpO1xuICAgICAgICAgICAgfSAgICBcbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuX2VudGVySW5jb3JyZWN0T3JpZW50YXRpb24oKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICB0aGlzLl9vcmllbnRhdGlvbiA9IEVEZXZpY2VPcmllbnRhdGlvbi5QT1JUUkFJVDtcbiAgICB9XG5cbiAgICBwcml2YXRlIF9oYW5kbGVFbnRlckxhbmRzY2FwZSgpOiB2b2lkIHtcbiAgICAgICAgaWYgKHRoaXMuX29yaWVudGF0aW9uID09PSBFRGV2aWNlT3JpZW50YXRpb24uUE9SVFJBSVQpIHtcbiAgICAgICAgICAgIGlmIChNREFwcGxpY2F0aW9uLkRFU0lSRURfT1JJRU5UQVRJT04gPT09IEVEZXZpY2VPcmllbnRhdGlvbi5MQU5EU0NBUEUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLl9lbnRlckNvcnJlY3RPcmllbnRhdGlvbigpO1xuICAgICAgICAgICAgfSAgICBcbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuX2VudGVySW5jb3JyZWN0T3JpZW50YXRpb24oKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBcbiAgICAgICAgdGhpcy5fb3JpZW50YXRpb24gPSBFRGV2aWNlT3JpZW50YXRpb24uTEFORFNDQVBFO1xuICAgIH1cblxuICAgIHByaXZhdGUgX2VudGVyQ29ycmVjdE9yaWVudGF0aW9uKCk6IHZvaWQge1xuICAgICAgICB0aGlzLm1lZGlhdG9yLnNlbmROb3RpZmljYXRpb24oTm90aWZpY2F0aW9ucy5SRVNVTUVfR0FNRSk7XG4gICAgICAgIGlmICghdGhpcy5nYW1lLmRldmljZS5kZXNrdG9wKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5faW5pdE9yaWVudGF0aW9uID09PSBFSW5pdE9yaWVudGF0aW9uLklOQ09SUkVDVCkge1xuICAgICAgICAgICAgICAgIHRoaXMuX2luaXRPcmllbnRhdGlvbiA9IEVJbml0T3JpZW50YXRpb24uQ09SUkVDVDtcbiAgICAgICAgICAgICAgICB0aGlzLmdhbWUudGltZS5ldmVudHMuYWRkKDEwMCwgdGhpcy5faW5pdGlhbFJlc2l6ZSwgdGhpcyk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuX2hpZGVSb3RhdGVJbWFnZSgpO1xuICAgICAgICAgICAgICAgIHRoaXMuX3Jlc2l6ZUFwcCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSAgIFxuICAgIFxuICAgIHByaXZhdGUgX2VudGVySW5jb3JyZWN0T3JpZW50YXRpb24oKTogdm9pZCB7XG4gICAgICAgIHRoaXMubWVkaWF0b3Iuc2VuZE5vdGlmaWNhdGlvbihOb3RpZmljYXRpb25zLlBBVVNFX0dBTUUpO1xuICAgICAgICBpZiAoIXRoaXMuZ2FtZS5kZXZpY2UuZGVza3RvcCkge1xuICAgICAgICAgICAgdGhpcy5fc2hvd1JvdGF0ZUltYWdlKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIF9zaG93Um90YXRlSW1hZ2UoKTogdm9pZCB7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidHVyblwiKS5zdHlsZS5kaXNwbGF5ID0gJ2Jsb2NrJztcbiAgICB9XG5cbiAgICBwcml2YXRlIF9oaWRlUm90YXRlSW1hZ2UoKTogdm9pZCB7XG4gICAgICAgIGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwidHVyblwiKS5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xuICAgIH1cblxuICAgIHByaXZhdGUgX3Jlc2l6ZUFwcCgpOiB2b2lkIHtcbiAgICAgICAgaWYgKHRoaXMuX2xhc3RIZWlnaHQgIT09IE1hdGguZmxvb3Iod2luZG93LmlubmVySGVpZ2h0KSkge1xuICAgICAgICAgICAgdGhpcy5fbGFzdEhlaWdodCA9IE1hdGguZmxvb3Iod2luZG93LmlubmVySGVpZ2h0KTtcbiAgICAgICAgLy8gICAgdGhpcy5nYW1lLnNjYWxlLnNldE1pbk1heCh3aW5kb3cuaW5uZXJXaWR0aCwgd2luZG93LmlubmVySGVpZ2h0LCB3aW5kb3cuaW5uZXJXaWR0aCwgd2luZG93LmlubmVySGVpZ2h0KTtcbiAgICAgICAgLy8gICAgdGhpcy5nYW1lLnNjYWxlLnJlZnJlc2goKTtcbiAgICAgICAgICAgIHRoaXMuX2hpZGVSb3RhdGVJbWFnZSgpO1xuICAgICAgICAvLyAgICBHbG9iYWxzLlNDQUxFX1JBVElPID0gdGhpcy5fb3JpZ2luYWxBc3BlY3RSYXRpbyAvICh3aW5kb3cuaW5uZXJIZWlnaHQgL3dpbmRvdy5pbm5lcldpZHRoKTtcbiAgICAgICAgICAgIHRoaXMubWVkaWF0b3Iubm90aWZ5QXBwUmVzaXplZCgpO1xuICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIC8qIE9SSUVOVEFUSU9OIENIRUNLUyAqL1xuICAgIC8qIFRoaXMgYmxvY2sgb2YgY29kZSBpcyBjdXN0b20gY29kZSBmb3IgaGFuZGxpbmcgZGV2aWNlIG9yaWVudGF0aW9uIGFuZCBsb2NraW5nXG4gICAgICogb3V0IHNwZWNpZmljIG9yaWVudGF0aW9ucyBhcyBkZXNpcmVkLiBUaGlzIGN1cnJlbnRseSBleGlzdHMgYmVjYXVzZSBpbiBwaGFzZXIgMi42LjJcbiAgICAgKiBpRnJhbWVzIGNhbiBjYXVzZSBwaGFzZXIgdG8gc3RvcCByZWNlaXZpbmcgdGhlIHBhZ2VzIHJlc2l6ZSBjYWxsYmFja3MuXG4gICAgICovXG5cbiAgICAvKipcbiAgICAgKiBDaGVja3MgZGV2aWNlIG9yaWVudGF0aW9uIGFnYWluc3QgYSBkZXNpcmVkIG9yaWVudGF0aW9uLiBBbHdheXMgdHJ1ZSBvbiBkZXNrdG9wLlxuICAgICAqIEBwYXJhbSBkZXNpcmVkOiBzdHJpbmcsIG9yaWVudGF0aW9uIHlvdSB3YW50IHRvIGJlIGluIC0gZXhwZWN0cyAncG9ydHJhaXQnIG9yICdsYW5kc2NhcGUnXG4gICAgICogQHJldHVybiBib29sZWFuOiByZXR1cm5zIHRydWUgaWYgZGV2aWNlIG9yaWVudGF0aW9uIG1hdGNoZXMgdGhlIGRlc2lyZWQgb3JpZW50YXRpb25cbiAgICAgKi9cbiAgICBwcm90ZWN0ZWQgX2luT3JpZW50YXRpb24oZGVzaXJlZDogc3RyaW5nKTogYm9vbGVhbiAge1xuICAgICAgICBpZiAodGhpcy5nYW1lLmRldmljZS5kZXNrdG9wKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIGlmICh0aGlzLmdhbWUuZGV2aWNlLmlPUykge1xuICAgICAgICAgICAgLy8gV2UgYXJlIGluIHBvcnRyYWl0LlxuICAgICAgICAgICAgaWYgKHdpbmRvdy5vcmllbnRhdGlvbiA9PT0gMCB8fCB3aW5kb3cub3JpZW50YXRpb24gPT09IDE4MCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBkZXNpcmVkID09PSAncG9ydHJhaXQnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLy8gV2UgYXJlIGluIGxhbmRzY2FwZS5cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHJldHVybiBkZXNpcmVkID09PSAnbGFuZHNjYXBlJztcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIC8vIFdlIGFyZSBpbiBwb3J0cmFpdC5cbiAgICAgICAgICAgIGlmIChzY3JlZW4ud2lkdGggPCBzY3JlZW4uaGVpZ2h0KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGRlc2lyZWQgPT09ICdwb3J0cmFpdCc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICAvLyBXZSBhcmUgaW4gbGFuZHNjYXBlLlxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGRlc2lyZWQgPT09ICdsYW5kc2NhcGUnO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuICAgIFxuICAgIHByaXZhdGUgX2NoZWNrT3JpZW50YXRpb25BbmRyb2lkKCk6IHZvaWQge1xuICAgICAgICAvLyBXZSBhcmUgaW4gcG9ydHJhaXQuXG4gICAgICAgIGlmIChzY3JlZW4ud2lkdGggPCBzY3JlZW4uaGVpZ2h0KSB7XG4gICAgICAgICAgICB0aGlzLl9oYW5kbGVFbnRlclBvcnRyYWl0KCk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gV2UgYXJlIGluIGxhbmRzY2FwZS5cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLl9oYW5kbGVFbnRlckxhbmRzY2FwZSgpO1xuICAgICAgICB9XG4gICAgfSAgIFxuXG4gICAgcHJpdmF0ZSBfY2hlY2tPcmllbnRhdGlvbmlPUygpOiB2b2lkIHtcbiAgICAgICAgLy8gV2UgYXJlIGluIHBvcnRyYWl0LlxuICAgICAgICBpZiAod2luZG93Lm9yaWVudGF0aW9uID09PSAwIHx8IHdpbmRvdy5vcmllbnRhdGlvbiA9PT0gMTgwKSB7XG4gICAgICAgICAgICB0aGlzLl9oYW5kbGVFbnRlclBvcnRyYWl0KCk7XG4gICAgICAgIH1cbiAgICAgICAgLy8gV2UgYXJlIGluIGxhbmRzY2FwZS5cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLl9oYW5kbGVFbnRlckxhbmRzY2FwZSgpO1xuICAgICAgICB9XG4gICAgfSBcblxuICAgIC8qIEdFVC9TRVRTICovXG4gICAgXG4gICAgcHJpdmF0ZSBnZXQgX29yaWdpbmFsQXNwZWN0UmF0aW8oKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuICh0aGlzLl9nZXRHYW1lV2lkdGgoKSAvIHRoaXMuX2dldEdhbWVIZWlnaHQoKSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfZ2V0R2FtZVdpZHRoKCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiBEZXZpY2UubW9iaWxlID8gMTAwMCA6IDEwNTA7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBfZ2V0R2FtZUhlaWdodCgpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gRGV2aWNlLm1vYmlsZSA/IDU4OCA6IDcwMDtcbiAgICB9XG5cbiAgICBwcml2YXRlIF9nZXRSZXNvbHV0aW9uKCk6IG51bWJlciB7XG4gICAgICAgIGlmIChBcHBsaWNhdGlvbi5xdWVyeVZhcigncmVzb2x1dGlvbicpICYmICFpc05hTihBcHBsaWNhdGlvbi5xdWVyeVZhcigncmVzb2x1dGlvbicpKSkge1xuICAgICAgICAgICAgcmV0dXJuIEFwcGxpY2F0aW9uLnF1ZXJ5VmFyKCdyZXNvbHV0aW9uJyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKERldmljZS5jb2Nvb24pIHtcbiAgICAgICAgICAgIHJldHVybiAod2luZG93LmRldmljZVBpeGVsUmF0aW8gPiAxID8gMiA6IDEpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgcmV0dXJuIERldmljZS5tb2JpbGUgPyAxIDogKHdpbmRvdy5kZXZpY2VQaXhlbFJhdGlvID4gMSA/IDIgOiAxKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgX2dldFJlbmRlcmVyQnlEZXZpY2UoKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIERldmljZS5tb2JpbGUgJiYgd2luZG93LmRldmljZVBpeGVsUmF0aW8gPCAyID8gUGhhc2VyLkNBTlZBUyA6IFBoYXNlci5BVVRPO1xuICAgIH1cbiAgICBcbiAgICAvLyBnZXR0ZXIgLyBzZXR0ZXJcbiAgICBwdWJsaWMgZ2V0IG1lZGlhdG9yKCk6IEFwcGxpY2F0aW9uTWVkaWF0b3Ige1xuICAgICAgICByZXR1cm4gPEFwcGxpY2F0aW9uTWVkaWF0b3I+dGhpcy5fbWVkaWF0b3I7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCBnYW1lTW9kZWwoKTogR2FtZU1vZGVsIHtcbiAgICAgICAgcmV0dXJuIDxHYW1lTW9kZWw+dGhpcy5yZXRyaWV2ZU1vZGVsKEdhbWVNb2RlbC5NT0RFTF9OQU1FKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IGNvcHlNb2RlbCgpOiBDb3B5TW9kZWwge1xuICAgICAgICByZXR1cm4gPENvcHlNb2RlbD50aGlzLnJldHJpZXZlTW9kZWwoQ29weU1vZGVsLk1PREVMX05BTUUpO1xuICAgIH1cbn0iLCIvLy8gPHJlZmVyZW5jZSBwYXRoPVwiLi4vLi4vc3VibW9kdWxlcy9kaWpvbi9idWlsZC9kaWpvbi5kLnRzXCIvPiAgXG5pbXBvcnQgTURBcHBsaWNhdGlvbiBmcm9tICcuL01EQXBwbGljYXRpb24nO1xuXG4vLyBib290c3RyYXAgdGhlIGFwcFxuZXhwb3J0IGNvbnN0IGFwcCA9IG5ldyBNREFwcGxpY2F0aW9uKCk7IiwiaW1wb3J0IHtBcHBsaWNhdGlvbn0gZnJvbSBcImRpam9uL2FwcGxpY2F0aW9uXCI7XG5pbXBvcnQge0dyb3VwLCBOaW5lU2xpY2VJbWFnZX0gZnJvbSBcImRpam9uL2Rpc3BsYXlcIjtcbmltcG9ydCBOb3RpZmljYXRpb25zIGZyb20gXCIuLi91dGlscy9Ob3RpZmljYXRpb25zXCI7XG5pbXBvcnQgQmFzZU1lZGlhdG9yIGZyb20gXCIuLi9tZWRpYXRvci9CYXNlTWVkaWF0b3JcIjtcbmltcG9ydCB7IEdsb2JhbHMgfSBmcm9tICcuLi91dGlscy9TdGF0aWNzJztcblxuZXhwb3J0IGludGVyZmFjZSBJUG9wdXAge1xuICAgIGlkOiBzdHJpbmc7XG4gICAgc2hvdygpOiBQaGFzZXIuVHdlZW47XG4gICAgaGlkZSgpOiBQaGFzZXIuVHdlZW47XG4gICAgdmlzaWJsZTogYm9vbGVhbjtcbn1cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgQmFzZVBvcHVwIGV4dGVuZHMgR3JvdXAgaW1wbGVtZW50cyBJUG9wdXAge1xuICAgIHByb3RlY3RlZCBzaG93VHdlZW46IFBoYXNlci5Ud2VlbjtcbiAgICBwcm90ZWN0ZWQgaGlkZVR3ZWVuOiBQaGFzZXIuVHdlZW47XG4gICAgcHJvdGVjdGVkIHN0YXJ0UG9zaXRpb246IFBoYXNlci5Qb2ludDtcbiAgICBwcm90ZWN0ZWQgYWxsb3dJbnB1dDogYm9vbGVhbjtcblxuICAgIC8vIEFueXRoaW5nIHRoYXQgd2lsbCBuZWVkIHRvIGJlIHNjYWxlZCwgYnV0IG90aGVyd2lzZSBoYXMgbm8gY2F1c2UgZm9yIGEgXG4gICAgLy8gc3RvcmVkIHJlZmVyZW5jZSwgc2hvdWxkIGJlIHBsYWNlZCBpbiBoZXJlLlxuICAgIHByb3RlY3RlZCBzY2FsbGFibGVzOiBhbnlbXTtcblxuICAgIHB1YmxpYyBiZzogUGhhc2VyLkltYWdlO1xuICAgIFxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBpZDogc3RyaW5nLCB4OiBudW1iZXI9IDAsIHk6IG51bWJlcj0gMCkge1xuICAgICAgICBzdXBlcih4LCB5LCBpZCk7XG5cbiAgICAgICAgdGhpcy52aXNpYmxlID0gZmFsc2U7XG4gICAgICAgIHRoaXMuYWxwaGEgPSAwO1xuICAgICAgICB0aGlzLnNjYWxsYWJsZXMgPSBbXTtcblxuICAgICAgICB0aGlzLmluaXQoKTtcbiAgICAgICAgdGhpcy5idWlsZEludGVyZmFjZSgpO1xuXG4gICAgICAgIHRoaXMuc3RhcnRQb3NpdGlvbiA9IHRoaXMucG9zaXRpb24uY2xvbmUoKTtcbiAgICAgICAgdGhpcy55IC09IDIwO1xuICAgICAgICBcbiAgICAgICAgdGhpcy5hZGRUd2VlbnMoKTtcbiAgICAgICAgdGhpcy5iZy5pbnB1dEVuYWJsZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLmJnLmlucHV0LnVzZUhhbmRDdXJzb3IgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgaW5pdCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5hZGRNZWRpYXRvcigpO1xuICAgIH1cblxuICAgIC8qIE9WRVJSSURFIFRIRVNFIEZVTkNUSU9OUyBJTiBFWFRFTkRFRCBDTEFTU0VTICovXG4gICAgXG4gICAgcHVibGljIGJ1aWxkSW50ZXJmYWNlKCk6IHZvaWQgeyB9XG4gICAgcHVibGljIGhhbmRsZUxhbmd1YWdlVG9nZ2xlKCk6IHZvaWQgeyB9XG4gICAgcHVibGljIGhhbmRsZVJlc2l6ZSgpOiB2b2lkIHtcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnNjYWxsYWJsZXMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICAgIHRoaXMuc2NhbGxhYmxlc1tpXS5zY2FsZS5zZXRUbyhHbG9iYWxzLlNDQUxFX1JBVElPLCAxKTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBwdWJsaWMgZ2V0Q29weUJ5SUQoaWQ6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIC8vIE92ZXJyaXRlIHRoaXMgdG8gZ2V0IGNvcHkgc3BlY2lmaWMgdG8geW91ciBwb3B1cHMgbmVlZHMuXG4gICAgICAgIHJldHVybiBpZDtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgYWRkTWVkaWF0b3IoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuX21lZGlhdG9yID0gQmFzZVBvcHVwLnJldHJpZXZlTWVkaWF0b3IoQmFzZU1lZGlhdG9yLk1FRElBVE9SX05BTUUsIHRoaXMpO1xuICAgICAgICBpZiAodGhpcy5fbWVkaWF0b3IgPT09IG51bGwpIHtcbiAgICAgICAgICAgIHRoaXMuX21lZGlhdG9yID0gbmV3IEJhc2VNZWRpYXRvcih0aGlzLCB0cnVlLCBCYXNlTWVkaWF0b3IuTUVESUFUT1JfTkFNRSk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5yZWdpc3RlcigpO1xuICAgIH1cblxuICAgIC8qIEVORCBWSVJUVUFMUyAqL1xuXG4gICAgcHVibGljIGRlc3Ryb3koKTogdm9pZCB7XG4gICAgICAgIHRoaXMuaGlkZVR3ZWVuLm9uQ29tcGxldGUucmVtb3ZlQWxsKCk7XG4gICAgICAgIHRoaXMuc2hvd1R3ZWVuID0gbnVsbDtcbiAgICAgICAgdGhpcy5oaWRlVHdlZW4gPSBudWxsO1xuICAgICAgICBzdXBlci5kZXN0cm95KCk7XG4gICAgfVxuXG4gICAgcHVibGljIHNob3coKTogUGhhc2VyLlR3ZWVuIHtcbiAgICAgICAgaWYgKHRoaXMuc2hvd1R3ZWVuLmlzUnVubmluZykge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY2xlYXJBY3RpdmVUd2VlbnMoKTtcbiAgICAgICAgdGhpcy52aXNpYmxlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5zaG93VHdlZW4uc3RhcnQoKTtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2hvd1R3ZWVuO1xuICAgIH1cblxuICAgIHB1YmxpYyBoaWRlKCk6IFBoYXNlci5Ud2VlbiB7XG4gICAgICAgIGlmICh0aGlzLmhpZGVUd2Vlbi5pc1J1bm5pbmcpIHtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmFsbG93SW5wdXQgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5jbGVhckFjdGl2ZVR3ZWVucygpO1xuICAgICAgICB0aGlzLmhpZGVUd2Vlbi5zdGFydCgpO1xuICAgICAgICByZXR1cm4gdGhpcy5oaWRlVHdlZW47XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGNsb3NlKCk6IHZvaWQge1xuICAgICAgICB0aGlzLl9tZWRpYXRvci5zZW5kTm90aWZpY2F0aW9uKE5vdGlmaWNhdGlvbnMuSElERV9QT1BVUCwgdGhpcy5pZCk7XG4gICAgfVxuICAgIFxuICAgIHByb3RlY3RlZCBhZGRUd2VlbnMoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuc2hvd1R3ZWVuID0gdGhpcy5nYW1lLmFkZC50d2Vlbih0aGlzKS50byh7IGFscGhhOiAxLCB5OiB0aGlzLnN0YXJ0UG9zaXRpb24ueSB9LCAzMDAsIFBoYXNlci5FYXNpbmcuQm91bmNlLk91dCk7XG4gICAgICAgIHRoaXMuc2hvd1R3ZWVuLm9uQ29tcGxldGUuYWRkKHRoaXMuc2hvd0NvbXBsZXRlLCB0aGlzKTtcbiAgICAgICAgdGhpcy5oaWRlVHdlZW4gPSB0aGlzLmdhbWUuYWRkLnR3ZWVuKHRoaXMpLnRvKHsgYWxwaGE6IDAsIHk6IHRoaXMuc3RhcnRQb3NpdGlvbi55ICsgODAgfSwgMzAwLCBQaGFzZXIuRWFzaW5nLlF1YWRyYXRpYy5PdXQpO1xuICAgICAgICB0aGlzLmhpZGVUd2Vlbi5vbkNvbXBsZXRlLmFkZCh0aGlzLmhpZGVDb21wbGV0ZSwgdGhpcyk7XG4gICAgfVxuICAgIFxuICAgIHByb3RlY3RlZCBzaG93Q29tcGxldGUoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuYWxsb3dJbnB1dCA9IHRydWU7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGhpZGVDb21wbGV0ZSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy52aXNpYmxlID0gZmFsc2U7XG4gICAgICAgIHRoaXMueSA9IHRoaXMuc3RhcnRQb3NpdGlvbi55IC0gMjA7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGZhZGVJbkVsZW1lbnQoZWxlbWVudDogYW55KTogdm9pZCB7XG4gICAgICAgIGVsZW1lbnQudmlzaWJsZSA9IHRydWU7XG4gICAgICAgIHRoaXMuZ2FtZS5hZGQudHdlZW4oZWxlbWVudCkudG8oeyBhbHBoYTogMSB9LCAzNTAsIFBoYXNlci5FYXNpbmcuQ3ViaWMuSW4sIHRydWUpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBmYWRlT3V0RWxlbWVudChlbGVtZW50OiBhbnkpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5nYW1lLmFkZC50d2VlbihlbGVtZW50KS50byh7IGFscGhhOiAwIH0sIDM1MCwgUGhhc2VyLkVhc2luZy5DdWJpYy5JbiwgdHJ1ZSkub25Db21wbGV0ZS5hZGRPbmNlKCgpID0+IHtcbiAgICAgICAgICAgIGVsZW1lbnQudmlzaWJsZSA9IGZhbHNlO1xuICAgICAgICB9KTtcbiAgICB9ICAgXG4gICAgXG4gICAgcHJvdGVjdGVkIHNlbmRSZXN1bWVOb3RpZmljYXRpb24oKTogdm9pZHtcbiAgICAgICAgdGhpcy5fbWVkaWF0b3Iuc2VuZE5vdGlmaWNhdGlvbihOb3RpZmljYXRpb25zLlJFU1VNRV9HQU1FKTtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgY2xlYXJBY3RpdmVUd2VlbnMoKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLnNob3dUd2Vlbi5pc1J1bm5pbmcpIHtcbiAgICAgICAgICAgIHRoaXMuc2hvd1R3ZWVuLnN0b3AoKTtcbiAgICAgICAgfVxuICAgICAgICBcbiAgICAgICAgaWYgKHRoaXMuaGlkZVR3ZWVuLmlzUnVubmluZykge1xuICAgICAgICAgICAgdGhpcy5oaWRlVHdlZW4uc3RvcCgpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIHN0YXRpYyByZXRyaWV2ZU1lZGlhdG9yKG5hbWU6IHN0cmluZywgdmlld0NvbXA6IGFueSk6IGFueSB7XG4gICAgICAgIHJldHVybiBCYXNlTWVkaWF0b3IucmV0cmlldmVNZWRpYXRvcihuYW1lLCB2aWV3Q29tcCk7XG4gICAgfSAgXG4gICAgXG4gICAgcHJvdGVjdGVkIHJlZ2lzdGVyKCk6IHZvaWQge1xuICAgICAgICB0aGlzLl9tZWRpYXRvci5zZW5kTm90aWZpY2F0aW9uKE5vdGlmaWNhdGlvbnMuUkVHSVNURVJfUE9QVVAsIHRoaXMpO1xuICAgIH1cbn0iLCJpbXBvcnQge0FwcGxpY2F0aW9ufSBmcm9tICdkaWpvbi9hcHBsaWNhdGlvbidcbmltcG9ydCB7IEdhbWUgfSBmcm9tICdkaWpvbi9jb3JlJztcbmltcG9ydCB7IFNwcml0ZSwgR3JvdXAsIFRleHQsIExhYmVsbGVkQnV0dG9uIH0gZnJvbSAnZGlqb24vZGlzcGxheSc7XG5pbXBvcnQgeyBDb25zdGFudHMgfSBmcm9tICcuLi91dGlscy9TdGF0aWNzJztcblxuZXhwb3J0IGNsYXNzIExDQnV0dG9uIGV4dGVuZHMgUGhhc2VyLkJ1dHRvbiB7XG4gICAgY29uc3RydWN0b3IoeDogbnVtYmVyLCB5OiBudW1iZXIsIGNhbGxiYWNrOiBhbnksIGNvbnRleHQ6IGFueSwgYXNzZXRLZXk6IHN0cmluZywgYmFzZUZyYW1lTmFtZTogc3RyaW5nLCBmb3JjZU91dDogYm9vbGVhbiA9IGZhbHNlKSB7XG4gICAgICAgIHN1cGVyKEFwcGxpY2F0aW9uLmdldEluc3RhbmNlKCkuZ2FtZSxcbiAgICAgICAgICAgIHgsXG4gICAgICAgICAgICB5LFxuICAgICAgICAgICAgYXNzZXRLZXksXG4gICAgICAgICAgICBjYWxsYmFjayxcbiAgICAgICAgICAgIGNvbnRleHQsXG4gICAgICAgICAgICBiYXNlRnJhbWVOYW1lICsgJ19vdmVyJyxcbiAgICAgICAgICAgIGJhc2VGcmFtZU5hbWUsXG4gICAgICAgICAgICBiYXNlRnJhbWVOYW1lICsgJ19kb3duJywgXG4gICAgICAgICAgICBiYXNlRnJhbWVOYW1lKTtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuZm9yY2VPdXQgPSBmb3JjZU91dDtcbiAgICAgICAgdGhpcy5pbnB1dC51c2VIYW5kQ3Vyc29yID0gdHJ1ZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgb25JbnB1dERvd25IYW5kbGVyKHNwcml0ZTogYW55LCBwb2ludGVyOiBhbnkpOiB2b2lkIHtcbiAgICAgICAgc3VwZXIub25JbnB1dERvd25IYW5kbGVyKHNwcml0ZSwgcG9pbnRlcik7XG4gICAgIC8vICAgdGhpcy5kZ2FtZS5hdWRpby5wbGF5QXVkaW8oJ3VpQ2xpY2snLCAwLjUpO1xuICAgIH1cblxuICAgIHB1YmxpYyBvbklucHV0T3ZlckhhbmRsZXIoc3ByaXRlOiBhbnksIHBvaW50ZXI6IGFueSk6IHZvaWQge1xuICAgICAgICBzdXBlci5vbklucHV0T3ZlckhhbmRsZXIoc3ByaXRlLCBwb2ludGVyKTtcbiAgICAvLyAgIHRoaXMuZGdhbWUuYXVkaW8ucGxheUF1ZGlvKCd1aUhvdmVyJywgMC41KTtcbiAgICB9XG5cbiAgICBwdWJsaWMgdXBkYXRlQmFzZUZyYW1lKGJhc2U6IHN0cmluZyk6IHZvaWQge1xuICAgICAgICB0aGlzLnNldEZyYW1lcyhiYXNlICsgJ19vdmVyJywgYmFzZSwgYmFzZSArICdfZG93bicsIGJhc2UpO1xuICAgIH0gIFxuICAgIFxuICAgIHB1YmxpYyBnZXQgZGdhbWUoKTogR2FtZSB7XG4gICAgICAgIHJldHVybiBBcHBsaWNhdGlvbi5nZXRJbnN0YW5jZSgpLmdhbWU7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgTENMYWJlbGxlZEJ1dHRvbiBleHRlbmRzIExhYmVsbGVkQnV0dG9uIHtcbiAgICBwdWJsaWMgb25JbnB1dERvd25IYW5kbGVyKHNwcml0ZTogYW55LCBwb2ludGVyOiBhbnkpOiB2b2lkIHtcbiAgICAgICAgc3VwZXIub25JbnB1dERvd25IYW5kbGVyKHNwcml0ZSwgcG9pbnRlcik7XG4gICAgIC8vICAgdGhpcy5kZ2FtZS5hdWRpby5wbGF5QXVkaW8oJ3VpQ2xpY2snLCAwLjUpO1xuICAgIH1cbiAgICBcbiAgICBwdWJsaWMgb25JbnB1dE92ZXJIYW5kbGVyKHNwcml0ZTogYW55LCBwb2ludGVyOiBhbnkpOiB2b2lkIHtcbiAgICAgICAgc3VwZXIub25JbnB1dE92ZXJIYW5kbGVyKHNwcml0ZSwgcG9pbnRlcik7XG4gICAgLy8gICAgdGhpcy5kZ2FtZS5hdWRpby5wbGF5QXVkaW8oJ3VpSG92ZXInLCAwLjUpO1xuICAgIH1cblxuICAgIHB1YmxpYyBnZXQgZGdhbWUoKTogR2FtZSB7XG4gICAgICAgIHJldHVybiBBcHBsaWNhdGlvbi5nZXRJbnN0YW5jZSgpLmdhbWU7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgU2xpZGVyIGV4dGVuZHMgR3JvdXAge1xuXG4gICAgcHJvdGVjdGVkIGNvbnRyb2w6IFNwcml0ZTtcbiAgICBwcm90ZWN0ZWQgYmFyOiBQaGFzZXIuVGlsZVNwcml0ZTtcbiAgICBwcm90ZWN0ZWQgc2Nyb2xsVHlwZTogc3RyaW5nO1xuICAgIFxuICAgIHB1YmxpYyBsYWJlbDogVGV4dDtcbiAgICBwdWJsaWMgaWNvbjogUGhhc2VyLkltYWdlO1xuICAgIHB1YmxpYyBpY29uRnJhbWVzOiB7IGFjdGl2ZTogc3RyaW5nLCBpbmFjdGl2ZTogc3RyaW5nIH07XG4gICAgcHVibGljIG9uRHJhZ0VuZGVkOiBQaGFzZXIuU2lnbmFsO1xuXG4gICAgY29uc3RydWN0b3IoeDogbnVtYmVyLCB5OiBudW1iZXIsIGtleTogc3RyaW5nLCBiZ0ZyYW1lOiBzdHJpbmcsIGNvbnRyb2xGcmFtZTogc3RyaW5nLCBpY29uRnJhbWVzOiB7IGFjdGl2ZTogc3RyaW5nLCBpbmFjdGl2ZTogc3RyaW5nIH0gPSBudWxsKSB7XG4gICAgICAgIHN1cGVyKHgsIHkpO1xuICAgICAgICB0aGlzLmljb25GcmFtZXMgPSBpY29uRnJhbWVzO1xuICAgICAgICB0aGlzLm9uRHJhZ0VuZGVkID0gbmV3IFBoYXNlci5TaWduYWwoKTtcbiAgICAgICAgdGhpcy5iYXIgPSB0aGlzLmFkZEludGVybmFsLnRpbGVTcHJpdGUoMCwgMCwgMTAsIDEwLCBrZXksIGJnRnJhbWUpO1xuICAgICAgICBcbiAgICAgICAgdGhpcy5jb250cm9sID0gdGhpcy5hZGRJbnRlcm5hbC5kU3ByaXRlKDAsIDAsIGtleSwgY29udHJvbEZyYW1lKTtcbiAgICAgICAgdGhpcy5jb250cm9sLmFuY2hvci5zZXRUbygwLjUsIDAuNSk7XG4gICAgICAgIHRoaXMuY29udHJvbC5pbnB1dEVuYWJsZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLmNvbnRyb2wuaW5wdXQuZHJhZ2dhYmxlID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5jb250cm9sLmV2ZW50cy5vbklucHV0VXAuYWRkKHRoaXMub25JbnB1dFVwLCB0aGlzKTtcblxuICAgICAgICBpZiAoaWNvbkZyYW1lcyAhPT0gbnVsbCkge1xuICAgICAgICAgICAgdGhpcy5pY29uID0gdGhpcy5hZGRJbnRlcm5hbC5pbWFnZSgwLCAwLCBrZXksIHRoaXMuaWNvbkZyYW1lcy5hY3RpdmUpO1xuICAgICAgICB9ICAgICAgICBcbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmljb24gPSBudWxsO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubGFiZWwgPSB0aGlzLmFkZEludGVybmFsLmRUZXh0KDAsIDAsICcxMDAlJywgQ29uc3RhbnRzLkZPTlRfUFJFU1NfU1RBUlQsIDE2LCBDb25zdGFudHMuQ09MT1JfU0xJREVSX0xBQkVMLCAnY2VudGVyJyk7XG4gICAgfVxuXG4gICAgcHVibGljIGluaXRpYWxpemVIb3Jpem9udGFsU2xpZGVyKGJhcldpZHRoOiBudW1iZXIsIGJhckhlaWdodDogbnVtYmVyLCBzaG93SWNvbjogYm9vbGVhbiA9IHRydWUsIHNob3dMYWJlbDogYm9vbGVhbiA9IHRydWUpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zY3JvbGxUeXBlID0gJ2hvcnonO1xuICAgICAgICB0aGlzLmJhci53aWR0aCA9IGJhcldpZHRoO1xuICAgICAgICB0aGlzLmJhci5oZWlnaHQgPSBiYXJIZWlnaHQ7XG4gICAgICAgIHRoaXMuYmFyLmFuY2hvci5zZXQoMCwgMC41KTtcblxuICAgICAgICB0aGlzLmNvbnRyb2wueCA9IHRoaXMuYmFyLnJpZ2h0O1xuICAgICAgICB0aGlzLmNvbnRyb2wueSA9IHRoaXMuYmFyLnkgLSAxMjtcbiAgICAgICAgdGhpcy5jb250cm9sLmlucHV0LmFsbG93VmVydGljYWxEcmFnID0gZmFsc2U7XG4gICAgICAgIFxuICAgICAgICBpZiAodGhpcy5pY29uICE9PSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLmljb24ueCA9IHRoaXMuYmFyLmxlZnQgLSA4O1xuICAgICAgICAgICAgdGhpcy5pY29uLnkgPSB0aGlzLmJhci55O1xuICAgICAgICAgICAgdGhpcy5pY29uLnZpc2libGUgPSBzaG93SWNvbjtcbiAgICAgICAgICAgIHRoaXMuaWNvbi5hbmNob3Iuc2V0VG8oMSwgMC41KTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmxhYmVsLnggPSB0aGlzLmJhci5yaWdodCArIDE2O1xuICAgICAgICB0aGlzLmxhYmVsLnkgPSB0aGlzLmJhci55ICsgMjtcbiAgICAgICAgdGhpcy5sYWJlbC52aXNpYmxlID0gc2hvd0xhYmVsO1xuICAgICAgICB0aGlzLmxhYmVsLmFuY2hvci5zZXRUbygwLCAwLjUpO1xuXG4gICAgICAgIHRoaXMudXBkYXRlRnVuYyA9IHRoaXMuaG9yaXpvbnRhbFVwZGF0ZTtcbiAgICB9XG5cbiAgICBwdWJsaWMgaW5pdGlhbGl6ZVZlcnRpY2FsU2xpZGVyKGJhcldpZHRoOiBudW1iZXIsIGJhckhlaWdodDogbnVtYmVyLCBzaG93SWNvbjogYm9vbGVhbiA9IHRydWUsIHNob3dMYWJlbDogYm9vbGVhbiA9IHRydWUpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zY3JvbGxUeXBlID0gJ3ZlcnQnO1xuICAgICAgICB0aGlzLmJhci53aWR0aCA9IGJhcldpZHRoO1xuICAgICAgICB0aGlzLmJhci5oZWlnaHQgPSBiYXJIZWlnaHQ7XG4gICAgICAgIHRoaXMuYmFyLmFuY2hvci5zZXQoMC41LCAwKTtcbiAgICAgICAgXG4gICAgICAgIHRoaXMuY29udHJvbC54ID0gdGhpcy5iYXIueCAtIDEyO1xuICAgICAgICB0aGlzLmNvbnRyb2wueSA9IHRoaXMuYmFyLnRvcDtcbiAgICAgICAgdGhpcy5jb250cm9sLmlucHV0LmFsbG93SG9yaXpvbnRhbERyYWcgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5jb250cm9sLmFuZ2xlID0gLTkwO1xuXG4gICAgICAgIGlmICh0aGlzLmljb24gIT09IG51bGwpIHtcbiAgICAgICAgICAgIHRoaXMuaWNvbi54ID0gdGhpcy5iYXIueDtcbiAgICAgICAgICAgIHRoaXMuaWNvbi55ID0gdGhpcy5iYXIudG9wIC0gODtcbiAgICAgICAgICAgIHRoaXMuaWNvbi52aXNpYmxlID0gc2hvd0ljb247XG4gICAgICAgICAgICB0aGlzLmljb24uYW5jaG9yLnNldFRvKDAuNSwgMSk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5sYWJlbC54ID0gdGhpcy5iYXIueDtcbiAgICAgICAgdGhpcy5sYWJlbC55ID0gdGhpcy5iYXIuYm90dG9tICsgMTY7XG4gICAgICAgIHRoaXMubGFiZWwudmlzaWJsZSA9IHNob3dMYWJlbDtcbiAgICAgICAgdGhpcy5sYWJlbC5hbmNob3Iuc2V0VG8oMC41LCAwKTtcblxuICAgICAgICB0aGlzLnVwZGF0ZUZ1bmMgPSB0aGlzLnZlcnRpY2FsVXBkYXRlO1xuICAgIH1cblxuICAgIHB1YmxpYyB1cGRhdGUoKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLmNvbnRyb2wuaW5wdXQuaXNEcmFnZ2VkID09PSB0cnVlKSB7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUZ1bmMoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByb3RlY3RlZCB1cGRhdGVGdW5jKCk6IHZvaWQge31cbiAgICBcbiAgICBwdWJsaWMgb25JbnB1dFVwKCk6IHZvaWQge1xuICAgICAgICB0aGlzLnVwZGF0ZUZ1bmMoKTtcbiAgICAgICAgdGhpcy5vbkRyYWdFbmRlZC5kaXNwYXRjaCh0aGlzKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgc2V0Q29udHJvbFBvc2l0aW9uKHBlcmNlbnQ6IG51bWJlcik6IHZvaWQge1xuICAgICAgICBpZiAodGhpcy5zY3JvbGxUeXBlID09PSAnaG9yeicpIHtcbiAgICAgICAgICAgIHRoaXMuY29udHJvbC54ID0gdGhpcy5iYXIueCArICh0aGlzLmJhcldpZHRoICogcGVyY2VudCk7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUVsZW1lbnRzKCk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAodGhpcy5zY3JvbGxUeXBlID09PSAndmVydCcpIHtcbiAgICAgICAgICAgIHRoaXMuY29udHJvbC55ID0gdGhpcy5iYXIueSArICh0aGlzLmJhckhlaWdodCAqIHBlcmNlbnQpO1xuICAgICAgICAgICAgdGhpcy51cGRhdGVFbGVtZW50cygpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIGhvcml6b250YWxVcGRhdGUoKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLmNvbnRyb2wueCA8PSB0aGlzLmJhci5sZWZ0KSB7XG4gICAgICAgICAgICB0aGlzLmNvbnRyb2wueCA9IHRoaXMuYmFyLmxlZnQ7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAodGhpcy5jb250cm9sLnggPj0gdGhpcy5iYXIucmlnaHQpIHtcbiAgICAgICAgICAgIHRoaXMuY29udHJvbC54ID0gdGhpcy5iYXIucmlnaHQ7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy51cGRhdGVFbGVtZW50cygpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCB2ZXJ0aWNhbFVwZGF0ZSgpOiB2b2lkIHtcbiAgICAgICAgaWYgKHRoaXMuY29udHJvbC55IDw9IHRoaXMuYmFyLnRvcCkge1xuICAgICAgICAgICAgdGhpcy5jb250cm9sLnkgPSB0aGlzLmJhci50b3A7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSBpZiAodGhpcy5jb250cm9sLnkgPj0gdGhpcy5iYXIuYm90dG9tKSB7XG4gICAgICAgICAgICB0aGlzLmNvbnRyb2wueSA9IHRoaXMuYmFyLmJvdHRvbTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnVwZGF0ZUVsZW1lbnRzKCk7XG4gICAgfVxuXG4gICAgcHJvdGVjdGVkIHVwZGF0ZUVsZW1lbnRzKCk6IHZvaWQge1xuICAgICAgICBpZiAodGhpcy5pY29uICE9PSBudWxsKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5zbGlkZXJQZXJjZW50IDw9IDApIHtcbiAgICAgICAgICAgICAgICB0aGlzLmljb24uZnJhbWVOYW1lID0gdGhpcy5pY29uRnJhbWVzLmluYWN0aXZlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5pY29uLmZyYW1lTmFtZSA9IHRoaXMuaWNvbkZyYW1lcy5hY3RpdmU7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMubGFiZWwudmlzaWJsZSkge1xuICAgICAgICAgICAgdGhpcy5sYWJlbC50ZXh0ID0gTWF0aC5yb3VuZCh0aGlzLnNsaWRlclBlcmNlbnQgKiAxMDApICsgJyUnO1xuICAgICAgICB9XG4gICAgfSAgICBcbiAgICBcbiAgICBwdWJsaWMgZ2V0IHNsaWRlclBlcmNlbnQoKTogbnVtYmVyIHtcbiAgICAgICAgaWYgKHRoaXMuc2Nyb2xsVHlwZSA9PT0gJ2hvcnonKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zbGlkZXJQZXJjZW50SG9yejtcbiAgICAgICAgfSAgICBcbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5zbGlkZXJQZXJjZW50VmVydDtcbiAgICAgICAgfVxuICAgIH1cbiBcbiAgICBwcm90ZWN0ZWQgZ2V0IHNsaWRlclBlcmNlbnRIb3J6KCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiBNYXRoLm1pbihNYXRoLm1heCgodGhpcy5jb250cm9sLnggLSB0aGlzLmJhci54KSAvIHRoaXMuYmFyV2lkdGgsIDApLCAxKTtcbiAgICB9XG5cbiAgICBwcm90ZWN0ZWQgZ2V0IHNsaWRlclBlcmNlbnRWZXJ0KCk6IG51bWJlciB7XG4gICAgICAgIHJldHVybiBNYXRoLm1pbihNYXRoLm1heCgodGhpcy5jb250cm9sLnkgLSB0aGlzLmJhci55KSAvIHRoaXMuYmFySGVpZ2h0LCAwKSwgMSk7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCBiYXJXaWR0aCgpOiBudW1iZXIge1xuICAgICAgICByZXR1cm4gdGhpcy5iYXIud2lkdGg7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCBiYXJIZWlnaHQoKTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuYmFyLmhlaWdodDtcbiAgICB9XG59IiwiaW1wb3J0IEJhc2VQb3B1cCBmcm9tICcuL0Jhc2VQb3B1cCc7XG5pbXBvcnQgeyBDb25zdGFudHMsIEdsb2JhbHMgfSBmcm9tICcuLi91dGlscy9TdGF0aWNzJztcbmltcG9ydCB7U2xpZGVyLCBMQ0xhYmVsbGVkQnV0dG9ufSBmcm9tICcuLi91aS9Db250cm9scyc7XG5pbXBvcnQgT3B0aW9uc1BvcHVwTWVkaWF0b3IgZnJvbSAnLi4vbWVkaWF0b3IvT3B0aW9uc1BvcHVwTWVkaWF0b3InO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBPcHRpb25zUG9wdXAgZXh0ZW5kcyBCYXNlUG9wdXAge1xuXG4gICAgcHJpdmF0ZSBzZnhTbGlkZXI6IFNsaWRlcjtcbiAgICBwcml2YXRlIG11c2ljU2xpZGVyOiBTbGlkZXI7XG4gICAgcHJpdmF0ZSBxdWl0QnRuOiBMQ0xhYmVsbGVkQnV0dG9uO1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHN1cGVyKENvbnN0YW50cy5QT1BVUF9PUFRJT05TLCAwLCAwKTtcbiAgICB9XG5cbiAgICAvKiBCVUlMRCBTRVFVRU5DRSAqL1xuICAgIFxuICAgIHB1YmxpYyBidWlsZEludGVyZmFjZSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5iZyA9IHRoaXMuYWRkSW50ZXJuYWwuaW1hZ2UodGhpcy5nYW1lLmNlbnRlclgsIHRoaXMuZ2FtZS5jZW50ZXJZLCAndWknLCAncG9wdXBfYmFja2dyb3VuZCcpO1xuICAgICAgICB0aGlzLmJnLmFuY2hvci5zZXRUbygwLjUpO1xuICAgICAgIFxuICAgICAgICB0aGlzLnNmeFNsaWRlciA9IG5ldyBTbGlkZXIodGhpcy5iZy54LCB0aGlzLmJnLnkgLSAodGhpcy5iZy5yZWFsSGVpZ2h0ICogMC4zKSwgJ3VpJywgJ2dyZXlfc2xpZGVyX2hvcnonLCAneWVsbG93X3NsaWRlcl9kb3duJywgeyBhY3RpdmU6ICdzZnhfaWNvbicsIGluYWN0aXZlOiAnc2Z4X2ljb25fbXV0ZSd9KTtcbiAgICAgICAgdGhpcy5zZnhTbGlkZXIuaW5pdGlhbGl6ZUhvcml6b250YWxTbGlkZXIoMjIwLCAxMCk7XG4gICAgICAgIHRoaXMuc2Z4U2xpZGVyLnggLT0gdGhpcy5zZnhTbGlkZXIuYmFyV2lkdGggKiAwLjU7XG4gICAgICAgIHRoaXMuYWRkSW50ZXJuYWwuZXhpc3RpbmcodGhpcy5zZnhTbGlkZXIpO1xuICAgICAgICB0aGlzLnNmeFNsaWRlci5vbkRyYWdFbmRlZC5hZGQodGhpcy5jaGFuZ2VTRlhWb2x1bWUsIHRoaXMpO1xuICAgICAgICB0aGlzLnNjYWxsYWJsZXMucHVzaCh0aGlzLnNmeFNsaWRlcik7XG5cbiAgICAgICAgdGhpcy5tdXNpY1NsaWRlciA9IG5ldyBTbGlkZXIodGhpcy5iZy54LCB0aGlzLmJnLnkgLSAodGhpcy5iZy5yZWFsSGVpZ2h0ICogMC4xKSwgJ3VpJywgJ2dyZXlfc2xpZGVyX2hvcnonLCAneWVsbG93X3NsaWRlcl9kb3duJywgeyBhY3RpdmU6ICdtdXNpY19pY29uJywgaW5hY3RpdmU6ICdtdXNpY19pY29uX211dGUnfSk7XG4gICAgICAgIHRoaXMubXVzaWNTbGlkZXIuaW5pdGlhbGl6ZUhvcml6b250YWxTbGlkZXIoMjIwLCAxMCk7XG4gICAgICAgIHRoaXMubXVzaWNTbGlkZXIueCAtPSB0aGlzLm11c2ljU2xpZGVyLmJhcldpZHRoICogMC41O1xuICAgICAgICB0aGlzLmFkZEludGVybmFsLmV4aXN0aW5nKHRoaXMubXVzaWNTbGlkZXIpO1xuICAgICAgICB0aGlzLm11c2ljU2xpZGVyLm9uRHJhZ0VuZGVkLmFkZCh0aGlzLmNoYW5nZU11c2ljVm9sdW1lLCB0aGlzKTtcbiAgICAgICAgdGhpcy5zY2FsbGFibGVzLnB1c2godGhpcy5tdXNpY1NsaWRlcik7XG5cbiAgICAgICAgdGhpcy5zZnhTbGlkZXIuc2V0Q29udHJvbFBvc2l0aW9uKHRoaXMuZ2FtZS5hdWRpby5zcHJpdGVWb2x1bWUpO1xuICAgICAgICB0aGlzLm11c2ljU2xpZGVyLnNldENvbnRyb2xQb3NpdGlvbih0aGlzLmdhbWUuYXVkaW8uc291bmRWb2x1bWUpO1xuICAgICAgICBcbiAgICAgICAgdGhpcy5xdWl0QnRuID0gdGhpcy5hZGRCdXR0b24odGhpcy5iZy54ICsgKHRoaXMuYmcucmVhbFdpZHRoICogMC4yMjUpLCB0aGlzLmJnLnkgKyAodGhpcy5iZy5yZWFsSGVpZ2h0ICogMC4zKSwgdGhpcy5xdWl0UHJlc3NlZCwgJ3F1aXRfZ2FtZScpO1xuICAgICAgICB0aGlzLmFkZEludGVybmFsLmV4aXN0aW5nKHRoaXMucXVpdEJ0bik7XG4gICAgICAgIHRoaXMuc2NhbGxhYmxlcy5wdXNoKHRoaXMucXVpdEJ0bik7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhZGRCdXR0b24oeDogbnVtYmVyLCB5OiBudW1iZXIsIGNhbGxiYWNrOiBhbnksIGNvcHlJRDogc3RyaW5nKTogTENMYWJlbGxlZEJ1dHRvbiB7XG4gICAgICAgIGxldCBidXR0b24gPSBuZXcgTENMYWJlbGxlZEJ1dHRvbih4LCB5LCBjYWxsYmFjaywgdGhpcywgJ21lbnUnLCAncmVkX2J0bl9ub3JtYWwnLCAncmVkX2J0bl9kb3duJywgJ3JlZF9idG5fb3ZlcicsICdyZWRfYnRuX25vcm1hbCcpO1xuICAgICAgICBidXR0b24uYWRkTGFiZWwodGhpcy5nZXRDb3B5QnlJRChjb3B5SUQpLCAxNCwgQ29uc3RhbnRzLkZPTlRfUFJFU1NfU1RBUlQpO1xuICAgICAgICBidXR0b24uY2VudGVyUGl2b3QoKTtcbiAgICAgICAgcmV0dXJuIGJ1dHRvbjtcbiAgICB9ICAgXG4gICAgXG4gICAgcHJpdmF0ZSBsYXlvdXRGb3JHYW1lcGxheSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zZnhTbGlkZXIueSA9IHRoaXMueSArIHRoaXMuYmcueSAtICh0aGlzLmJnLnJlYWxIZWlnaHQgKiAwLjI1KTtcbiAgICAgICAgdGhpcy5tdXNpY1NsaWRlci55ID0gdGhpcy5iZy55IC0gKHRoaXMuYmcucmVhbEhlaWdodCAqIDAuMSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBsYXlvdXRGb3JNZW51KCk6IHZvaWQge1xuICAgICAgICB0aGlzLnNmeFNsaWRlci55ID0gdGhpcy55ICsgdGhpcy5iZy55IC0gKHRoaXMuYmcucmVhbEhlaWdodCAqIDAuMik7XG4gICAgICAgIHRoaXMubXVzaWNTbGlkZXIueSA9IHRoaXMuZ2FtZS5jZW50ZXJZO1xuICAgIH1cblxuICAgIC8qIElOUFVUICYgRVZFTlRTICovXG4gICAgXG4gICAgcHVibGljIGNoYW5nZU11c2ljVm9sdW1lKCk6IHZvaWQge1xuICAgICAgICB0aGlzLmdhbWUuYXVkaW8uc291bmRWb2x1bWUgPSB0aGlzLm11c2ljU2xpZGVyLnNsaWRlclBlcmNlbnQ7XG4gICAgICAgIHRoaXMuZ2FtZS5hdWRpby5wbGF5QXVkaW8oJ21lc3NhZ2VfYWx0JywgdGhpcy5nYW1lLmF1ZGlvLnNvdW5kVm9sdW1lKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgY2hhbmdlU0ZYVm9sdW1lKCk6IHZvaWQge1xuICAgICAgICB0aGlzLmdhbWUuYXVkaW8uc3ByaXRlVm9sdW1lID0gdGhpcy5zZnhTbGlkZXIuc2xpZGVyUGVyY2VudDtcbiAgICAgICAgdGhpcy5nYW1lLmF1ZGlvLnBsYXlBdWRpbygnbWVzc2FnZV9hbHQnKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgcXVpdFByZXNzZWQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMubWVkaWF0b3IucXVpdFRvTWVudSgpO1xuICAgIH1cblxuICAgIHB1YmxpYyBob3dUb1ByZXNzZWQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMubWVkaWF0b3IucmVxdWVzdFBvcHVwKENvbnN0YW50cy5QT1BVUF9IRUxQKTtcbiAgICB9XG5cbiAgIC8qIE9WRVJSSURFUyAqL1xuICAgIFxuICAgIHB1YmxpYyBzaG93KCk6IFBoYXNlci5Ud2VlbiB7XG4gICAgICAgIGlmICh0aGlzLmluR2FtZVN0YXRlKSB7IFxuICAgICAgICAgICAgdGhpcy5sYXlvdXRGb3JHYW1lcGxheSgpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5sYXlvdXRGb3JNZW51KCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5xdWl0QnRuLnZpc2libGUgPSB0aGlzLmluR2FtZVN0YXRlO1xuICAgICAgICByZXR1cm4gc3VwZXIuc2hvdygpO1xuICAgIH1cbiAgICBcbiAgICBwcm90ZWN0ZWQgaGlkZUNvbXBsZXRlKCk6IHZvaWQge1xuICAgICAgICBzdXBlci5oaWRlQ29tcGxldGUoKTtcbiAgICAgICAgaWYgKHRoaXMuZ2FtZS5zdGF0ZS5jdXJyZW50ICE9PSBDb25zdGFudHMuU1RBVEVfTUVOVSkge1xuICAgICAgICAgICAgdGhpcy5tZWRpYXRvci5ub3RpZnlSZXN1bWVHYW1lKCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5tZWRpYXRvci51cGRhdGVQbGF5ZXJTZXR0aW5ncygpO1xuICAgIH1cblxuICAgIHByb3RlY3RlZCBhZGRNZWRpYXRvcigpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5fbWVkaWF0b3IgPSBCYXNlUG9wdXAucmV0cmlldmVNZWRpYXRvcihPcHRpb25zUG9wdXBNZWRpYXRvci5NRURJQVRPUl9OQU1FLCB0aGlzKTtcbiAgICAgICAgaWYgKHRoaXMuX21lZGlhdG9yID09PSBudWxsKSB7XG4gICAgICAgICAgICB0aGlzLl9tZWRpYXRvciA9IG5ldyBPcHRpb25zUG9wdXBNZWRpYXRvcih0aGlzLCB0cnVlLCBPcHRpb25zUG9wdXBNZWRpYXRvci5NRURJQVRPUl9OQU1FKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnJlZ2lzdGVyKCk7XG4gICAgfVxuXG4gICAgcHVibGljIGdldENvcHlCeUlEKGlkOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gdGhpcy5tZWRpYXRvci5jb3B5TW9kZWwuZ2V0Q29weSgnb3B0aW9ucycsIGlkKTtcbiAgICB9XG4gICAgXG4gICAgLyogR0VUL1NFVFMgKi9cbiAgICBcbiAgICBwdWJsaWMgZ2V0IG1lZGlhdG9yKCk6IE9wdGlvbnNQb3B1cE1lZGlhdG9yIHtcbiAgICAgICAgcmV0dXJuIDxPcHRpb25zUG9wdXBNZWRpYXRvcj50aGlzLl9tZWRpYXRvcjtcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0IGluR2FtZVN0YXRlKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gKHRoaXMuZ2FtZS5zdGF0ZS5jdXJyZW50ICE9PSBDb25zdGFudHMuU1RBVEVfTUVOVSk7XG4gICAgfVxuICAgIFxufSIsImltcG9ydCB7SU5vdGlmaWNhdGlvbn0gZnJvbSAnZGlqb24vaW50ZXJmYWNlcyc7XG5pbXBvcnQge0NvbnN0YW50cywgR2xvYmFsc30gZnJvbSBcIi4uL3V0aWxzL1N0YXRpY3NcIjtcbmltcG9ydCBCYXNlTWVkaWF0b3IgZnJvbSAnLi9CYXNlTWVkaWF0b3InO1xuaW1wb3J0IE5vdGlmaWNhdGlvbnMgZnJvbSAnLi4vdXRpbHMvTm90aWZpY2F0aW9ucyc7XG5pbXBvcnQgT3B0aW9uc1BvcHVwIGZyb20gJy4uL3BvcHVwcy9PcHRpb25zUG9wdXAnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBPcHRpb25zUG9wdXBNZWRpYXRvciBleHRlbmRzIEJhc2VNZWRpYXRvciB7XG4gICAgcHVibGljIHN0YXRpYyBNRURJQVRPUl9OQU1FOiBzdHJpbmcgPSAnb3B0aW9uc1BvcHVwTWVkaWF0b3InO1xuXG4gICAgcHVibGljIGhhbmRsZU5vdGlmaWNhdGlvbihub3RpZmljYXRpb246IElOb3RpZmljYXRpb24pIHtcbiAgICAgICAgY29uc3Qgbm90ZU5hbWUgPSBub3RpZmljYXRpb24uZ2V0TmFtZSgpO1xuICAgICAgICBjb25zdCBub3RlQm9keSA9IG5vdGlmaWNhdGlvbi5nZXRCb2R5KCk7XG4gICAgICAgIHN3aXRjaCAobm90ZU5hbWUpIHtcbiAgICAgICAgICAgIGNhc2UgTm90aWZpY2F0aW9ucy5BUFBfUkVTSVpFRDpcbiAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMuaGFuZGxlUmVzaXplKCk7XG4gICAgICAgICAgICAgICAgYnJlYWs7ICAgIFxuICAgICAgICB9XG4gICAgfVxuXG4gICAgLyogRVZFTlRTICovXG5cbiAgICBwdWJsaWMgdXBkYXRlUGxheWVyU2V0dGluZ3MoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuZ2FtZU1vZGVsLnNhdmVQbGF5ZXJTZXR0aW5ncygpO1xuICAgIH0gIFxuICAgIFxuICAgIHB1YmxpYyBxdWl0VG9NZW51KCk6IHZvaWQge1xuICAgICAgICB0aGlzLmNsb3NlQWxsUG9wdXBzKCk7XG4gICAgICAgIHRoaXMuZ2FtZS50cmFuc2l0aW9uLnRvKENvbnN0YW50cy5TVEFURV9NRU5VKTtcbiAgICB9ICAgIFxuXG4gICAgLyogR0VUL1NFVFMgKi9cblxuICAgIHB1YmxpYyBnZXQgbmFtZSgpIHtcbiAgICAgICAgcmV0dXJuIE9wdGlvbnNQb3B1cE1lZGlhdG9yLk1FRElBVE9SX05BTUU7XG4gICAgfVxuXG4gICAgcHVibGljIGdldCBvcHRpb25zKCk6IE9wdGlvbnNQb3B1cCB7XG4gICAgICAgIHJldHVybiA8T3B0aW9uc1BvcHVwPnRoaXMuX3ZpZXdDb21wb25lbnQ7XG4gICAgfVxufSIsImltcG9ydCB7R3JvdXB9IGZyb20gJ2Rpam9uL2Rpc3BsYXknO1xuaW1wb3J0IFBvcHVwTWFuYWdlck1lZGlhdG9yIGZyb20gJy4uL21lZGlhdG9yL1BvcHVwTWFuYWdlck1lZGlhdG9yJztcbmltcG9ydCB7SVBvcHVwfSBmcm9tIFwiLi9CYXNlUG9wdXBcIjtcbmltcG9ydCBCYXNlUG9wdXAgZnJvbSBcIi4vQmFzZVBvcHVwXCI7XG5pbXBvcnQgeyBUZXh0dXJlcywgUGxhY2Vob2xkZXJzIH0gZnJvbSAnZGlqb24vdXRpbHMnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQb3B1cE1hbmFnZXIgZXh0ZW5kcyBHcm91cCB7XG4gICAgcHVibGljIG1vZGFsOiBQaGFzZXIuU3ByaXRlO1xuICAgIFxuICAgIHByb3RlY3RlZCBmcm9tT3B0aW9uczogYm9vbGVhbiA9IGZhbHNlO1xuICAgIFxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICBzdXBlcigwLCAwLCAnUG9wdXBNYW5hZ2VyJywgZmFsc2UpO1xuXG4gICAgICAgIHRoaXMuaW5pdCgpO1xuICAgICAgICB0aGlzLmJ1aWxkSW50ZXJmYWNlKCk7XG4gICAgfVxuXG4gICAgcHVibGljIGluaXQoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuY2xhc3NUeXBlID0gQmFzZVBvcHVwO1xuICAgICAgICB0aGlzLmZpeGVkVG9DYW1lcmEgPSB0cnVlO1xuICAgICAgICB0aGlzLl9tZWRpYXRvciA9IEJhc2VQb3B1cC5yZXRyaWV2ZU1lZGlhdG9yKFBvcHVwTWFuYWdlck1lZGlhdG9yLk1FRElBVE9SX05BTUUsIHRoaXMpO1xuICAgICAgICBpZiAodGhpcy5fbWVkaWF0b3IgPT09IG51bGwpIHtcbiAgICAgICAgICAgIHRoaXMuX21lZGlhdG9yID0gbmV3IFBvcHVwTWFuYWdlck1lZGlhdG9yKHRoaXMpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIGJ1aWxkSW50ZXJmYWNlKCk6IHZvaWQge1xuICAgICAgICB0aGlzLmFkZE1vZGFsKCk7XG4gICAgfVxuXG4gICAgcHVibGljIGFkZE1vZGFsKCk6IHZvaWQge1xuICAgICAgICBpZiAodGhpcy5tb2RhbCkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5tb2RhbCA9IHRoaXMuYWRkKHRoaXMuZ2FtZS5hZGQuc3ByaXRlKDAsIDAsIFRleHR1cmVzLnJlY3QodGhpcy5nYW1lLndpZHRoLCB0aGlzLmdhbWUuaGVpZ2h0LCAweDAwMDAwMCwgMC4yNSkpKTtcbiAgICAgICAgdGhpcy5tb2RhbC5uYW1lID0gJ3BvcHVwTWFuYWdlck1vZGFsJztcbiAgICAgICAgdGhpcy5tb2RhbC5pbnB1dEVuYWJsZWQgPSB0cnVlO1xuICAgICAgICB0aGlzLm1vZGFsLmV2ZW50cy5vbklucHV0RG93bi5hZGQodGhpcy5tZWRpYXRvci5tb2RhbFByZXNzZWQsIHRoaXMubWVkaWF0b3IpO1xuICAgICAgICBcbiAgICAgICAgdGhpcy5tb2RhbC5jYWNoZUFzQml0bWFwID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5tb2RhbC52aXNpYmxlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgcHVibGljIHNob3dNb2RhbCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zZW5kVG9CYWNrKHRoaXMubW9kYWwpO1xuICAgICAgICB0aGlzLm1lZGlhdG9yLm5vdGlmeVBhdXNlR2FtZSgpO1xuICAgICAgICB0aGlzLm1vZGFsLnZpc2libGUgPSB0cnVlO1xuICAgIH1cblxuICAgIHB1YmxpYyBoaWRlTW9kYWwoYW5kUmVzdW1lOiBib29sZWFuID0gdHJ1ZSk6IHZvaWQge1xuICAgICAgICBpZiAoYW5kUmVzdW1lID09PSB0cnVlKSB7XG4gICAgICAgICAgICB0aGlzLm1lZGlhdG9yLm5vdGlmeVJlc3VtZUdhbWUoKTtcbiAgICAgICAgfSAgICBcbiAgICAgICAgdGhpcy5tb2RhbC52aXNpYmxlID0gZmFsc2U7XG4gICAgfVxuXG4gICAgcHVibGljIHJlbW92ZUFsbChkZXN0cm95OiBib29sZWFuID0gdHJ1ZSwgc2lsZW50OiBib29sZWFuID0gZmFsc2UpOiB2b2lkIHtcbiAgICAgICAgaWYgKHRoaXMubW9kYWwpIHtcbiAgICAgICAgICAgIHRoaXMuc2VuZFRvQmFjayh0aGlzLm1vZGFsKTtcbiAgICAgICAgICAgIHRoaXMuaGlkZU1vZGFsKCk7XG4gICAgICAgIH1cbiAgICAgICAgd2hpbGUgKHRoaXMuY2hpbGRyZW4ubGVuZ3RoID4gMSkge1xuICAgICAgICAgICAgdGhpcy5yZW1vdmUodGhpcy5nZXRDaGlsZEF0KDEpLCBkZXN0cm95LCBzaWxlbnQpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMubWVkaWF0b3IuY2xlYXJMb29rdXAoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldCBtZWRpYXRvcigpOiBQb3B1cE1hbmFnZXJNZWRpYXRvciB7XG4gICAgICAgIHJldHVybiA8UG9wdXBNYW5hZ2VyTWVkaWF0b3I+dGhpcy5fbWVkaWF0b3I7XG4gICAgfVxufSIsImltcG9ydCB7TWVkaWF0b3IsIE5vdGlmaWNhdGlvbn0gZnJvbSBcImRpam9uL212Y1wiO1xuaW1wb3J0IHtJTm90aWZpY2F0aW9ufSBmcm9tIFwiZGlqb24vaW50ZXJmYWNlc1wiO1xuaW1wb3J0IEJhc2VNZWRpYXRvciBmcm9tICcuL0Jhc2VNZWRpYXRvcic7XG5pbXBvcnQgTm90aWZpY2F0aW9ucyBmcm9tICcuLi91dGlscy9Ob3RpZmljYXRpb25zJztcbmltcG9ydCB7Q29uc3RhbnRzfSBmcm9tICcuLi91dGlscy9TdGF0aWNzJztcbmltcG9ydCBQb3B1cE1hbmFnZXIgZnJvbSAnLi4vcG9wdXBzL1BvcHVwTWFuYWdlcic7XG5pbXBvcnQgeyBJUG9wdXAgfSBmcm9tICcuLi9wb3B1cHMvQmFzZVBvcHVwJztcblxuLy8gcG9wdXBzXG5pbXBvcnQgT3B0aW9uc1BvcHVwIGZyb20gJy4uL3BvcHVwcy9PcHRpb25zUG9wdXAnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBQb3B1cE1hbmFnZXJNZWRpYXRvciBleHRlbmRzIEJhc2VNZWRpYXRvciB7XG4gICAgcHVibGljIHN0YXRpYyBNRURJQVRPUl9OQU1FOiBzdHJpbmcgPSAncG9wdXBNYW5hZ2VyTWVkaWF0b3InO1xuXG4gICAgcHJpdmF0ZSBsb29rdXA6IHsgW3BvcHVwSUQ6IHN0cmluZ106IElQb3B1cCB9ID0ge307XG4gICAgcHJpdmF0ZSBhbGxvd01vZGFsUHJlc3M6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBwcml2YXRlIHZpc2libGVMaXN0OiBzdHJpbmdbXTtcblxuICAgIGNvbnN0cnVjdG9yKHZpZXdDb21wOiBhbnkpIHtcbiAgICAgICAgc3VwZXIodmlld0NvbXAsIHRydWUsIFBvcHVwTWFuYWdlck1lZGlhdG9yLk1FRElBVE9SX05BTUUpO1xuICAgICAgICB0aGlzLnZpc2libGVMaXN0ID0gW107XG4gICAgfSAgICBcbiAgICBcbiAgICBwdWJsaWMgbGlzdE5vdGlmaWNhdGlvbkludGVyZXN0cygpOiBzdHJpbmdbXSB7XG4gICAgICAgIHJldHVybiBbXG4gICAgICAgICAgICBOb3RpZmljYXRpb25zLkFERF9BTExfUE9QVVBTLFxuICAgICAgICAgICAgTm90aWZpY2F0aW9ucy5SRUdJU1RFUl9QT1BVUCxcbiAgICAgICAgICAgIE5vdGlmaWNhdGlvbnMuU0hPV19QT1BVUCxcbiAgICAgICAgICAgIE5vdGlmaWNhdGlvbnMuSElERV9QT1BVUCxcbiAgICAgICAgICAgIE5vdGlmaWNhdGlvbnMuSElERV9BTExfUE9QVVBTXG4gICAgICAgIF07XG4gICAgfVxuXG4gICAgcHVibGljIGhhbmRsZU5vdGlmaWNhdGlvbihub3RpZmljYXRpb246IElOb3RpZmljYXRpb24pIHtcbiAgICAgICAgY29uc3Qgbm90ZU5hbWUgPSBub3RpZmljYXRpb24uZ2V0TmFtZSgpO1xuICAgICAgICBjb25zdCBub3RlQm9keSA9IG5vdGlmaWNhdGlvbi5nZXRCb2R5KCk7ICAgICAgIFxuXG4gICAgICAgIHN3aXRjaCAobm90ZU5hbWUpIHtcbiAgICAgICAgICAgIGNhc2UgTm90aWZpY2F0aW9ucy5BRERfQUxMX1BPUFVQUzpcbiAgICAgICAgICAgICAgICB0aGlzLmFkZEFsbFBvcHVwcygpO1xuICAgICAgICAgICAgICAgIGJyZWFrO1xuICAgICAgICAgICAgICAgIFxuICAgICAgICAgICAgY2FzZSBOb3RpZmljYXRpb25zLlJFR0lTVEVSX1BPUFVQOlxuICAgICAgICAgICAgICAgIHRoaXMucmVnaXN0ZXJQb3B1cChub3RlQm9keSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgTm90aWZpY2F0aW9ucy5TSE9XX1BPUFVQOlxuICAgICAgICAgICAgICAgIGxldCB0b1Nob3c6IElQb3B1cCA9IHRoaXMuZ2V0UG9wdXAobm90ZUJvZHkpOyBcbiAgICAgICAgICAgICAgICBpZiAodG9TaG93ICE9PSBudWxsKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWFuYWdlci5zaG93TW9kYWwoKTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tYW5hZ2VyLmJyaW5nVG9Ub3AodG9TaG93KTtcbiAgICAgICAgICAgICAgICAgICAgdG9TaG93LnNob3coKS5vbkNvbXBsZXRlLmFkZE9uY2UoKCkgPT4geyB0aGlzLmFsbG93TW9kYWxQcmVzcyA9IHRydWUgfSk7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmlzaWJsZUxpc3QucHVzaChub3RlQm9keSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGJyZWFrO1xuXG4gICAgICAgICAgICBjYXNlIE5vdGlmaWNhdGlvbnMuSElERV9QT1BVUDpcbiAgICAgICAgICAgICAgICBsZXQgdG9IaWRlOiBJUG9wdXAgPSB0aGlzLmdldFBvcHVwKG5vdGVCb2R5KTsgXG4gICAgICAgICAgICAgICAgaWYgKHRvSGlkZSAhPT0gbnVsbCkge1xuICAgICAgICAgICAgICAgICAgICB0b0hpZGUuaGlkZSgpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnVwZGF0ZVZpc2libGVMaXN0KG5vdGVCb2R5KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgYnJlYWs7XG5cbiAgICAgICAgICAgIGNhc2UgTm90aWZpY2F0aW9ucy5ISURFX0FMTF9QT1BVUFM6XG4gICAgICAgICAgICAgICAgd2hpbGUgKHRoaXMudmlzaWJsZUxpc3QubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvb2t1cFt0aGlzLnZpc2libGVMaXN0LnNoaWZ0KCldLmhpZGUoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5tYW5hZ2VyLmhpZGVNb2RhbChmYWxzZSk7XG4gICAgICAgICAgICAgICAgYnJlYWs7ICAgIFxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIGNsZWFyTG9va3VwKCk6IHZvaWQge1xuICAgICAgICB0aGlzLmxvb2t1cCA9IHt9O1xuICAgIH1cblxuICAgIHB1YmxpYyBjbG9zZVRvcCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5zZW5kTm90aWZpY2F0aW9uKE5vdGlmaWNhdGlvbnMuSElERV9QT1BVUCwgdGhpcy52aXNpYmxlTGlzdFt0aGlzLnZpc2libGVMaXN0Lmxlbmd0aCAtIDFdKTtcbiAgICB9XG5cbiAgICBwdWJsaWMgbW9kYWxQcmVzc2VkKCk6IHZvaWQge1xuICAgICAgICBpZiAodGhpcy5wb3B1cFNlbGVjdGlvblJlcXVpcmVkID09PSB0cnVlIHx8IHRoaXMuYWxsb3dNb2RhbFByZXNzID09PSBmYWxzZSkge1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY2xvc2VUb3AoKTtcbiAgICB9ICAgXG4gICAgXG4gICAgcHVibGljIGhhc1BvcHVwV2l0aElkKHBvcHVwSUQ6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5nZXRQb3B1cChwb3B1cElEKSA/IHRydWUgOiBmYWxzZTtcbiAgICB9XG5cbiAgICAvLyBwcml2YXRlIG1ldGhvZHNcbiAgICBwcml2YXRlIGFkZEFsbFBvcHVwcygpOiB2b2lkIHtcbiAgICAgICAgbGV0IGEgPSBuZXcgT3B0aW9uc1BvcHVwKCk7XG4gICAgICAgIFxuICAgICAgICB0aGlzLmFkZEFsbFBvcHVwcyA9ICgpID0+IHsgfTtcbiAgICB9XG4gICAgXG4gICAgcHJpdmF0ZSBnZXRQb3B1cChwb3B1cElEOiBzdHJpbmcpOiBJUG9wdXAge1xuICAgICAgICByZXR1cm4gdGhpcy5sb29rdXBbcG9wdXBJRF0gfHwgbnVsbDtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJlZ2lzdGVyUG9wdXAocG9wdXA6IElQb3B1cCk6IHZvaWQge1xuICAgICAgICB0aGlzLmxvb2t1cFtwb3B1cC5pZF0gPSBwb3B1cDtcbiAgICAgICAgdGhpcy5tYW5hZ2VyLmFkZE1vZGFsKCk7XG4gICAgICAgIHRoaXMubWFuYWdlci5hZGQocG9wdXApO1xuICAgIH1cblxuICAgIHByaXZhdGUgdXBkYXRlVmlzaWJsZUxpc3QodG9SZW1vdmU6IHN0cmluZyk6IHZvaWQge1xuICAgICAgICBsZXQgaW5kZXggPSB0aGlzLnZpc2libGVMaXN0Lmxhc3RJbmRleE9mKHRvUmVtb3ZlKTtcbiAgICAgICAgaWYgKGluZGV4ICE9IC0xKSB7XG4gICAgICAgICAgICBsZXQgaG9sZCA9IHRoaXMudmlzaWJsZUxpc3RbdGhpcy52aXNpYmxlTGlzdC5sZW5ndGggLSAxXTtcbiAgICAgICAgICAgIHRoaXMudmlzaWJsZUxpc3RbaW5kZXhdID0gdGhpcy52aXNpYmxlTGlzdFt0aGlzLnZpc2libGVMaXN0Lmxlbmd0aCAtIDFdO1xuICAgICAgICAgICAgdGhpcy52aXNpYmxlTGlzdFtpbmRleF0gPSBob2xkO1xuICAgICAgICAgICAgdGhpcy52aXNpYmxlTGlzdC5wb3AoKTtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy52aXNpYmxlTGlzdC5sZW5ndGggPT09IDAgfHwgaW5kZXggPT09IC0xKSB7XG4gICAgICAgICAgICB0aGlzLm1hbmFnZXIuaGlkZU1vZGFsKCk7XG4gICAgICAgICAgICB0aGlzLmFsbG93TW9kYWxQcmVzcyA9IGZhbHNlO1xuICAgICAgICB9XG4gICAgfSAgIFxuXG4gICAgLyogR0VUL1NFVFMgKi9cbiAgICBcbiAgICBwcml2YXRlIGdldCBtYW5hZ2VyKCk6IFBvcHVwTWFuYWdlciB7XG4gICAgICAgIHJldHVybiA8UG9wdXBNYW5hZ2VyPnRoaXMudmlld0NvbXBvbmVudDtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldCBwb3B1cFNlbGVjdGlvblJlcXVpcmVkKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICAgIFxuICAgIHB1YmxpYyBnZXQgbmFtZSgpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gUG9wdXBNYW5hZ2VyTWVkaWF0b3IuTUVESUFUT1JfTkFNRTtcbiAgICB9XG59IiwiaW1wb3J0IHtJTm90aWZpY2F0aW9ufSBmcm9tICdkaWpvbi9pbnRlcmZhY2VzJztcbmltcG9ydCB7Q29uc3RhbnRzfSBmcm9tIFwiLi4vdXRpbHMvU3RhdGljc1wiO1xuaW1wb3J0IEJhc2VNZWRpYXRvciBmcm9tICcuL0Jhc2VNZWRpYXRvcic7XG5pbXBvcnQgTm90aWZpY2F0aW9ucyBmcm9tICcuLi91dGlscy9Ob3RpZmljYXRpb25zJztcblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgUHJlbG9hZE1lZGlhdG9yIGV4dGVuZHMgQmFzZU1lZGlhdG9yIHtcbiAgICBwdWJsaWMgc3RhdGljIE1FRElBVE9SX05BTUU6IHN0cmluZyA9ICdwcmVsb2FkTWVkaWF0b3InO1xuXHRcdFxuICAgIC8vIHB1YmxpYyBtZXRob2RzXG4gICAgLy8gY2FsbGVkIGZyb20gUHJlbG9hZCBzdGF0ZVxuXG4gICAgcHVibGljIG5leHQoKTogdm9pZHtcbiAgICAgICAgdGhpcy5nYW1lLnRyYW5zaXRpb24udG8oQ29uc3RhbnRzLlNUQVRFX01FTlUpO1xuICAgIH1cblx0XHRcbiAgICAvLyBnZXR0ZXIgLyBzZXR0ZXJcbiAgICBwdWJsaWMgZ2V0IG5hbWUoKSB7XG4gICAgICAgIHJldHVybiBQcmVsb2FkTWVkaWF0b3IuTUVESUFUT1JfTkFNRTtcbiAgICB9XG59Il19
